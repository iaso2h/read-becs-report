# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settingsUI.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):

    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(683, 516)
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font.setPointSize(10)
        Form.setFont(font)
        self.energyGroup = QGroupBox(Form)
        self.energyGroup.setObjectName(u"energyGroup")
        self.energyGroup.setGeometry(QRect(10, 70, 181, 181))
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(12)
        font1.setBold(True)
        font1.setWeight(75)
        self.energyGroup.setFont(font1)
        self.energyGroup.setStyleSheet(u"color:rgb(122, 142, 255);")
        self.energyGroup.setAlignment(Qt.AlignLeading | Qt.AlignLeft |
                                      Qt.AlignVCenter)
        self.formLayoutWidget = QWidget(self.energyGroup)
        self.formLayoutWidget.setObjectName(u"formLayoutWidget")
        self.formLayoutWidget.setGeometry(QRect(0, 30, 269, 149))
        self.formLayout = QFormLayout(self.formLayoutWidget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setHorizontalSpacing(6)
        self.formLayout.setVerticalSpacing(4)
        self.formLayout.setContentsMargins(8, 0, 0, 0)
        self.eraseHeaderChkBtn = QCheckBox(self.formLayoutWidget)
        self.eraseHeaderChkBtn.setObjectName(u"eraseHeaderChkBtn")
        self.eraseHeaderChkBtn.setFont(font)
        self.eraseHeaderChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(2, QFormLayout.LabelRole,
                                  self.eraseHeaderChkBtn)

        self.eraseTOCChkBtn = QCheckBox(self.formLayoutWidget)
        self.eraseTOCChkBtn.setObjectName(u"eraseTOCChkBtn")
        self.eraseTOCChkBtn.setEnabled(False)
        self.eraseTOCChkBtn.setFont(font)
        self.eraseTOCChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.eraseTOCChkBtn)

        self.insertCoverChkBtn = QCheckBox(self.formLayoutWidget)
        self.insertCoverChkBtn.setObjectName(u"insertCoverChkBtn")
        self.insertCoverChkBtn.setFont(font)
        self.insertCoverChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(4, QFormLayout.LabelRole,
                                  self.insertCoverChkBtn)

        self.insertCoverToolBtn = QToolButton(self.formLayoutWidget)
        self.insertCoverToolBtn.setObjectName(u"insertCoverToolBtn")
        self.insertCoverToolBtn.setMinimumSize(QSize(25, 0))
        self.insertCoverToolBtn.setMaximumSize(QSize(16777215, 25))
        self.insertCoverToolBtn.setFont(font)
        self.insertCoverToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(4, QFormLayout.FieldRole,
                                  self.insertCoverToolBtn)

        self.unifyWinJargonChkBtn = QCheckBox(self.formLayoutWidget)
        self.unifyWinJargonChkBtn.setObjectName(u"unifyWinJargonChkBtn")
        self.unifyWinJargonChkBtn.setFont(font)
        self.unifyWinJargonChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(5, QFormLayout.LabelRole,
                                  self.unifyWinJargonChkBtn)

        self.unifyWinJargonToolBtn = QToolButton(self.formLayoutWidget)
        self.unifyWinJargonToolBtn.setObjectName(u"unifyWinJargonToolBtn")
        self.unifyWinJargonToolBtn.setMinimumSize(QSize(25, 0))
        self.unifyWinJargonToolBtn.setMaximumSize(QSize(16777215, 25))
        self.unifyWinJargonToolBtn.setFont(font)
        self.unifyWinJargonToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(5, QFormLayout.FieldRole,
                                  self.unifyWinJargonToolBtn)

        self.eraseCoverChkBtn = QCheckBox(self.formLayoutWidget)
        self.eraseCoverChkBtn.setObjectName(u"eraseCoverChkBtn")
        self.eraseCoverChkBtn.setFont(font)
        self.eraseCoverChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(1, QFormLayout.LabelRole,
                                  self.eraseCoverChkBtn)

        self.eraseCoverToolBtn = QToolButton(self.formLayoutWidget)
        self.eraseCoverToolBtn.setObjectName(u"eraseCoverToolBtn")
        self.eraseCoverToolBtn.setMinimumSize(QSize(25, 0))
        self.eraseCoverToolBtn.setMaximumSize(QSize(16777215, 25))
        self.eraseCoverToolBtn.setFont(font)
        self.eraseCoverToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(1, QFormLayout.FieldRole,
                                  self.eraseCoverToolBtn)

        self.eraseHeaderToolBtn = QToolButton(self.formLayoutWidget)
        self.eraseHeaderToolBtn.setObjectName(u"eraseHeaderToolBtn")
        self.eraseHeaderToolBtn.setMinimumSize(QSize(25, 0))
        self.eraseHeaderToolBtn.setMaximumSize(QSize(16777215, 25))
        self.eraseHeaderToolBtn.setFont(font)
        self.eraseHeaderToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(2, QFormLayout.FieldRole,
                                  self.eraseHeaderToolBtn)

        self.eraseTOCToolBtn = QToolButton(self.formLayoutWidget)
        self.eraseTOCToolBtn.setObjectName(u"eraseTOCToolBtn")
        self.eraseTOCToolBtn.setMinimumSize(QSize(25, 0))
        self.eraseTOCToolBtn.setMaximumSize(QSize(16777215, 25))
        self.eraseTOCToolBtn.setFont(font)
        self.eraseTOCToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout.setWidget(3, QFormLayout.FieldRole,
                                  self.eraseTOCToolBtn)

        self.globalGroup = QGroupBox(Form)
        self.globalGroup.setObjectName(u"globalGroup")
        self.globalGroup.setGeometry(QRect(10, 10, 531, 51))
        self.globalGroup.setFont(font1)
        self.globalGroup.setStyleSheet(u"color:rgb(122, 142, 255);")
        self.globalGroup.setAlignment(Qt.AlignLeading | Qt.AlignLeft |
                                      Qt.AlignVCenter)
        self.globalGroup.setCheckable(False)
        self.globalGroup.setChecked(False)
        self.layoutWidget = QWidget(self.globalGroup)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 20, 521, 29))
        self.horizontalLayout_2 = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(8, 0, 0, 0)
        self.completeVersionNameChkBtn = QCheckBox(self.layoutWidget)
        self.completeVersionNameChkBtn.setObjectName(
            u"completeVersionNameChkBtn")
        self.completeVersionNameChkBtn.setFont(font)
        self.completeVersionNameChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_2.addWidget(self.completeVersionNameChkBtn)

        self.timeStampChkBtn = QCheckBox(self.layoutWidget)
        self.timeStampChkBtn.setObjectName(u"timeStampChkBtn")
        self.timeStampChkBtn.setFont(font)
        self.timeStampChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_2.addWidget(self.timeStampChkBtn)

        self.consoleModeChkBtn = QCheckBox(self.layoutWidget)
        self.consoleModeChkBtn.setObjectName(u"consoleModeChkBtn")
        self.consoleModeChkBtn.setFont(font)
        self.consoleModeChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_2.addWidget(self.consoleModeChkBtn)

        self.thermalGroup = QGroupBox(Form)
        self.thermalGroup.setObjectName(u"thermalGroup")
        self.thermalGroup.setGeometry(QRect(10, 260, 181, 151))
        font2 = QFont()
        font2.setPointSize(12)
        font2.setBold(True)
        font2.setWeight(75)
        self.thermalGroup.setFont(font2)
        self.thermalGroup.setStyleSheet(u"color:rgb(122, 142, 255);")
        self.thermalGroup.setAlignment(Qt.AlignLeading | Qt.AlignLeft |
                                       Qt.AlignVCenter)
        self.formLayoutWidget_2 = QWidget(self.thermalGroup)
        self.formLayoutWidget_2.setObjectName(u"formLayoutWidget_2")
        self.formLayoutWidget_2.setGeometry(QRect(0, 30, 167, 114))
        self.formLayout_2 = QFormLayout(self.formLayoutWidget_2)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.formLayout_2.setHorizontalSpacing(6)
        self.formLayout_2.setVerticalSpacing(4)
        self.formLayout_2.setContentsMargins(8, 0, 0, 0)
        self.eraseCoverChkBtn_2 = QCheckBox(self.formLayoutWidget_2)
        self.eraseCoverChkBtn_2.setObjectName(u"eraseCoverChkBtn_2")
        self.eraseCoverChkBtn_2.setFont(font)
        self.eraseCoverChkBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole,
                                    self.eraseCoverChkBtn_2)

        self.eraseHeaderChkBtn_2 = QCheckBox(self.formLayoutWidget_2)
        self.eraseHeaderChkBtn_2.setObjectName(u"eraseHeaderChkBtn_2")
        self.eraseHeaderChkBtn_2.setFont(font)
        self.eraseHeaderChkBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole,
                                    self.eraseHeaderChkBtn_2)

        self.insertCoverChkBtn_2 = QCheckBox(self.formLayoutWidget_2)
        self.insertCoverChkBtn_2.setObjectName(u"insertCoverChkBtn_2")
        self.insertCoverChkBtn_2.setFont(font)
        self.insertCoverChkBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole,
                                    self.insertCoverChkBtn_2)

        self.eraseTOCChkBtn_2 = QCheckBox(self.formLayoutWidget_2)
        self.eraseTOCChkBtn_2.setObjectName(u"eraseTOCChkBtn_2")
        self.eraseTOCChkBtn_2.setEnabled(False)
        self.eraseTOCChkBtn_2.setFont(font)
        self.eraseTOCChkBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole,
                                    self.eraseTOCChkBtn_2)

        self.insertCoverToolBtn_2 = QToolButton(self.formLayoutWidget_2)
        self.insertCoverToolBtn_2.setObjectName(u"insertCoverToolBtn_2")
        self.insertCoverToolBtn_2.setMinimumSize(QSize(25, 0))
        self.insertCoverToolBtn_2.setMaximumSize(QSize(16777215, 25))
        self.insertCoverToolBtn_2.setFont(font)
        self.insertCoverToolBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole,
                                    self.insertCoverToolBtn_2)

        self.eraseCoverToolBtn_2 = QToolButton(self.formLayoutWidget_2)
        self.eraseCoverToolBtn_2.setObjectName(u"eraseCoverToolBtn_2")
        self.eraseCoverToolBtn_2.setMinimumSize(QSize(25, 0))
        self.eraseCoverToolBtn_2.setMaximumSize(QSize(16777215, 25))
        self.eraseCoverToolBtn_2.setFont(font)
        self.eraseCoverToolBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole,
                                    self.eraseCoverToolBtn_2)

        self.eraseHeaderToolBtn_2 = QToolButton(self.formLayoutWidget_2)
        self.eraseHeaderToolBtn_2.setObjectName(u"eraseHeaderToolBtn_2")
        self.eraseHeaderToolBtn_2.setMinimumSize(QSize(25, 0))
        self.eraseHeaderToolBtn_2.setMaximumSize(QSize(16777215, 25))
        self.eraseHeaderToolBtn_2.setFont(font)
        self.eraseHeaderToolBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole,
                                    self.eraseHeaderToolBtn_2)

        self.eraseTOCToolBtn_2 = QToolButton(self.formLayoutWidget_2)
        self.eraseTOCToolBtn_2.setObjectName(u"eraseTOCToolBtn_2")
        self.eraseTOCToolBtn_2.setMinimumSize(QSize(25, 0))
        self.eraseTOCToolBtn_2.setMaximumSize(QSize(16777215, 25))
        self.eraseTOCToolBtn_2.setFont(font)
        self.eraseTOCToolBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole,
                                    self.eraseTOCToolBtn_2)

        self.dwgGroup = QGroupBox(Form)
        self.dwgGroup.setObjectName(u"dwgGroup")
        self.dwgGroup.setGeometry(QRect(200, 70, 341, 341))
        self.dwgGroup.setFont(font1)
        self.dwgGroup.setStyleSheet(u"color:rgb(122, 142, 255);")
        self.dwgGroup.setAlignment(Qt.AlignLeading | Qt.AlignLeft |
                                   Qt.AlignVCenter)
        self.winSheetGroup = QGroupBox(self.dwgGroup)
        self.winSheetGroup.setObjectName(u"winSheetGroup")
        self.winSheetGroup.setGeometry(QRect(10, 170, 321, 61))
        self.winSheetGroup.setFont(font)
        self.winSheetGroup.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.winSheetGroup.setFlat(True)
        self.winSheetGroup.setCheckable(True)
        self.horizontalLayoutWidget = QWidget(self.winSheetGroup)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(40, 20, 271, 31))
        self.horizontalLayout_6 = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.decimalPrecisionLabel_2 = QLabel(self.horizontalLayoutWidget)
        self.decimalPrecisionLabel_2.setObjectName(u"decimalPrecisionLabel_2")
        self.decimalPrecisionLabel_2.setMaximumSize(QSize(111, 16777215))
        self.decimalPrecisionLabel_2.setFont(font)

        self.horizontalLayout_6.addWidget(self.decimalPrecisionLabel_2)

        self.horizontalSpacer_2 = QSpacerItem(70, 25, QSizePolicy.Maximum,
                                              QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_2)

        self.decimalPrecisionSpinBox_2 = QSpinBox(self.horizontalLayoutWidget)
        self.decimalPrecisionSpinBox_2.setObjectName(
            u"decimalPrecisionSpinBox_2")
        self.decimalPrecisionSpinBox_2.setMinimumSize(QSize(60, 25))
        self.decimalPrecisionSpinBox_2.setMaximumSize(QSize(60, 16777215))
        self.decimalPrecisionSpinBox_2.setFont(font)
        self.decimalPrecisionSpinBox_2.setLayoutDirection(Qt.LeftToRight)
        self.decimalPrecisionSpinBox_2.setAlignment(Qt.AlignRight |
                                                    Qt.AlignTrailing |
                                                    Qt.AlignVCenter)
        self.decimalPrecisionSpinBox_2.setAccelerated(False)
        self.decimalPrecisionSpinBox_2.setProperty("showGroupSeparator", True)
        self.decimalPrecisionSpinBox_2.setMinimum(1)
        self.decimalPrecisionSpinBox_2.setMaximum(5)
        self.decimalPrecisionSpinBox_2.setValue(5)

        self.horizontalLayout_6.addWidget(self.decimalPrecisionSpinBox_2)

        self.horizontalSpacer_3 = QSpacerItem(40, 25, QSizePolicy.Maximum,
                                              QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_3)

        self.decimalPrecisionToolBtn_2 = QToolButton(
            self.horizontalLayoutWidget)
        self.decimalPrecisionToolBtn_2.setObjectName(
            u"decimalPrecisionToolBtn_2")
        self.decimalPrecisionToolBtn_2.setMinimumSize(QSize(25, 0))
        self.decimalPrecisionToolBtn_2.setMaximumSize(QSize(16777215, 25))
        self.decimalPrecisionToolBtn_2.setFont(font)
        self.decimalPrecisionToolBtn_2.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_6.addWidget(self.decimalPrecisionToolBtn_2)

        self.exaSheetChkBtn = QCheckBox(self.dwgGroup)
        self.exaSheetChkBtn.setObjectName(u"exaSheetChkBtn")
        self.exaSheetChkBtn.setGeometry(QRect(10, 270, 218, 23))
        self.exaSheetChkBtn.setFont(font)
        self.exaSheetChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.winShadingGroup = QGroupBox(self.dwgGroup)
        self.winShadingGroup.setObjectName(u"winShadingGroup")
        self.winShadingGroup.setGeometry(QRect(10, 220, 321, 61))
        self.winShadingGroup.setFont(font)
        self.winShadingGroup.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.winShadingGroup.setFlat(True)
        self.winShadingGroup.setCheckable(True)
        self.horizontalLayoutWidget_2 = QWidget(self.winShadingGroup)
        self.horizontalLayoutWidget_2.setObjectName(u"horizontalLayoutWidget_2")
        self.horizontalLayoutWidget_2.setGeometry(QRect(40, 20, 271, 31))
        self.horizontalLayout_5 = QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.duplicateMergeChkBtn = QCheckBox(self.horizontalLayoutWidget_2)
        self.duplicateMergeChkBtn.setObjectName(u"duplicateMergeChkBtn")
        self.duplicateMergeChkBtn.setFont(font)

        self.horizontalLayout_5.addWidget(self.duplicateMergeChkBtn)

        self.duplicateMergeToolBtn = QToolButton(self.horizontalLayoutWidget_2)
        self.duplicateMergeToolBtn.setObjectName(u"duplicateMergeToolBtn")
        self.duplicateMergeToolBtn.setMinimumSize(QSize(25, 0))
        self.duplicateMergeToolBtn.setMaximumSize(QSize(16777215, 25))
        self.duplicateMergeToolBtn.setFont(font)
        self.duplicateMergeToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_5.addWidget(self.duplicateMergeToolBtn)

        self.outterComponentGroup = QGroupBox(self.dwgGroup)
        self.outterComponentGroup.setObjectName(u"outterComponentGroup")
        self.outterComponentGroup.setGeometry(QRect(10, 80, 321, 91))
        self.outterComponentGroup.setFont(font)
        self.outterComponentGroup.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.outterComponentGroup.setFlat(True)
        self.outterComponentGroup.setCheckable(True)
        self.gridLayoutWidget = QWidget(self.outterComponentGroup)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(40, 20, 271, 62))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.b1MatKeywordLabel = QLabel(self.gridLayoutWidget)
        self.b1MatKeywordLabel.setObjectName(u"b1MatKeywordLabel")
        self.b1MatKeywordLabel.setFont(font)

        self.gridLayout.addWidget(self.b1MatKeywordLabel, 1, 0, 1, 1)

        self.b1MatKeywordComboBox = QComboBox(self.gridLayoutWidget)
        self.b1MatKeywordComboBox.setObjectName(u"b1MatKeywordComboBox")
        self.b1MatKeywordComboBox.setMinimumSize(QSize(80, 0))
        self.b1MatKeywordComboBox.setMaximumSize(QSize(16777215, 25))
        self.b1MatKeywordComboBox.setFont(font)

        self.gridLayout.addWidget(self.b1MatKeywordComboBox, 1, 1, 1, 1)

        self.decimalPrecisionSpinBox = QSpinBox(self.gridLayoutWidget)
        self.decimalPrecisionSpinBox.setObjectName(u"decimalPrecisionSpinBox")
        self.decimalPrecisionSpinBox.setMinimumSize(QSize(0, 25))
        self.decimalPrecisionSpinBox.setMaximumSize(QSize(60, 16777215))
        self.decimalPrecisionSpinBox.setFont(font)
        self.decimalPrecisionSpinBox.setLayoutDirection(Qt.LeftToRight)
        self.decimalPrecisionSpinBox.setAlignment(Qt.AlignRight |
                                                  Qt.AlignTrailing |
                                                  Qt.AlignVCenter)
        self.decimalPrecisionSpinBox.setAccelerated(False)
        self.decimalPrecisionSpinBox.setProperty("showGroupSeparator", True)
        self.decimalPrecisionSpinBox.setMinimum(1)
        self.decimalPrecisionSpinBox.setMaximum(5)
        self.decimalPrecisionSpinBox.setValue(5)

        self.gridLayout.addWidget(self.decimalPrecisionSpinBox, 0, 1, 1, 1)

        self.decimalPrecisionLabel = QLabel(self.gridLayoutWidget)
        self.decimalPrecisionLabel.setObjectName(u"decimalPrecisionLabel")
        self.decimalPrecisionLabel.setFont(font)

        self.gridLayout.addWidget(self.decimalPrecisionLabel, 0, 0, 1, 1)

        self.decimalPrecisionToolBtn = QToolButton(self.gridLayoutWidget)
        self.decimalPrecisionToolBtn.setObjectName(u"decimalPrecisionToolBtn")
        self.decimalPrecisionToolBtn.setMinimumSize(QSize(25, 0))
        self.decimalPrecisionToolBtn.setMaximumSize(QSize(16777215, 25))
        self.decimalPrecisionToolBtn.setFont(font)
        self.decimalPrecisionToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.gridLayout.addWidget(self.decimalPrecisionToolBtn, 0, 2, 1, 1)

        self.b1MatKeywordToolBtn = QToolButton(self.gridLayoutWidget)
        self.b1MatKeywordToolBtn.setObjectName(u"b1MatKeywordToolBtn")
        self.b1MatKeywordToolBtn.setMinimumSize(QSize(25, 0))
        self.b1MatKeywordToolBtn.setMaximumSize(QSize(16777215, 25))
        self.b1MatKeywordToolBtn.setFont(font)
        self.b1MatKeywordToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.gridLayout.addWidget(self.b1MatKeywordToolBtn, 1, 2, 1, 1)

        self.projectGeneralGroup = QGroupBox(self.dwgGroup)
        self.projectGeneralGroup.setObjectName(u"projectGeneralGroup")
        self.projectGeneralGroup.setGeometry(QRect(10, 30, 321, 61))
        self.projectGeneralGroup.setFont(font)
        self.projectGeneralGroup.setStyleSheet(u"color: rgb(0, 0, 0);")
        self.projectGeneralGroup.setFlat(True)
        self.projectGeneralGroup.setCheckable(True)
        self.layoutWidget_2 = QWidget(self.projectGeneralGroup)
        self.layoutWidget_2.setObjectName(u"layoutWidget_2")
        self.layoutWidget_2.setGeometry(QRect(40, 20, 271, 29))
        self.horizontalLayout_3 = QHBoxLayout(self.layoutWidget_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.mostUsedWinShadingChkBtn = QCheckBox(self.layoutWidget_2)
        self.mostUsedWinShadingChkBtn.setObjectName(u"mostUsedWinShadingChkBtn")
        self.mostUsedWinShadingChkBtn.setFont(font)

        self.horizontalLayout_3.addWidget(self.mostUsedWinShadingChkBtn)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Maximum,
                                            QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.mostUsedWinShadingToolBtn = QToolButton(self.layoutWidget_2)
        self.mostUsedWinShadingToolBtn.setObjectName(
            u"mostUsedWinShadingToolBtn")
        self.mostUsedWinShadingToolBtn.setMinimumSize(QSize(25, 0))
        self.mostUsedWinShadingToolBtn.setMaximumSize(QSize(16777215, 25))
        self.mostUsedWinShadingToolBtn.setFont(font)
        self.mostUsedWinShadingToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout_3.addWidget(self.mostUsedWinShadingToolBtn)

        self.exaGroup = QGroupBox(Form)
        self.exaGroup.setObjectName(u"exaGroup")
        self.exaGroup.setGeometry(QRect(10, 420, 181, 51))
        self.exaGroup.setFont(font2)
        self.exaGroup.setStyleSheet(u"color:rgb(122, 142, 255);")
        self.exaGroup.setAlignment(Qt.AlignLeading | Qt.AlignLeft |
                                   Qt.AlignVCenter)
        self.formLayoutWidget_3 = QWidget(self.exaGroup)
        self.formLayoutWidget_3.setObjectName(u"formLayoutWidget_3")
        self.formLayoutWidget_3.setGeometry(QRect(0, 20, 167, 31))
        self.formLayout_3 = QFormLayout(self.formLayoutWidget_3)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.formLayout_3.setContentsMargins(8, 0, 0, 0)
        self.checkWinShadingConsChkBtn = QCheckBox(self.formLayoutWidget_3)
        self.checkWinShadingConsChkBtn.setObjectName(
            u"checkWinShadingConsChkBtn")
        self.checkWinShadingConsChkBtn.setFont(font)
        self.checkWinShadingConsChkBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole,
                                    self.checkWinShadingConsChkBtn)

        self.checkWinShadingConsToolBtn = QToolButton(self.formLayoutWidget_3)
        self.checkWinShadingConsToolBtn.setObjectName(
            u"checkWinShadingConsToolBtn")
        self.checkWinShadingConsToolBtn.setMinimumSize(QSize(25, 0))
        self.checkWinShadingConsToolBtn.setMaximumSize(QSize(16777215, 25))
        self.checkWinShadingConsToolBtn.setFont(font)
        self.checkWinShadingConsToolBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole,
                                    self.checkWinShadingConsToolBtn)

        self.layoutWidget1 = QWidget(Form)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(200, 430, 341, 51))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 6, 0, 11)
        self.defaultBtn = QPushButton(self.layoutWidget1)
        self.defaultBtn.setObjectName(u"defaultBtn")
        self.defaultBtn.setMaximumSize(QSize(16777215, 25))
        self.defaultBtn.setFont(font)

        self.horizontalLayout.addWidget(self.defaultBtn)

        self.horizontalSpacer_4 = QSpacerItem(100, 20, QSizePolicy.Maximum,
                                              QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.infoBtn = QToolButton(self.layoutWidget1)
        self.infoBtn.setObjectName(u"infoBtn")
        self.infoBtn.setMinimumSize(QSize(25, 0))
        self.infoBtn.setMaximumSize(QSize(16777215, 25))
        self.infoBtn.setFont(font)
        self.infoBtn.setStyleSheet(u"color: rgb(0, 0, 0);")

        self.horizontalLayout.addWidget(self.infoBtn)

        self.confirmBtn = QPushButton(self.layoutWidget1)
        self.confirmBtn.setObjectName(u"confirmBtn")
        self.confirmBtn.setMinimumSize(QSize(0, 25))
        self.confirmBtn.setFont(font)

        self.horizontalLayout.addWidget(self.confirmBtn)

        self.closeBtn = QPushButton(self.layoutWidget1)
        self.closeBtn.setObjectName(u"closeBtn")
        self.closeBtn.setMinimumSize(QSize(0, 25))
        self.closeBtn.setFont(font)

        self.horizontalLayout.addWidget(self.closeBtn)

        QWidget.setTabOrder(self.infoBtn, self.confirmBtn)
        QWidget.setTabOrder(self.confirmBtn, self.closeBtn)
        QWidget.setTabOrder(self.closeBtn, self.completeVersionNameChkBtn)
        QWidget.setTabOrder(self.completeVersionNameChkBtn,
                            self.timeStampChkBtn)
        QWidget.setTabOrder(self.timeStampChkBtn, self.consoleModeChkBtn)
        QWidget.setTabOrder(self.consoleModeChkBtn, self.energyGroup)
        QWidget.setTabOrder(self.energyGroup, self.eraseCoverChkBtn)
        QWidget.setTabOrder(self.eraseCoverChkBtn, self.eraseCoverToolBtn)
        QWidget.setTabOrder(self.eraseCoverToolBtn, self.eraseHeaderChkBtn)
        QWidget.setTabOrder(self.eraseHeaderChkBtn, self.eraseHeaderToolBtn)
        QWidget.setTabOrder(self.eraseHeaderToolBtn, self.eraseTOCChkBtn)
        QWidget.setTabOrder(self.eraseTOCChkBtn, self.eraseTOCToolBtn)
        QWidget.setTabOrder(self.eraseTOCToolBtn, self.insertCoverChkBtn)
        QWidget.setTabOrder(self.insertCoverChkBtn, self.insertCoverToolBtn)
        QWidget.setTabOrder(self.insertCoverToolBtn, self.unifyWinJargonChkBtn)
        QWidget.setTabOrder(self.unifyWinJargonChkBtn,
                            self.unifyWinJargonToolBtn)
        QWidget.setTabOrder(self.unifyWinJargonToolBtn, self.thermalGroup)
        QWidget.setTabOrder(self.thermalGroup, self.eraseCoverChkBtn_2)
        QWidget.setTabOrder(self.eraseCoverChkBtn_2, self.eraseCoverToolBtn_2)
        QWidget.setTabOrder(self.eraseCoverToolBtn_2, self.eraseHeaderChkBtn_2)
        QWidget.setTabOrder(self.eraseHeaderChkBtn_2, self.eraseHeaderToolBtn_2)
        QWidget.setTabOrder(self.eraseHeaderToolBtn_2, self.eraseTOCChkBtn_2)
        QWidget.setTabOrder(self.eraseTOCChkBtn_2, self.eraseTOCToolBtn_2)
        QWidget.setTabOrder(self.eraseTOCToolBtn_2, self.insertCoverChkBtn_2)
        QWidget.setTabOrder(self.insertCoverChkBtn_2, self.insertCoverToolBtn_2)
        QWidget.setTabOrder(self.insertCoverToolBtn_2, self.exaGroup)
        QWidget.setTabOrder(self.exaGroup, self.checkWinShadingConsChkBtn)
        QWidget.setTabOrder(self.checkWinShadingConsChkBtn,
                            self.checkWinShadingConsToolBtn)
        QWidget.setTabOrder(self.checkWinShadingConsToolBtn, self.dwgGroup)
        QWidget.setTabOrder(self.dwgGroup, self.projectGeneralGroup)
        QWidget.setTabOrder(self.projectGeneralGroup,
                            self.mostUsedWinShadingChkBtn)
        QWidget.setTabOrder(self.mostUsedWinShadingChkBtn,
                            self.mostUsedWinShadingToolBtn)
        QWidget.setTabOrder(self.mostUsedWinShadingToolBtn,
                            self.outterComponentGroup)
        QWidget.setTabOrder(self.outterComponentGroup,
                            self.decimalPrecisionSpinBox)
        QWidget.setTabOrder(self.decimalPrecisionSpinBox,
                            self.decimalPrecisionToolBtn)
        QWidget.setTabOrder(self.decimalPrecisionToolBtn,
                            self.b1MatKeywordComboBox)
        QWidget.setTabOrder(self.b1MatKeywordComboBox, self.b1MatKeywordToolBtn)
        QWidget.setTabOrder(self.b1MatKeywordToolBtn, self.winSheetGroup)
        QWidget.setTabOrder(self.winSheetGroup, self.decimalPrecisionSpinBox_2)
        QWidget.setTabOrder(self.decimalPrecisionSpinBox_2,
                            self.decimalPrecisionToolBtn_2)
        QWidget.setTabOrder(self.decimalPrecisionToolBtn_2,
                            self.winShadingGroup)
        QWidget.setTabOrder(self.winShadingGroup, self.duplicateMergeChkBtn)
        QWidget.setTabOrder(self.duplicateMergeChkBtn,
                            self.duplicateMergeToolBtn)
        QWidget.setTabOrder(self.duplicateMergeToolBtn, self.exaSheetChkBtn)
        QWidget.setTabOrder(self.exaSheetChkBtn, self.defaultBtn)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(
            QCoreApplication.translate("Form", u"\u8bbe\u7f6e", None))
        self.energyGroup.setTitle(
            QCoreApplication.translate(
                "Form", u"\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66", None))
        self.eraseHeaderChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u9875\u7709",
                None))
        #if QT_CONFIG(tooltip)
        self.eraseTOCChkBtn.setToolTip(
            QCoreApplication.translate("Form",
                                       u"\u529f\u80fd\u5f00\u53d1\u5f53\u4e2d",
                                       None))
        #endif // QT_CONFIG(tooltip)
        self.eraseTOCChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u76ee\u5f55",
                None))
        self.insertCoverChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u63d2\u5165\u534e\u84dd\u8bbe\u8ba1\u5c01\u9762",
                None))
        self.insertCoverToolBtn.setText(
            QCoreApplication.translate("Form", u"...", None))
        #if QT_CONFIG(tooltip)
        self.unifyWinJargonChkBtn.setToolTip(
            QCoreApplication.translate(
                "Form",
                u"\u4fee\u6539\u5e76\u7edf\u4e00\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66\u7684\u5173\u4e8e\u95e8\u7a97\u906e\u9633\u7cfb\u6570\u7684\u672f\u8bed",
                None))
        #endif // QT_CONFIG(tooltip)
        self.unifyWinJargonChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u7edf\u4e00\u95e8\u7a97\u906e\u9633\u672f\u8bed",
                None))
        self.unifyWinJargonToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.eraseCoverChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u5c01\u9762",
                None))
        self.eraseCoverToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.eraseHeaderToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.eraseTOCToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.globalGroup.setTitle(
            QCoreApplication.translate("Form", u"\u5168\u5c40\u8bbe\u7f6e",
                                       None))
        self.completeVersionNameChkBtn.setText(
            QCoreApplication.translate(
                "Form",
                u"\u663e\u793a\u5b8c\u6574\u7684\u8f6f\u4ef6\u7248\u672c\u53f7",
                None))
        self.timeStampChkBtn.setText(
            QCoreApplication.translate(
                "Form",
                u"\u8f93\u51fa\u6587\u4ef6\u6dfb\u52a0\u65f6\u95f4\u540e\u7f00",
                None))
        self.consoleModeChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u4f7f\u7528\u547d\u4ee4\u7a97\u53e3\u6a21\u5f0f",
                None))
        self.thermalGroup.setTitle(
            QCoreApplication.translate(
                "Form", u"\u9694\u70ed\u68c0\u67e5\u8ba1\u7b97\u4e66", None))
        self.eraseCoverChkBtn_2.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u5c01\u9762",
                None))
        self.eraseHeaderChkBtn_2.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u9875\u7709",
                None))
        self.insertCoverChkBtn_2.setText(
            QCoreApplication.translate(
                "Form", u"\u63d2\u5165\u534e\u84dd\u8bbe\u8ba1\u5c01\u9762",
                None))
        #if QT_CONFIG(tooltip)
        self.eraseTOCChkBtn_2.setToolTip(
            QCoreApplication.translate("Form",
                                       u"\u529f\u80fd\u5f00\u53d1\u5f53\u4e2d",
                                       None))
        #endif // QT_CONFIG(tooltip)
        self.eraseTOCChkBtn_2.setText(
            QCoreApplication.translate(
                "Form", u"\u53bb\u9664\u65af\u7ef4\u5c14\u539f\u76ee\u5f55",
                None))
        self.insertCoverToolBtn_2.setText(
            QCoreApplication.translate("Form", u"...", None))
        self.eraseCoverToolBtn_2.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.eraseHeaderToolBtn_2.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.eraseTOCToolBtn_2.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.dwgGroup.setTitle(
            QCoreApplication.translate(
                "Form",
                u"\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u4e13\u9879\u8bf4\u660e",
                None))
        self.winSheetGroup.setTitle(
            QCoreApplication.translate(
                "Form", u"\u586b\u5199\u5916\u7a97\u70ed\u5de5\u8868", None))
        self.decimalPrecisionLabel_2.setText(
            QCoreApplication.translate(
                "Form", u"\u5c0f\u6570\u70b9\u540e\u7cbe\u786e\u4f4d\u6570",
                None))
        self.decimalPrecisionToolBtn_2.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.exaSheetChkBtn.setText(
            QCoreApplication.translate(
                "Form",
                u"\u586b\u5199\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u6307\u6807\u4e00\u89c8\u8868",
                None))
        self.winShadingGroup.setTitle(
            QCoreApplication.translate(
                "Form", u"\u586b\u5199\u5916\u7a97\u906e\u9633\u8868", None))
        self.duplicateMergeChkBtn.setText(
            QCoreApplication.translate(
                "Form",
                u"\u5408\u5e76\u91cd\u590d\u7684\u95e8\u7a97\u8868\u683c\u884c",
                None))
        self.duplicateMergeToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.outterComponentGroup.setTitle(
            QCoreApplication.translate(
                "Form",
                u"\u586b\u5199\u5916\u56f4\u62a4\u7ed3\u6784\u6784\u9020\u53ca\u70ed\u5de5\u6027\u80fd\u53c2\u6570\u8868",
                None))
        self.b1MatKeywordLabel.setText(
            QCoreApplication.translate(
                "Form",
                u"\u6750\u6599B1\u7ea7\u71c3\u70e7\u6027\u80fd\u5173\u952e\u5b57",
                None))
        self.decimalPrecisionLabel.setText(
            QCoreApplication.translate(
                "Form", u"\u5c0f\u6570\u70b9\u540e\u7cbe\u786e\u4f4d\u6570",
                None))
        self.decimalPrecisionToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.b1MatKeywordToolBtn.setText(
            QCoreApplication.translate("Form", u"...", None))
        self.projectGeneralGroup.setTitle(
            QCoreApplication.translate("Form",
                                       u"\u586b\u5199\u5de5\u7a0b\u6982\u51b5",
                                       None))
        self.mostUsedWinShadingChkBtn.setText(
            QCoreApplication.translate(
                "Form",
                u"\u586b\u5199\u6700\u9891\u7e41\u4f7f\u7528\u7684\u906e\u9633\u7c7b\u578b",
                None))
        self.mostUsedWinShadingToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.exaGroup.setTitle(
            QCoreApplication.translate(
                "Form",
                u"\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u5ba1\u67e5\u8868",
                None))
        #if QT_CONFIG(tooltip)
        self.checkWinShadingConsChkBtn.setToolTip(
            QCoreApplication.translate(
                "Form",
                u"\u68c0\u67e5\u8282\u80fd\u8bbe\u8ba1\u5ba1\u67e5\u8868\u7684\u5916\u906e\u9633\u7cfb\u6570\u662f\u5426\u4e0e\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66\u7684\u906e\u9633\u7cfb\u6570SD\u4e00\u81f4",
                None))
        #endif // QT_CONFIG(tooltip)
        self.checkWinShadingConsChkBtn.setText(
            QCoreApplication.translate(
                "Form", u"\u68c0\u67e5\u906e\u9633\u7cfb\u6570\u7edf\u4e00",
                None))
        self.checkWinShadingConsToolBtn.setText(
            QCoreApplication.translate("Form", u"?", None))
        self.defaultBtn.setText(
            QCoreApplication.translate("Form", u"\u6062\u590d\u9ed8\u8ba4",
                                       None))
        self.infoBtn.setText(QCoreApplication.translate("Form", u"!", None))
        self.confirmBtn.setText(
            QCoreApplication.translate("Form", u"\u786e\u5b9a(&S)", None))
        self.closeBtn.setText(
            QCoreApplication.translate("Form", u"\u5173\u95ed(&C)", None))

    # retranslateUi
