import os
import logging
from gui import UIUtil, myUI
from docx import Document
from docxcompose.composer import Composer
import errors
import file_open
import global_control

logger = logging.getLogger(name=__name__)


def get_file_path(file_base_name, file_ext, output_dir):
    """return new file path concatenate new time string"""
    if global_control.settings["全局设置"]["输出文件添加时间后缀"]:
        new_file_name = file_base_name + file_open.timestr + file_ext
    else:
        new_file_name = file_base_name + file_ext

    new_file_path = r"{}\{}".format(output_dir, new_file_name)
    return new_file_path


def output():

    def insert_cover_and_save(report, save_file):
        import re

        cover_path = r"{}\src\模板_设计报告书封面.docx".format(file_open.wd)
        cover = Document(cover_path)
        tables = cover.tables
        # get report content
        building_ara = int(re.search(r'\d+', energy.building["area"]).group())
        project_property = energy.building_property
        report_content = ""
        word_document_name = dict(建筑节能计算书="建筑节能计算书", 隔热检查计算书="隔热检查计算书")

        if file_type == "建筑节能计算书":
            if project_property == "公共建筑":
                if building_ara > 300:
                    report_content = "       甲类公共建筑节能       "
                else:
                    report_content = "       乙类公共建筑节能       "
            else:
                report_content = "        居住建筑节能        "
        elif file_type == "隔热检查计算书":
            if project_property == "公共建筑":
                if building_ara > 300:
                    report_content = "       甲类公共建筑隔热       "
                else:
                    report_content = "       乙类公共建筑隔热       "
            else:
                report_content = "        居住建筑隔热        "

        # 表格1
        def change_cover_text(table_index, cell_index, text, style=1):
            """
            tables[1]._cells[1].text = report.Project_info.建设单位
            tables[1]._cells[1].paragraphs[0].style = "Style1"
            """
            if style == 1:
                tables[table_index]._cells[cell_index].text = text
                tables[table_index]._cells[cell_index].paragraphs[
                    0].style = "Style1"
            elif style == 2:
                tables[table_index]._cells[cell_index].text = text
                tables[table_index]._cells[cell_index].paragraphs[
                    0].style = "Style2"
            else:
                pass

        change_cover_text(1, 1, energy.project["client"])
        change_cover_text(1, 5, energy.project["name"])
        change_cover_text(1, 11, energy.project["id"])
        change_cover_text(1, 15,
                          global_control.project_settings["department_name"])
        change_cover_text(
            1, 17, energy.software["name"][len(energy.software["name"]) - 8:])
        change_cover_text(1, 19, word_document_name[file_type])

        # 表格2

        change_cover_text(2, 1, report_content, 2)
        change_cover_text(2, 4, global_control.project_settings["user_name"])
        try:
            change_cover_text(
                2, 7, global_control.project_settings["projects"][
                    energy.project["name"]]["校核人"])
        except KeyError:
            change_cover_text(2, 7, "输入姓名")

        try:
            change_cover_text(
                2, 10, global_control.project_settings["projects"][
                    energy.project["name"]]["审核人"])
        except KeyError:
            change_cover_text(2, 10, "输入姓名")

        # merge docxs
        try:
            composer = Composer(cover)
            composer.append(report.docx)
            composer.save(save_file)
        except Exception:
            myUI.MyMessageBox.about(myUI.MyWidget(), "提示",
                                    "插入新封面失败，可尝试此关闭设置再重新运行")

    def save(file_path):

        if file_type == "建筑节能计算书":
            if global_control.settings["建筑节能计算书"]["插入华蓝设计封面"] is True:
                insert_cover_and_save(energy, file_path)
            else:
                energy.docx.save(file_path)
        elif file_type == "隔热检查计算书":
            if global_control.settings["隔热检查计算书"]["插入华蓝设计封面"] is True:
                insert_cover_and_save(thermal, file_path)
                pass
            else:
                thermal.docx.save(file_path)
        elif file_type == "建筑节能设计专项说明":
            dwg.wb.save(file_path)
        elif file_type == "建筑节能设计审查表":
            exa.docx.save(file_path)
        else:
            pass

        logger.info("Saving file at: {}".format(file_path))

    # check file location
    output_dir = global_control.settings["output_dir"]
    if not os.path.isdir(global_control.settings["output_dir"]):
        try:
            os.mkdir(output_dir)
        except Exception:
            myUI.MyMessageBox.warning(myUI.MyWidget(), "创建输出文件夹失败",
                                      "程序在创建输出文件夹时遇到了错误，请选择其他文件夹",
                                      myUI.MyMessageBox.Ok,
                                      myUI.MyMessageBox.Ok)
    else:
        pass
    error_module = None
    from becs import becs_energy_process as energy
    output_file_keys = list(file_open.output_file_dict.keys())
    if "建筑节能计算书" in output_file_keys:
        if global_control.default_settings["全局设置"]["开发者模式"] is False:
            try:
                file_type = "建筑节能计算书"
                match_file = file_open.output_file_dict[file_type]
                energy.run(match_file.file_name)
                logger.info("Saving files")
                save(
                    get_file_path(match_file.file_base_name,
                                  match_file.file_ext, output_dir))
            except Exception as exe:
                logger.error(exe)
                logger.info(errors.exception_Text)
                error_module = "建筑节能计算书"
        else:
            file_type = "建筑节能计算书"
            match_file = file_open.output_file_dict[file_type]
            energy.run(match_file.file_name)
            logger.info("Saving files")
            save(
                get_file_path(match_file.file_base_name, match_file.file_ext,
                              output_dir))

    if "隔热检查计算书" in output_file_keys and not error_module:
        if global_control.default_settings["全局设置"]["开发者模式"] is False:
            try:
                from becs import becs_thermal_process as thermal
                file_type = "隔热检查计算书"
                match_file = file_open.output_file_dict[file_type]
                save(
                    get_file_path(match_file.file_base_name,
                                  match_file.file_ext, output_dir))
            except Exception as exe:
                logger.error(exe)
                logger.info(errors.exception_Text)
                error_module = "隔热检查计算书"
        else:
            from becs import becs_thermal_process as thermal
            file_type = "隔热检查计算书"
            match_file = file_open.output_file_dict[file_type]
            save(
                get_file_path(match_file.file_base_name, match_file.file_ext,
                              output_dir))

    if "建筑节能设计审查表" in output_file_keys and not error_module:
        if global_control.default_settings["全局设置"]["开发者模式"] is False:
            try:
                file_type = "建筑节能设计审查表"
                match_file = file_open.output_file_dict[file_type]
                if energy.building_property == "公共建筑":
                    # 公共建筑不执行此程序
                    logger.info("公共建筑 detected, skip outputing 建筑节能设计审查表")
                else:
                    if global_control.settings["output_mode"][
                            "建筑节能设计审查表"] is False:
                        pass
                    else:
                        from becs import becs_exa_report as exa
                        exa.process(match_file)
                        # All option disable but output check is enable
                        if global_control.settings["建筑节能设计审查表"][
                                "检查外窗遮阳系数统一"] is False:
                            pass
                        # option and output enable
                        else:
                            save(
                                get_file_path(exa.file_base_name, exa.file_ext,
                                              output_dir))
            except Exception as exe:
                logger.error(exe)
                logger.info(errors.exception_Text)
                error_module = "建筑节能设计审查表"
        else:
            file_type = "建筑节能设计审查表"
            match_file = file_open.output_file_dict[file_type]
            if energy.building_property == "公共建筑":
                # 公共建筑不执行此程序
                logger.info("公共建筑 detected, skip outputing 建筑节能设计审查表")
            else:
                if global_control.settings["output_mode"]["建筑节能设计审查表"] is False:
                    pass
                else:
                    from becs import becs_exa_report as exa
                    exa.process(match_file)
                    # All option disable but output check is enable
                    if global_control.settings["建筑节能设计审查表"][
                            "检查外窗遮阳系数统一"] is False:
                        pass
                    # option and output enable
                    else:
                        save(
                            get_file_path(exa.file_base_name, exa.file_ext,
                                          output_dir))

    if "建筑节能设计专项说明" in output_file_keys and not error_module:
        if global_control.default_settings["全局设置"]["开发者模式"] is False:
            try:
                file_type = "建筑节能设计专项说明"
                output_check_list = list(global_control.settings["建筑节能设计专项说明"]
                                         ["sheet_output_mode"].values())
                # All option disable but output check is enable
                if output_check_list.count(False) == len(output_check_list):
                    myUI.MyMessageBox.about(
                        myUI.MyWidget(), "提示",
                        "已取消“建筑节能设计专项说明.xlsx”的输出，原因是：\n\n“设置-->建筑节能设计专项说明”选项已被取消，\n\n没有输出该文件的必要"
                    )
                else:
                    from becs import becs_process_sheet as dwg
                    if global_control.settings["auto_match"] is True:
                        file_base_name = file_open.output_file_dict[
                            "建筑节能计算书"].file_prefix + file_type
                    else:
                        file_base_name = file_type
                    save(get_file_path(file_base_name, ".xlsx", output_dir))
            except Exception as exe:
                logger.error(exe)
                logger.info(errors.exception_Text)
                error_module = "建筑节能设计专项说明"
        else:
            file_type = "建筑节能设计专项说明"
            output_check_list = list(global_control.settings["建筑节能设计专项说明"]
                                     ["sheet_output_mode"].values())
            # All option disable but output check is enable
            if output_check_list.count(False) == len(output_check_list):
                myUI.MyMessageBox.about(
                    myUI.MyWidget(), "提示",
                    "已取消“建筑节能设计专项说明.xlsx”的输出，原因是：\n\n“设置-->建筑节能设计专项说明”选项已被取消，\n\n没有输出该文件的必要"
                )
            else:
                from becs import becs_process_sheet as dwg
                if global_control.settings["auto_match"] is True:
                    file_base_name = file_open.output_file_dict[
                        "建筑节能计算书"].file_prefix + file_type
                else:
                    file_base_name = file_type
                save(get_file_path(file_base_name, ".xlsx", output_dir))

    # Successfully output all files
    if not error_module:
        # Special case for 建筑节能设计审查表
        if (global_control.settings["output_mode"]["建筑节能设计审查表"] is True) and (
                global_control.settings["建筑节能设计审查表"]["检查外窗遮阳系数统一"] is False):
            myUI.MyMessageBox.about(
                myUI.MyWidget(), "提示",
                "已取消“建筑节能设计审查表.docx”的输出，原因是：\n\n“设置-->建筑节能设计审查表-->检查遮阳系数统一”选项已被取消，\n\n没有输出该文件的必要"
            )
        else:
            pass
        # Successfully output all files
        logger.info("Processing successfully")
        # Saving settings
        global_control.project_settings["total_run_time"] += 1
        current_running_project = global_control.project_settings[
            "current_running_project"]
        global_control.project_settings["projects"][current_running_project][
            "runtime"] += 1
        global_control.write_settings("project_settings")

        # GUI
        UIUtil.property = energy.building_property
        if global_control.settings["全局设置"]['开发者模式'] is False:
            UIUtil.askOpenFolder()
            UIUtil.askSetUpCoverInfo()

    # clean up output file when process fails
    else:
        output_dir_files = os.listdir(output_dir)
        for file in output_dir_files:
            # TODO: support when timestr is disable
            if file_open.timestr in file:
                os.remove(os.path.join(output_dir, file))
            else:
                pass
        myUI.MyMessageBox.critical(myUI.MyWidget(), "错误",
                                   f"在输出{error_module}时发生错误",
                                   myUI.MyMessageBox.Ok, myUI.MyMessageBox.Ok)


if __name__ == "__main__":
    pass
