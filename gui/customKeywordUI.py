# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'customKeywordUI.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):

    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(544, 325)
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font.setPointSize(10)
        Form.setFont(font)
        self.layoutWidget = QWidget(Form)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 521, 251))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setWordWrap(True)

        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding,
                                            QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.energyPrefixLabel = QLabel(self.layoutWidget)
        self.energyPrefixLabel.setObjectName(u"energyPrefixLabel")
        self.energyPrefixLabel.setFont(font)
        self.energyPrefixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout_2.addWidget(self.energyPrefixLabel)

        self.energyLineEdit = QLineEdit(self.layoutWidget)
        self.energyLineEdit.setObjectName(u"energyLineEdit")

        self.horizontalLayout_2.addWidget(self.energyLineEdit)

        self.energySuffixLabel = QLabel(self.layoutWidget)
        self.energySuffixLabel.setObjectName(u"energySuffixLabel")
        self.energySuffixLabel.setFont(font)
        self.energySuffixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout_2.addWidget(self.energySuffixLabel)

        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_2 = QSpacerItem(0, 0, QSizePolicy.Expanding,
                                              QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.thermalPrefixLabel = QLabel(self.layoutWidget)
        self.thermalPrefixLabel.setObjectName(u"thermalPrefixLabel")
        self.thermalPrefixLabel.setFont(font)
        self.thermalPrefixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout.addWidget(self.thermalPrefixLabel)

        self.thermLineEdit = QLineEdit(self.layoutWidget)
        self.thermLineEdit.setObjectName(u"thermLineEdit")

        self.horizontalLayout.addWidget(self.thermLineEdit)

        self.thermalSuffixLabel = QLabel(self.layoutWidget)
        self.thermalSuffixLabel.setObjectName(u"thermalSuffixLabel")
        self.thermalSuffixLabel.setFont(font)
        self.thermalSuffixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout.addWidget(self.thermalSuffixLabel)

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_3 = QSpacerItem(0, 0, QSizePolicy.Expanding,
                                              QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_3)

        self.exaPrefixLabel = QLabel(self.layoutWidget)
        self.exaPrefixLabel.setObjectName(u"exaPrefixLabel")
        self.exaPrefixLabel.setFont(font)
        self.exaPrefixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout_3.addWidget(self.exaPrefixLabel)

        self.exaLineEdit = QLineEdit(self.layoutWidget)
        self.exaLineEdit.setObjectName(u"exaLineEdit")

        self.horizontalLayout_3.addWidget(self.exaLineEdit)

        self.exaSuffixLabel = QLabel(self.layoutWidget)
        self.exaSuffixLabel.setObjectName(u"exaSuffixLabel")
        self.exaSuffixLabel.setFont(font)
        self.exaSuffixLabel.setStyleSheet(u"color: rgb(100, 100, 100);")

        self.horizontalLayout_3.addWidget(self.exaSuffixLabel)

        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.layoutWidget1 = QWidget(Form)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(280, 280, 254, 37))
        self.horizontalLayout_4 = QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(5, 5, 5, 5)
        self.defaultBtn = QPushButton(self.layoutWidget1)
        self.defaultBtn.setObjectName(u"defaultBtn")

        self.horizontalLayout_4.addWidget(self.defaultBtn)

        self.confirmBtn = QPushButton(self.layoutWidget1)
        self.confirmBtn.setObjectName(u"confirmBtn")

        self.horizontalLayout_4.addWidget(self.confirmBtn)

        self.cancelBtn = QPushButton(self.layoutWidget1)
        self.cancelBtn.setObjectName(u"cancelBtn")

        self.horizontalLayout_4.addWidget(self.cancelBtn)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle("自定义关键字")
        self.label.setText(
            QCoreApplication.translate(
                "Form",
                u"\u4ee5\u4e0b\u8f93\u5165\u6846\u5185\u7684\u5173\u952e\u5b57\u662f\u7a0b\u5e8f\u5728\u201c\u81ea\u52a8\u9009\u62e9\u6587\u4ef6\u201d\u6a21\u5f0f\u4e0b\u6267\u884c\u8f93\u51fa\u547d\u4ee4\u65f6\u4f1a\u5728\u7a0b\u5e8f\u76ee\u5f55\u81ea\u52a8\u641c\u7d22\u7684\u5173\u952e\u5b57\uff0c\u4f60\u53ef\u4ee5\u5728\u6b64\u8bbe\u7f6e\u81ea\u5b9a\u4e49\u5173\u952e\u5b57\uff0c\u7a0b\u5e8f\u4f1a\u5728\u8bbe\u7f6e\u786e\u5b9a\u4fdd\u5b58\u540e\u4ee5\u65b0\u7684\u8bbe\u7f6e\u6765\u6267\u884c\u547d\u4ee4\u3002\n"
                "\n"
                "\u6bd4\u5982\uff1a\u5f53\u9879\u76ee\u540d\u79f0\u4e3a\u201cABC123#\u5927\u697c\u201d\u65f6\u4f1a\u81ea\u52a8\u68c0\u7d22\u7a0b\u5e8f\u76ee\u5f55\u4e0b\u7684\u201cABC123#\u5927\u697c\u9694\u70ed\u68c0\u67e5\u8ba1\u7b97\u4e66.docx\"\n"
                "", None))
        self.energyPrefixLabel.setText(
            QCoreApplication.translate("Form", u"ABC123#\u5927\u697c", None))
        self.energyLineEdit.setPlaceholderText(
            QCoreApplication.translate(
                "Form", u"\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66", None))
        self.energySuffixLabel.setText(
            QCoreApplication.translate("Form", u".docx", None))
        self.thermalPrefixLabel.setText(
            QCoreApplication.translate("Form", u"ABC123#\u5927\u697c", None))
        self.thermLineEdit.setPlaceholderText(
            QCoreApplication.translate(
                "Form", u"\u9694\u70ed\u68c0\u67e5\u8ba1\u7b97\u4e66", None))
        self.thermalSuffixLabel.setText(
            QCoreApplication.translate("Form", u".docx", None))
        self.exaPrefixLabel.setText(
            QCoreApplication.translate("Form", u"ABC123#\u5927\u697c", None))
        self.exaLineEdit.setPlaceholderText("建筑节能设计审查表")
        self.exaSuffixLabel.setText(
            QCoreApplication.translate("Form", u".docx", None))
        self.defaultBtn.setText(
            QCoreApplication.translate("Form", u"\u6062\u590d\u9ed8\u8ba4",
                                       None))
        self.confirmBtn.setText(
            QCoreApplication.translate("Form", u"\u786e\u5b9a(&S)", None))
        self.cancelBtn.setText(
            QCoreApplication.translate("Form", u"\u53d6\u6d88(&C)", None))

    # retranslateUi
