# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'multipleFile.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):

    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(440, 100)
        self.horizontalLayoutWidget = QWidget(Form)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(10, 60, 421, 31))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.comboBox = QComboBox(self.horizontalLayoutWidget)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setMaximumSize(QSize(16777215, 25))

        self.horizontalLayout.addWidget(self.comboBox)

        self.pushButton = QPushButton(self.horizontalLayoutWidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setMaximumSize(QSize(70, 25))

        self.horizontalLayout.addWidget(self.pushButton)

        self.toolButton = QToolButton(self.horizontalLayoutWidget)
        self.toolButton.setObjectName(u"toolButton")
        self.toolButton.setMaximumSize(QSize(25, 25))

        self.horizontalLayout.addWidget(self.toolButton)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 0, 421, 41))
        self.label_2.setAlignment(Qt.AlignLeading | Qt.AlignLeft | Qt.AlignTop)
        self.label_2.setWordWrap(True)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(
            QCoreApplication.translate("Form", u"\u9009\u62e9\u6587\u4ef6",
                                       None))
        self.pushButton.setText(
            QCoreApplication.translate("Form", u"\u786e\u5b9a", None))
        #if QT_CONFIG(tooltip)
        self.toolButton.setToolTip(
            QCoreApplication.translate(
                "Form", u"\u624b\u52a8\u6dfb\u52a0\u5176\u4ed6\u6587\u4ef6",
                None))
        #endif // QT_CONFIG(tooltip)
        self.toolButton.setText(QCoreApplication.translate(
            "Form", u"...", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"test", None))

    # retranslateUi
