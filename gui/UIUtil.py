import os
import global_control
import logging
import errors
from gui import myUI
from PySide2.QtCore import Qt, QSize
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QFormLayout, QLabel, QLineEdit, QPushButton, \
    QHBoxLayout, QComboBox

building_property = None
logger = logging.getLogger(name=__name__)


def write_settings(parentWidget, settingsMode):
    try:
        global_control.write_settings(settingsMode)
    except Exception:
        if global_control.default_settings["全局设置"][
            "开发者模式"] is True:
            pass
        else:
            myUI.MyMessageBox.critical(parentWidget, "错误",
                                       "程序在写入设置发生错误,请确保以下路径是可读的\n{}".format(
                                           global_control.settings_dir),
                                       myUI.MyMessageBox.Yes |
                                       myUI.MyMessageBox.No,
                                       myUI.MyMessageBox.Yes)


def getComboBoxItems(comboBoxWidget):
    count = comboBoxWidget.count()
    if count == 0:
        items = [comboBoxWidget.itemText(0)]
    else:
        items = [comboBoxWidget.itemText(i) for i in range(count)]
    return items


def askOpenFolder():
    """Show messages different error types and how many times each error type
    occurs"""
    global logger
    if errors.warn_message:
        message_dict = {message: errors.warn_message.count(message) for message
                        in
                        tuple(errors.warn_message)}
        merge_items = [str(item[0]) + "：" + str(item[1]) + "次" for item in
                       message_dict.items()]
        messages = "\n".join(merge_items)
        logger.warning("检查发现的错误：\n" + messages)
        messages = "检查发现的错误：\n\n" + messages + "\n\n\n输出完成，是否打开输出文件夹"
    else:
        messages = "输出完成，是否打开输出文件夹"
    reply = myUI.MyMessageBox.question(myUI.MyWidget(), "提示", messages,
                                       myUI.MyMessageBox.Yes |
                                       myUI.MyMessageBox.No,
                                       myUI.MyMessageBox.Yes)

    # when user say yes
    if reply == myUI.MyMessageBox.Yes:
        openPath()
    # when user turn down to open file folder
    else:
        pass


def openPath():
    try:
        os.startfile(global_control.settings["output_dir"])
    except FileNotFoundError:
        os.mkdir(global_control.settings["output_dir"])
        os.startfile(global_control.settings["output_dir"])


def setIcon(widget, iconName, widgetSize=None):
    iconPath = global_control.wd + os.path.sep + "src" + os.path.sep + \
               "icons" + os.path.sep + iconName + ".ico"
    icon = QIcon()
    icon.addFile(iconPath, QSize(), QIcon.Normal, QIcon.Off)
    widget.setIcon(icon)
    widget.setIconSize(QSize(20, 20))
    if widgetSize:
        widget.setFixedSize(widgetSize[0], widgetSize[1])
    else:
        pass


class ProjectCoverWidget(myUI.MyDialog):
    def __init__(self):
        super(ProjectCoverWidget, self).__init__()
        self.setWindowTitle("计算书封面自定义")
        # Add layout
        self.formLayout = QFormLayout(self)
        self.formLayout.setRowWrapPolicy(
            self.formLayout.rowWrapPolicy().WrapLongRows)
        self.setLayout(self.formLayout)
        self.setFixedSize(260, 200)
        self.initWidgets()
        self.infoChnageCheck = False

    def closeEvent(self, event):
        # Change happen but some area is empty
        if (self.infoChnageCheck):
            if not (self.recheckLineEdit.text()) and not (
                    self.checkLineEdit.text()):
                event.accept()
            else:
                reply = myUI.MyMessageBox.question(self, "设置未保存", "是否放弃所作改动？",
                                                   myUI.MyMessageBox.Yes |
                                                   myUI.MyMessageBox.No,
                                                   myUI.MyMessageBox.No)
                if reply == myUI.MyMessageBox.Yes:
                    event.accept()
                else:
                    event.ignore()
        # No change happen
        else:
            event.accept()

    def initWidgets(self):
        # -----------------Initatialize Widget-----------------------------
        # input people's name
        self.departmentLabel = QLabel("部门名称", self)
        self.departmentLabel.setAlignment(Qt.AlignRight)
        self.departmentLabel.setFixedSize(60, 25)
        self.departmentLabel.setMargin(3)
        self.departmentLineEdit = QLineEdit(self)
        self.departmentLineEdit.setClearButtonEnabled(True)
        self.departmentLineEdit.setFixedSize(170, 25)
        self.departmentLineEdit.setText(
            global_control.project_settings["department_name"])
        self.departmentLineEdit.setPlaceholderText("输入部门名称")
        self.departmentLineEdit.textChanged.connect(self.infoChange)

        self.designerLabel = QLabel("设计人", self)
        self.designerLabel.setAlignment(Qt.AlignRight)
        self.designerLabel.setFixedSize(60, 25)
        self.designerLabel.setMargin(3)
        self.designerLineEdit = QLineEdit(self)
        self.designerLineEdit.setClearButtonEnabled(True)
        self.designerLineEdit.setFixedSize(170, 25)
        self.designerLineEdit.setText(
            global_control.project_settings[
                "user_name"])
        self.designerLineEdit.setPlaceholderText("输入姓名")
        self.designerLineEdit.textChanged.connect(self.infoChange)

        self.checkLabel = QLabel("校核人", self)
        self.checkLabel.setAlignment(Qt.AlignRight)
        self.checkLabel.setFixedSize(60, 25)
        self.checkLabel.setMargin(3)
        self.checkLineEdit = QLineEdit(self)
        self.checkLineEdit.setClearButtonEnabled(True)
        self.checkLineEdit.setFixedSize(170, 25)
        self.checkLineEdit.setPlaceholderText("输入姓名")
        self.checkLineEdit.textChanged.connect(self.infoChange)

        self.recheckLabel = QLabel("审核人", self)
        self.recheckLabel.setAlignment(Qt.AlignRight)
        self.recheckLabel.setFixedSize(60, 25)
        self.recheckLabel.setMargin(3)
        self.recheckLineEdit = QLineEdit(self)
        self.recheckLineEdit.setClearButtonEnabled(True)
        self.recheckLineEdit.setFixedSize(170, 25)
        self.recheckLineEdit.setPlaceholderText("输入姓名")
        self.recheckLineEdit.textChanged.connect(self.infoChange)

        # Buttons
        self.confirmBtn = QPushButton("确定(&S)", self)
        self.confirmBtn.setFixedSize(60, 25)
        self.cancelBtn = QPushButton("取消(&C)", self)
        self.cancelBtn.setFixedSize(60, 25)
        self.deleteBtn = QPushButton("删除当前预设", self)
        self.deleteBtn.setFixedHeight(25)
        self.HBoxLayout = QHBoxLayout(self)
        self.HBoxLayout.addWidget(self.cancelBtn)
        self.HBoxLayout.addWidget(self.deleteBtn)
        self.confirmBtn.clicked.connect(self.onClickConfirmBtn)
        self.cancelBtn.clicked.connect(self.onClickCancelBtn)
        self.deleteBtn.clicked.connect(self.onClickDeleteBtn)

        # New project
        projectName = global_control.project_settings[
            "current_running_project"]
        self.loadProjectLabel = QLabel("项目名称", self)
        self.loadProjectLabel.setAlignment(Qt.AlignRight)
        self.loadProjectLabel.setFixedSize(60, 25)
        self.loadProjectLabel.setMargin(3)
        self.loadProjectComboBox = QComboBox(self)
        self.loadProjectComboBox.setFixedSize(170, 25)
        self.loadProjectComboBox.setEditable(True)
        self.loadProjectComboBox.activated.connect(
            self.changeComboBoxSelection)
        self.loadProjectComboBox.editTextChanged.connect(self.infoChange)

        # Setting text
        self.loadProjectSettings(projectName)

        # Add widget into layout
        self.formLayout.addRow(self.loadProjectLabel, self.loadProjectComboBox)
        self.formLayout.addRow(self.departmentLabel, self.departmentLineEdit)
        self.formLayout.addRow(self.designerLabel, self.designerLineEdit)
        self.formLayout.addRow(self.checkLabel, self.checkLineEdit)
        self.formLayout.addRow(self.recheckLabel, self.recheckLineEdit)
        self.formLayout.addRow(self.confirmBtn, self.HBoxLayout)
        # Set focus to 校核人
        self.checkLineEdit.setFocus()

    def loadProjectSettings(self, projectKey=None):
        # Run program at least onece
        if projectKey:
            projectList = list(
                global_control.project_settings["projects"].keys())
            self.loadProjectComboBox.clear()
            self.loadProjectComboBox.addItem("<添加项目名称>")
            self.loadProjectComboBox.addItems(projectList)
            self.loadProjectComboBox.setCurrentText(projectKey)
            # Info exists
            if global_control.project_settings["projects"][projectKey][
                "校核人"] != "输入姓名":
                self.checkLineEdit.setText(
                    global_control.project_settings["projects"][projectKey][
                        "校核人"])
                self.recheckLineEdit.setText(
                    global_control.project_settings["projects"][projectKey][
                        "审核人"])
            # No Info
            else:
                pass
            self.checkLineEdit.setFocus()
        # First time of running program
        else:
            self.loadProjectComboBox.setCurrentText("<添加项目名称>")
            self.loadProjectComboBox.setFocus()

    def changeComboBoxSelection(self):
        self.comboBoxItems = getComboBoxItems(self.loadProjectComboBox)

        if self.loadProjectComboBox.currentText() == "<添加项目名称>":
            self.loadProjectComboBox.clearEditText()
            self.designerLineEdit.setText(
                global_control.project_settings["user_name"])
            self.checkLineEdit.setText("")
            self.recheckLineEdit.setText("")
        else:
            self.loadProjectSettings(self.loadProjectComboBox.currentText())

    def onClickConfirmBtn(self):
        # value check
        if not self.loadProjectComboBox.currentText() or \
                self.loadProjectComboBox.currentText() == "<添加项目名称>":
            self.loadProjectComboBox.clearEditText()
            myUI.MyMessageBox.critical(self, "添加项目错误", "项目名称不能为空",
                                       myUI.MyMessageBox.StandardButton.Ok,
                                       myUI.MyMessageBox.StandardButton.Ok)
            self.loadProjectComboBox.setFocus()
        elif not self.departmentLineEdit.text():
            myUI.MyMessageBox.critical(self, "添加项目错误", "部门名称不能为空",
                                       myUI.MyMessageBox.StandardButton.Ok,
                                       myUI.MyMessageBox.StandardButton.Ok)
            self.departmentLineEdit.setFocus()
        elif not self.designerLineEdit.text():
            myUI.MyMessageBox.critical(self, "添加项目错误", "设计人不能为空",
                                       myUI.MyMessageBox.StandardButton.Ok,
                                       myUI.MyMessageBox.StandardButton.Ok)
            self.designerLineEdit.setFocus()
        elif not self.checkLineEdit.text():
            myUI.MyMessageBox.critical(self, "添加项目错误", "校核人不能为空",
                                       myUI.MyMessageBox.StandardButton.Ok,
                                       myUI.MyMessageBox.StandardButton.Ok)
            self.checkLineEdit.setFocus()
        elif not self.recheckLineEdit.text():
            myUI.MyMessageBox.critical(self, "添加项目错误", "审核人不能为空",
                                       myUI.MyMessageBox.StandardButton.Ok,
                                       myUI.MyMessageBox.StandardButton.Ok)
            self.recheckLineEdit.setFocus()
        else:
            # Gathering info and write into json file
            currentProjectText = self.loadProjectComboBox.currentText()

            # Project info exist in settings
            if global_control.project_settings["projects"]:
                # Deleting projects info
                self.comboBoxItems = getComboBoxItems(
                    self.loadProjectComboBox)
                deleteItems = list(filter(
                    lambda settingItem: settingItem not in self.comboBoxItems,
                    global_control.project_settings["projects"].keys()))
                if deleteItems:
                    for item in deleteItems:
                        del global_control.project_settings["projects"][
                            item]
                # No deletion
                else:
                    pass

                # Check if the current item is brand new
                if currentProjectText not in global_control.project_settings[
                    "projects"].keys():
                    global_control.project_settings[
                        "current_running_project"] = currentProjectText
                    global_control.project_settings[
                        "department_name"] = self.departmentLineEdit.text()
                    global_control.project_settings[
                        "user_name"] = self.designerLineEdit.text()
                    global_control.project_settings["projects"][
                        currentProjectText] = {"runtime": 0,
                                               "校核人"    :
                                                   self.checkLineEdit.text(),
                                               "审核人"    : self.recheckLineEdit.text()}
                    write_settings(self, "project_settings")
                    self.infoChnageCheck = False
                    self.close()
                # Project info exist
                else:
                    # project cover info does not exist
                    if (global_control.project_settings["projects"][
                            currentProjectText]["校核人"] == "输入姓名") and (
                            global_control.project_settings["projects"][
                                currentProjectText]["审核人"] == "输入姓名"):
                        global_control.project_settings[
                            "department_name"] = self.departmentLineEdit.text()
                        global_control.project_settings[
                            "user_name"] = self.designerLineEdit.text()
                        global_control.project_settings["projects"][
                            currentProjectText][
                            "校核人"] = self.checkLineEdit.text()
                        global_control.project_settings["projects"][
                            currentProjectText][
                            "审核人"] = self.recheckLineEdit.text()
                        write_settings(self, "project_settings")
                        self.infoChnageCheck = False
                        self.close()
                    # project cover info exists
                    else:
                        reply = myUI.MyMessageBox.question(self, "更新项目信息",
                                                           "{}已存在当前预设信息中，是否进行更新？".format(
                                                               currentProjectText),
                                                           myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                                                           myUI.MyMessageBox.Yes)
                        if reply == myUI.MyMessageBox.Yes:
                            global_control.project_settings[
                                "current_running_project"] = currentProjectText
                            global_control.project_settings[
                                "department_name"] = \
                                self.departmentLineEdit.text()
                            global_control.project_settings[
                                "user_name"] = self.designerLineEdit.text()
                            global_control.project_settings["projects"][
                                currentProjectText][
                                "校核人"] = self.checkLineEdit.text()
                            global_control.project_settings["projects"][
                                currentProjectText][
                                "审核人"] = self.recheckLineEdit.text()
                            write_settings(self, "project_settings")
                            self.infoChnageCheck = False
                            self.close()
                        else:
                            pass

            # First time of running the program
            else:
                global_control.project_settings[
                    "current_running_project"] = currentProjectText
                global_control.project_settings[
                    "department_name"] = self.departmentLineEdit.text()
                global_control.project_settings[
                    "user_name"] = self.designerLineEdit.text()
                global_control.project_settings["projects"][
                    currentProjectText] = {"runtime": 0,
                                           "校核人"    :
                                               self.checkLineEdit.text(),
                                           "审核人"    : self.recheckLineEdit.text()}
                write_settings(self, "project_settings")
                self.infoChnageCheck = False
                self.close()

    def onClickCancelBtn(self):
        self.close()

    def onClickDeleteBtn(self):
        reply = myUI.MyMessageBox.question(self, "是否删除？", "是否删除该项目预设？",
                                           myUI.MyMessageBox.Yes |
                                           myUI.MyMessageBox.No,
                                           myUI.MyMessageBox.Yes)
        if reply == myUI.MyMessageBox.Yes:
            index = self.loadProjectComboBox.currentIndex()
            self.loadProjectComboBox.removeItem(index)
            self.comboBoxItems = getComboBoxItems(self.loadProjectComboBox)
            try:
                self.loadProjectComboBox.setCurrentIndex(1)
            except Exception:
                self.loadProjectComboBox.setCurrentIndex(0)

    def infoChange(self):
        self.infoChnageCheck = True


def askSetUpCoverInfo():
    """check to prompt the user to set project cover infomation"""
    # findout output file with insert conver func turned on
    if building_property == "公共建筑":
        pass  # 公共建筑跳过
    else:
        outputList = [check_file for check_file in
                      global_control.settings["output_mode"].keys() if
                      global_control.settings["output_mode"][
                          check_file] is True]
        insertCoverOutputFile = list(filter(lambda file: (
                global_control.settings[file][
                    "插入华蓝设计封面"] is True) if "计算书" in file else False,
                                            outputList))
        if insertCoverOutputFile:
            currentProject = global_control.project_settings[
                "current_running_project"]
            # when cover info does ont exists
            if \
                    global_control.project_settings["projects"][
                        currentProject][
                        "审核人"] == "输入姓名":
                reply = myUI.MyMessageBox.question(myUI.MyWidget(), "提示",
                                                   "当前项目“{"
                                                   "0}”的以下输出文件开启了“插入华蓝设计封面”的功能：\n\n{1}\n\n是否现在设置封面信息？下一次程序输出项目“{0}”的文件的时，会自动在{1}的华蓝封面处填写审核人与校核人的信息".format(
                                                       currentProject,
                                                       insertCoverOutputFile),
                                                   myUI.MyMessageBox.Yes |
                                                   myUI.MyMessageBox.No,
                                                   myUI.MyMessageBox.Yes)
                if reply == myUI.MyMessageBox.Yes:
                    widget = ProjectCoverWidget()
                    widget.exec_()
                else:
                    pass
            # when cover info exists pass
            else:
                pass
        # no output file with insert cover turned on
        else:
            pass
