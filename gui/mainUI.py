# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainUI.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *
import mylog


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(551, 382)
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font.setPointSize(10)
        MainWindow.setFont(font)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(10, 10, 531, 121))
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.checkBox = QCheckBox(self.groupBox)
        self.checkBox.setObjectName(u"checkBox")
        self.checkBox.setEnabled(False)
        self.checkBox.setCheckable(True)
        self.checkBox.setChecked(True)
        self.checkBox.setTristate(False)

        self.verticalLayout.addWidget(self.checkBox)

        self.checkBox_2 = QCheckBox(self.groupBox)
        self.checkBox_2.setObjectName(u"checkBox_2")
        self.checkBox_2.setChecked(True)

        self.verticalLayout.addWidget(self.checkBox_2)

        self.checkBox_3 = QCheckBox(self.groupBox)
        self.checkBox_3.setObjectName(u"checkBox_3")
        self.checkBox_3.setChecked(True)

        self.verticalLayout.addWidget(self.checkBox_3)

        self.checkBox_4 = QCheckBox(self.groupBox)
        self.checkBox_4.setObjectName(u"checkBox_4")
        self.checkBox_4.setChecked(True)

        self.verticalLayout.addWidget(self.checkBox_4)

        self.layoutWidget = QWidget(self.centralwidget)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 140, 521, 31))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.lineEdit = QLineEdit(self.layoutWidget)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setEnabled(True)
        self.lineEdit.setReadOnly(True)

        self.horizontalLayout.addWidget(self.lineEdit)

        self.customPathBtn = QPushButton(self.layoutWidget)
        self.customPathBtn.setObjectName(u"customPathBtn")

        self.horizontalLayout.addWidget(self.customPathBtn)

        self.openPathBtn = QPushButton(self.layoutWidget)
        self.openPathBtn.setObjectName(u"openPathBtn")

        self.horizontalLayout.addWidget(self.openPathBtn)

        self.layoutWidget1 = QWidget(self.centralwidget)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(210, 90, 321, 45))
        self.horizontalLayout_2 = QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 6)
        self.funBtn = QPushButton(self.layoutWidget1)
        self.funBtn.setObjectName(u"funBtn")
        self.funBtn.setMaximumSize(QSize(28, 16777215))

        self.horizontalLayout_2.addWidget(self.funBtn)

        self.aboutBtn = QPushButton(self.layoutWidget1)
        self.aboutBtn.setObjectName(u"aboutBtn")

        self.horizontalLayout_2.addWidget(self.aboutBtn)

        self.settingBtn = QPushButton(self.layoutWidget1)
        self.settingBtn.setObjectName(u"settingBtn")

        self.horizontalLayout_2.addWidget(self.settingBtn)

        self.outputBtn = QPushButton(self.layoutWidget1)
        self.outputBtn.setObjectName(u"outputBtn")

        self.horizontalLayout_2.addWidget(self.outputBtn)


        myHandler = mylog.Pyside2LogHandler(self.centralwidget)
        self.loggingPlainText = myHandler.widget
        self.loggingPlainText.setObjectName(u"loggingPlainText")
        self.loggingPlainText.setEnabled(True)
        self.loggingPlainText.setGeometry(QRect(10, 180, 531, 171))
        font1 = QFont()
        font1.setFamily(u"Consolas")
        font1.setPointSize(10)
        self.loggingPlainText.setFont(font1)
        self.loggingPlainText.setLineWrapMode(QPlainTextEdit.WidgetWidth)
        self.layoutWidget2 = QWidget(self.centralwidget)
        self.layoutWidget2.setObjectName(u"layoutWidget2")
        self.layoutWidget2.setGeometry(QRect(290, 20, 241, 31))
        self.horizontalLayout_3 = QHBoxLayout(self.layoutWidget2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.manualChooseModeBtn = QRadioButton(self.layoutWidget2)
        self.manualChooseModeBtn.setObjectName(u"manualChooseModeBtn")

        self.horizontalLayout_3.addWidget(self.manualChooseModeBtn)

        self.autoChooseModeBtn = QRadioButton(self.layoutWidget2)
        self.autoChooseModeBtn.setObjectName(u"autoChooseModeBtn")

        self.horizontalLayout_3.addWidget(self.autoChooseModeBtn)

        self.customKeywordBtn = QToolButton(self.layoutWidget2)
        self.customKeywordBtn.setObjectName(u"customKeywordBtn")
        self.customKeywordBtn.setMinimumSize(QSize(25, 25))

        self.horizontalLayout_3.addWidget(self.customKeywordBtn)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(150, 165, 210, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        brush2 = QBrush(QColor(255, 255, 255, 128))
        brush2.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush2)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        brush3 = QBrush(QColor(255, 255, 255, 128))
        brush3.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush3)
#endif
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        brush4 = QBrush(QColor(255, 255, 255, 128))
        brush4.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush4)
#endif
        self.statusbar.setPalette(palette)
        font2 = QFont()
        font2.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        self.statusbar.setFont(font2)
        self.statusbar.setStyleSheet(u"background-color: rgb(150, 165, 210);\n"
"color: rgb(255, 255, 255);")
        MainWindow.setStatusBar(self.statusbar)
        QWidget.setTabOrder(self.settingBtn, self.outputBtn)
        QWidget.setTabOrder(self.outputBtn, self.aboutBtn)
        QWidget.setTabOrder(self.aboutBtn, self.funBtn)
        QWidget.setTabOrder(self.funBtn, self.customPathBtn)
        QWidget.setTabOrder(self.customPathBtn, self.openPathBtn)
        QWidget.setTabOrder(self.openPathBtn, self.lineEdit)
        QWidget.setTabOrder(self.lineEdit, self.checkBox)
        QWidget.setTabOrder(self.checkBox, self.checkBox_2)
        QWidget.setTabOrder(self.checkBox_2, self.checkBox_3)
        QWidget.setTabOrder(self.checkBox_3, self.checkBox_4)
        QWidget.setTabOrder(self.checkBox_4, self.manualChooseModeBtn)
        QWidget.setTabOrder(self.manualChooseModeBtn, self.autoChooseModeBtn)
        QWidget.setTabOrder(self.autoChooseModeBtn, self.customKeywordBtn)
        QWidget.setTabOrder(self.customKeywordBtn, self.loggingPlainText)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"\u53bb\u4f60\u5927\u7237\u7684\u65af\u7ef4\u5c14\u8282\u80fdV0.0.9", None))
#if QT_CONFIG(tooltip)
        self.checkBox.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.checkBox.setStatusTip(QCoreApplication.translate("MainWindow", u"\u5fc5\u9700\u52fe\u9009\u7684\u9009\u9879", None))
#endif // QT_CONFIG(statustip)
        self.checkBox.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66.docx", None))
#if QT_CONFIG(tooltip)
        self.checkBox_2.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.checkBox_2.setStatusTip(QCoreApplication.translate("MainWindow", u"\u70b9\u51fb\u201c\u8bbe\u7f6e...\u201d\u6309\u94ae\u67e5\u770b\u66f4\u591a", None))
#endif // QT_CONFIG(statustip)
        self.checkBox_2.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa\u9694\u70ed\u68c0\u67e5\u8ba1\u7b97\u4e66.docx", None))
#if QT_CONFIG(tooltip)
        self.checkBox_3.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.checkBox_3.setStatusTip(QCoreApplication.translate("MainWindow", u"\u70b9\u51fb\u201c\u8bbe\u7f6e...\u201d\u6309\u94ae\u67e5\u770b\u66f4\u591a", None))
#endif // QT_CONFIG(statustip)
        self.checkBox_3.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u5ba1\u67e5\u8868.docx", None))
#if QT_CONFIG(tooltip)
        self.checkBox_4.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.checkBox_4.setStatusTip(QCoreApplication.translate("MainWindow", u"\u70b9\u51fb\u201c\u8bbe\u7f6e...\u201d\u6309\u94ae\u67e5\u770b\u66f4\u591a", None))
#endif // QT_CONFIG(statustip)
        self.checkBox_4.setText(QCoreApplication.translate("MainWindow", u"\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u4e13\u9879\u8bf4\u660e.xlsx", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa\u8def\u5f84", None))
#if QT_CONFIG(statustip)
        self.customPathBtn.setStatusTip(QCoreApplication.translate("MainWindow", u"\u81ea\u5b9a\u4e49\u8def\u5f84", None))
#endif // QT_CONFIG(statustip)
        self.customPathBtn.setText(QCoreApplication.translate("MainWindow", u"PushButton", None))
#if QT_CONFIG(statustip)
        self.openPathBtn.setStatusTip(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8f93\u51fa\u8def\u5f84", None))
#endif // QT_CONFIG(statustip)
        self.openPathBtn.setText(QCoreApplication.translate("MainWindow", u"PushButton", None))
        self.funBtn.setText(QCoreApplication.translate("MainWindow", u"!", None))
        self.aboutBtn.setText(QCoreApplication.translate("MainWindow", u"\u5173\u4e8e\u8f6f\u4ef6(&Q)", None))
        self.settingBtn.setText(QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e(&S)", None))
        self.outputBtn.setText(QCoreApplication.translate("MainWindow", u"\u8f93\u51fa(&A)", None))
        self.loggingPlainText.setPlainText("")
#if QT_CONFIG(statustip)
        self.manualChooseModeBtn.setStatusTip(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u8f93\u51fa\u65f6\uff0c\u4f1a\u8981\u6c42\u624b\u52a8\u9009\u62e9\u6240\u6709\u9700\u8981\u8f93\u51fa\u7c7b\u578b\u6587\u4ef6\u7684\u8def\u5f84", None))
#endif // QT_CONFIG(statustip)
        self.manualChooseModeBtn.setText(QCoreApplication.translate("MainWindow", u"\u624b\u52a8\u9009\u62e9\u6587\u4ef6", None))
#if QT_CONFIG(statustip)
        self.autoChooseModeBtn.setStatusTip(QCoreApplication.translate("MainWindow", u"\u81ea\u52a8\u5bfb\u627e\u4e0e\u7a0b\u5e8f\u540c\u4e00\u76ee\u5f55\u4e0b\u7684\u8f93\u51fa\u7c7b\u578b\u6587\u4ef6\uff0c\u6240\u4f9d\u636e\u7684\u5173\u952e\u5b57\u53ef\u81ea\u5b9a\u4e49\u8bbe\u7f6e", None))
#endif // QT_CONFIG(statustip)
        self.autoChooseModeBtn.setText(QCoreApplication.translate("MainWindow", u"\u81ea\u52a8\u9009\u62e9\u6587\u4ef6", None))
#if QT_CONFIG(statustip)
        self.customKeywordBtn.setStatusTip(QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e\u81ea\u52a8\u9009\u62e9\u6587\u4ef6\u65f6\u4f9d\u636e\u7684\u5173\u952e\u5b57", None))
#endif // QT_CONFIG(statustip)
        self.customKeywordBtn.setText(QCoreApplication.translate("MainWindow", u"...", None))
    # retranslateUi
