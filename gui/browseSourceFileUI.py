# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'browseSourceFile.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):

    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(458, 150)
        self.layoutWidget = QWidget(Form)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 441, 95))
        self.verticalLayout = QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 3, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(120, 0))

        self.horizontalLayout.addWidget(self.label)

        self.energyLineEdit = QLineEdit(self.layoutWidget)
        self.energyLineEdit.setObjectName(u"energyLineEdit")
        self.energyLineEdit.setEnabled(False)
        self.energyLineEdit.setReadOnly(True)

        self.horizontalLayout.addWidget(self.energyLineEdit)

        self.energyBtn = QToolButton(self.layoutWidget)
        self.energyBtn.setObjectName(u"energyBtn")
        self.energyBtn.setEnabled(False)
        self.energyBtn.setMinimumSize(QSize(0, 25))

        self.horizontalLayout.addWidget(self.energyBtn)

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_2 = QLabel(self.layoutWidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(120, 0))

        self.horizontalLayout_2.addWidget(self.label_2)

        self.thermalLineEdit = QLineEdit(self.layoutWidget)
        self.thermalLineEdit.setObjectName(u"thermalLineEdit")
        self.thermalLineEdit.setEnabled(False)
        self.thermalLineEdit.setReadOnly(True)

        self.horizontalLayout_2.addWidget(self.thermalLineEdit)

        self.thermalBtn = QToolButton(self.layoutWidget)
        self.thermalBtn.setObjectName(u"thermalBtn")
        self.thermalBtn.setEnabled(False)
        self.thermalBtn.setMinimumSize(QSize(0, 25))

        self.horizontalLayout_2.addWidget(self.thermalBtn)

        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_4 = QLabel(self.layoutWidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(120, 0))

        self.horizontalLayout_4.addWidget(self.label_4)

        self.exaLineEdit = QLineEdit(self.layoutWidget)
        self.exaLineEdit.setObjectName(u"exaLineEdit")
        self.exaLineEdit.setEnabled(False)
        self.exaLineEdit.setReadOnly(True)

        self.horizontalLayout_4.addWidget(self.exaLineEdit)

        self.exaBtn = QToolButton(self.layoutWidget)
        self.exaBtn.setObjectName(u"exaBtn")
        self.exaBtn.setEnabled(False)
        self.exaBtn.setMinimumSize(QSize(0, 25))

        self.horizontalLayout_4.addWidget(self.exaBtn)

        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.layoutWidget1 = QWidget(Form)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(290, 120, 158, 25))
        self.horizontalLayout_3 = QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.confirmBtn = QPushButton(self.layoutWidget1)
        self.confirmBtn.setObjectName(u"confirmBtn")

        self.horizontalLayout_3.addWidget(self.confirmBtn)

        self.cancelBtn = QPushButton(self.layoutWidget1)
        self.cancelBtn.setObjectName(u"cancelBtn")

        self.horizontalLayout_3.addWidget(self.cancelBtn)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(
            QCoreApplication.translate(
                "Form", u"\u9009\u62e9\u65af\u7ef4\u5c14\u6587\u6863", None))
        self.label.setText(
            QCoreApplication.translate(
                "Form", u"\u5efa\u7b51\u8282\u80fd\u8ba1\u7b97\u4e66", None))
        self.energyBtn.setText(QCoreApplication.translate("Form", u"...", None))
        self.label_2.setText(
            QCoreApplication.translate(
                "Form", u"\u9694\u70ed\u68c0\u67e5\u8ba1\u7b97\u4e66", None))
        self.thermalBtn.setText(QCoreApplication.translate(
            "Form", u"...", None))
        self.label_4.setText(
            QCoreApplication.translate(
                "Form",
                u"\u5efa\u7b51\u8282\u80fd\u8bbe\u8ba1\u5ba1\u67e5\u8868",
                None))
        self.exaBtn.setText(QCoreApplication.translate("Form", u"...", None))
        self.confirmBtn.setText(
            QCoreApplication.translate("Form", u"\u786e\u5b9a(&S)", None))
        self.cancelBtn.setText(
            QCoreApplication.translate("Form", u"\u53d6\u6d88(&C)", None))

    # retranslateUi
