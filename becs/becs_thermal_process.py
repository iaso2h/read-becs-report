import os
import erase_cover
import logging
import file_open
from docx import Document

# global vars
logger = logging.getLogger(name=__name__)
file_name = file_open.output_file_dict["隔热检查计算书"].file_name
file_base_name = file_open.output_file_dict["隔热检查计算书"].file_base_name
file_ext = file_open.output_file_dict["隔热检查计算书"].file_ext
docx = Document(os.path.join(file_open.wd, file_name))
erase_cover.erase("隔热检查计算书", docx, 8)
