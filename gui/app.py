import sys
import os

home = os.path.expanduser('~')
settings_dir = os.path.join(home, "BECS tool")
parentdir = os.getcwd()
sys.path.append(parentdir)
from PySide2.QtWidgets import *
from PySide2.QtGui import QPixmap, QFont, QIcon, QMovie
from PySide2.QtCore import Qt, QSize
from gui import myUI
from gui import aboutUI
from gui import mainUI
from gui import settings
from gui import departmentUI
from gui import UIUtil
from gui import customKeywordUI
import global_control
import logging
import mylog
import errors

logger = logging.getLogger(name=__name__)
global_control.settings_init()
# Calculate and prompting user information
checkBoxCount = []


def write_settings(parentWidget, settingsMode):
    try:
        global_control.write_settings(settingsMode)
    except Exception:
        if global_control.default_settings["全局设置"]["开发者模式"] is True:
            pass
        else:
            myUI.MyMessageBox.critical(
                parentWidget, "错误", "程序在写入设置发生错误,请确保以下路径是可读的\n{}".format(
                    global_control.settings_dir),
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.Yes)


def reset_default(parentUI):
    global_control.settings = global_control.default_settings
    if parentUI:
        write_settings(parentUI, "settings")


def convertPath(pathString, mode):
    """Convert \\ to / in given path string when in \\2/ mode
    Convert / to \\ in given path string when in /2\\ mode"""
    if mode == "\\2/":
        return pathString.replace("\\", "/")
    elif mode == "/2\\":
        return pathString.replace("/", "\\")
    else:
        pass


class MainUI(object):
    funClickTime = 0
    refreshKeywordUI = False
    settingChange = False
    app = QApplication(sys.argv)
    icon = QPixmap(parentdir + '/src/icons/icons8_tea.ico')

    def __init__(self):
        # First thing first
        self.mainWindow = myUI.MyMainWindow()
        self.ui = mainUI.Ui_MainWindow()
        self.ui.setupUi(self.mainWindow)
        self.mainWindow.setFixedSize(551, 381)
        self.mainWindow.setWindowIcon(MainUI.icon)
        self.mainWindow.setWindowTitle(f"去你大爷的斯维尔节能v{global_control.version}")
        self.ui.loggingPlainText.setFont(QFont("Consolas"))
        self.initBtnCheckState()
        self.initIcon()
        self.btnSlots()
        self.outputModeCheckSignal()
        self.mainWindow.show()
        self.ui.settingBtn.setFocus()
        self.checkDepartmentInfo()

        MainUI.app.exit(MainUI.app.exec_())

    def testPrompt(self):
        myUI.MyMessageBox.about(self.mainWindow, "test", "this")

    def initIcon(self):
        # Clear placeholder in .ui file
        self.ui.customPathBtn.setText("")
        self.ui.openPathBtn.setText("")
        self.ui.funBtn.setText("")

        UIUtil.setIcon(self.ui.customPathBtn, "icons8_add_folder", (30, 30))
        UIUtil.setIcon(self.ui.openPathBtn, "icons8_opened_folder", (30, 30))
        UIUtil.setIcon(self.ui.customKeywordBtn, "icons8_pass_fail", (30, 30))
        UIUtil.setIcon(self.ui.settingBtn, "icons8_edit_property")
        UIUtil.setIcon(self.ui.aboutBtn, "icons8_info_squared")
        UIUtil.setIcon(self.ui.outputBtn, "icons8_restart")
        UIUtil.setIcon(self.ui.funBtn, "icons8_puzzled", (30, 30))

    def btnSlots(self):

        def onClickBtn():
            # Reload when needed
            global_control.settings_init()
            # Reload these modules when hit the output btn
            reloadList = [
                "file_open", "becs", "becs.becs_energy_process",
                "becs.becs_thermal_process", "becs.becs_exa_process",
                "becs.becs_process_sheet", "output"
            ]
            for m in reloadList:
                if m in sys.modules:
                    del sys.modules[m]
            # Clear errror message
            errors.warn_message = []
            # Clear text in the plainTextEditor
            self.ui.loggingPlainText.clear()
            # Some Text
            logger.info(r"""
██████╗ ███████╗ ██████╗███████╗    ████████╗ ██████╗  ██████╗ ██╗     
██╔══██╗██╔════╝██╔════╝██╔════╝    ╚══██╔══╝██╔═══██╗██╔═══██╗██║     
██████╔╝█████╗  ██║     ███████╗       ██║   ██║   ██║██║   ██║██║     
██╔══██╗██╔══╝  ██║     ╚════██║       ██║   ██║   ██║██║   ██║██║     
██████╔╝███████╗╚██████╗███████║       ██║   ╚██████╔╝╚██████╔╝███████╗
╚═════╝ ╚══════╝ ╚═════╝╚══════╝       ╚═╝    ╚═════╝  ╚═════╝ ╚══════╝
            """)
            # Start analysing
            import file_open
            if global_control.settings["全局设置"]["开发者模式"] is True:
                logger.info("Develop mode on")
                file_open.analyse_file()
                MainUI.funClickTime = 0
                UIUtil.setIcon(self.ui.funBtn, "icons8_puzzled", (30, 30))
            else:
                try:
                    logger.info("Develop mode off")
                    file_open.analyse_file()
                    MainUI.funClickTime = 0
                    UIUtil.setIcon(self.ui.funBtn, "icons8_puzzled", (30, 30))
                except Exception as exc:
                    logger.error(errors.exception_Text)
                    logger.debug(exc)
                    logger.exception("Exception")
                    # setup.restart_program()

        def onClickSetting():
            widget = settings.SettingWidget()
            # widget.exec_()
            # if MainUI.settingChange:
            #     global_control.settings_init()
            #     self.initBtnCheckState()
            #     MainUI.settingChange = False
            widget.show()

        def onClickAbout():
            widget = AboutWidget()
            widget.exec_()

        def refreshPath():
            showPath = global_control.settings["output_dir"]
            showPath = convertPath(showPath, "/2\\")
            self.ui.lineEdit.setText(showPath)

        def setPath():
            reply = QFileDialog.getExistingDirectory(self.ui.centralwidget,
                                                     "请选择文件夹作为输出路径",
                                                     f"{home}\\Desktop")
            if reply == convertPath(parentdir, "\\2/"):
                myUI.MyMessageBox.critical(self.mainWindow, "路径选择错误",
                                           "路径不能与程序同一路径，避免文件被覆盖或与其他文件混淆",
                                           myUI.MyMessageBox.StandardButton.Ok,
                                           myUI.MyMessageBox.StandardButton.Ok)
            elif not reply:
                pass
            else:
                global_control.settings["output_dir"] = reply
                write_settings(self, "settings")
                refreshPath()

        def onClickCustomKeyword():
            widget = CustomMatchKeywordWidget()
            widget.exec_()
            if MainUI.settingChange is True:
                write_settings(self.mainWindow, "settings")
                global_control.settings_init()
                MainUI.settingChange = False
            if MainUI.refreshKeywordUI is True:
                self.initBtnCheckState()
                MainUI.refreshKeywordUI = False

        def onClickFun():
            MainUI.funClickTime += 1
            if MainUI.funClickTime == 1:
                UIUtil.setIcon(self.ui.funBtn, "icons8_anime_emoji", (30, 30))
            elif MainUI.funClickTime == 2:
                UIUtil.setIcon(self.ui.funBtn, "icons8_fat_emoji", (30, 30))
            elif MainUI.funClickTime == 3:
                UIUtil.setIcon(self.ui.funBtn, "icons8_shocker_emoji", (30, 30))
            elif MainUI.funClickTime == 4:
                UIUtil.setIcon(self.ui.funBtn, "icons8_angry_emoji", (30, 30))
            elif MainUI.funClickTime == 5:
                UIUtil.setIcon(self.ui.funBtn, "icons8_cool", (30, 30))
            else:
                pass
            # Trigger animation gif
            if MainUI.funClickTime >= 5:
                widget = myUI.MyDialog()
                widget.setWindowTitle("Before&After")
                widget.setFixedSize(1240, 698)
                label = QLabel(widget)
                gif = QMovie(f"{parentdir}/md/comp.gif")
                label.setMovie(gif)
                gif.start()
                widget.exec_()
            else:
                pass

        refreshPath()

        self.ui.customPathBtn.clicked.connect(setPath)
        self.ui.openPathBtn.clicked.connect(UIUtil.openPath)
        self.ui.settingBtn.clicked.connect(onClickSetting)
        self.ui.aboutBtn.clicked.connect(onClickAbout)
        self.ui.outputBtn.clicked.connect(onClickBtn)
        self.ui.customKeywordBtn.clicked.connect(onClickCustomKeyword)
        self.ui.funBtn.clicked.connect(onClickFun)

    def initBtnCheckState(self):
        # Checkbox text
        self.ui.checkBox.setText(
            global_control.settings["auto_match_keywords"]["建筑节能计算书"] + ".docx")
        self.ui.checkBox_2.setText(
            global_control.settings["auto_match_keywords"]["隔热检查计算书"] + ".docx")
        self.ui.checkBox_3.setText(
            global_control.settings["auto_match_keywords"]["建筑节能设计审查表"] +
            ".docx")

        # Checkbox tick
        if global_control.settings["auto_match"] is True:
            self.ui.autoChooseModeBtn.setChecked(True)
        else:
            self.ui.manualChooseModeBtn.setChecked(True)

        if global_control.settings["output_mode"]["建筑节能计算书"] is True:
            self.ui.checkBox.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.checkBox.setCheckState(Qt.CheckState.Unchecked)
        if global_control.settings["output_mode"]["隔热检查计算书"] is True:
            self.ui.checkBox_2.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.checkBox_2.setCheckState(Qt.CheckState.Unchecked)
        if global_control.settings["output_mode"]["建筑节能设计审查表"] is True:
            self.ui.checkBox_3.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.checkBox_3.setCheckState(Qt.CheckState.Unchecked)
        if global_control.settings["output_mode"]["建筑节能设计专项说明"] is True:
            self.ui.checkBox_4.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.checkBox_4.setCheckState(Qt.CheckState.Unchecked)

    def outputModeCheckSignal(self):

        def onClickBtn():
            if self.ui.checkBox.checkState() == Qt.CheckState.Unchecked:
                global_control.settings["output_mode"]["建筑节能计算书"] = False
            else:
                global_control.settings["output_mode"]["建筑节能计算书"] = True
            global_control.write_settings("settings")
            write_settings(self.mainWindow, "settings")

        def onClickBtn2():
            if self.ui.checkBox_2.checkState() == Qt.CheckState.Unchecked:
                global_control.settings["output_mode"]["隔热检查计算书"] = False
            else:
                global_control.settings["output_mode"]["隔热检查计算书"] = True
            write_settings(self.mainWindow, "settings")

        def onClickBtn3():
            if self.ui.checkBox_3.checkState() == Qt.CheckState.Unchecked:
                if (global_control.settings["output_mode"]["建筑节能设计专项说明"] is True
                        and global_control.settings["建筑节能设计专项说明"]
                    ["sheet_output_mode"]["节能设计指标一览表"] is True) or (
                        global_control.settings["output_mode"]["建筑节能设计专项说明"] is
                        True and global_control.settings["建筑节能设计专项说明"]
                        ["sheet_output_mode"]["工程概况"] is True):
                    self.ui.checkBox_3.setCheckState(Qt.CheckState.Checked)
                    global checkBoxCount
                    if len(checkBoxCount) == 3:
                        checkBoxCount = []
                    else:
                        pass
                    checkBoxCount.append(self.ui.checkBox_3.objectName())

                    # when user click the button 3 times
                    if checkBoxCount.count(
                            self.ui.checkBox_3.objectName()) == 3:
                        myUI.MyMessageBox.about(
                            self.mainWindow, "提示", "当以下设置之一开启时无法取消：\n\n"
                            "☑  输出-->输出建筑节能设计专项说明\n"
                            "☑  设置-->建筑节能设计专项说明-->工程概况\n"
                            "☑  "
                            "设置-->建筑节能设计专项说明-->节能设计指标一览表")
                        global_control.settings["output_mode"][
                            "建筑节能设计审查表"] = True
                    # when all 3 elements in checkBoxCount is not the same
                    else:
                        pass
                else:
                    global_control.settings["output_mode"]["建筑节能设计审查表"] = False
            else:
                global_control.settings["output_mode"]["建筑节能设计审查表"] = True

            global_control.write_settings("settings")
            write_settings(self.mainWindow, "settings")

        def onClickBtn4():
            if self.ui.checkBox_4.checkState() == Qt.CheckState.Unchecked:
                global_control.settings["output_mode"]["建筑节能设计专项说明"] = False
            else:
                if (global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"]
                    ["节能设计指标一览表"] is
                        True) or (global_control.settings["建筑节能设计专项说明"]
                                  ["sheet_output_mode"]["工程概况"] is True):
                    self.ui.checkBox_3.setCheckState(Qt.CheckState.Checked)
                    global_control.settings["output_mode"]["建筑节能设计审查表"] = True
                else:
                    pass
                global_control.settings["output_mode"]["建筑节能设计专项说明"] = True

            global_control.write_settings("settings")
            write_settings(self.mainWindow, "settings")

        def onClickMatch():
            if self.ui.autoChooseModeBtn.isChecked() is True:
                global_control.settings["auto_match"] = True
            else:
                global_control.settings["auto_match"] = False
            write_settings(self.mainWindow, "settings")

        self.ui.checkBox.clicked.connect(onClickBtn)
        self.ui.checkBox_2.clicked.connect(onClickBtn2)
        self.ui.checkBox_3.clicked.connect(onClickBtn3)
        self.ui.checkBox_4.clicked.connect(onClickBtn4)

        self.ui.manualChooseModeBtn.toggled.connect(onClickMatch)
        self.ui.autoChooseModeBtn.toggled.connect(onClickMatch)

    def checkDepartmentInfo(self):
        # ask the user to initialize the department info
        if global_control.project_settings["department_setup_ask"][
            0] is False and \
                global_control.project_settings["department_setup_ask"][1] < 3:
            if global_control.project_settings["department_setup_ask"][1] == 0:
                text = "这是你首次启动该程序，是否开始输入设置部门名称？"
            else:
                text = "是否开始输入设置部门名称？还有{}次提醒次数".format(
                    3 -
                    global_control.project_settings["department_setup_ask"][1])

            reply = myUI.MyMessageBox.question(
                self.mainWindow, "设置部门名称", text,
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.Yes)
            if reply == myUI.MyMessageBox.Yes:
                widget = DepartmentWidget()
                widget.exec_()
            else:
                global_control.project_settings["department_setup_ask"][1] += 1
                write_settings(self.mainWindow, "project_settings")


class CustomMatchKeywordWidget(myUI.MyDialog):

    def __init__(self):
        super().__init__()
        self.ui = customKeywordUI.Ui_Form()
        self.ui.setupUi(self)
        self.ui.label = \
            "以下输入框内的关键字是程序在“自动选择文件”模式下执行输出命令时会在程序目录自动搜索的关键字，你可以在此设置自定义关键字，程序会在设置确定保存后以新的设置来执行命令。\n\n比如：当项目名称为“ABC123#大楼”时会自动检索程序目录下的“ABC123#大楼{}.docx”\n".format(
                global_control.settings["auto_match_keywords"]["建筑节能计算书"])
        self.setFixedSize(541, 326)
        self.initLineEditText()
        # connect slot and signals
        self.ui.energyLineEdit.textEdited.connect(self.changeSetting)
        self.ui.thermLineEdit.textEdited.connect(self.changeSetting)
        self.ui.exaLineEdit.textEdited.connect(self.changeSetting)
        self.ui.confirmBtn.clicked.connect(self.onClickConfirmBtn)
        self.ui.cancelBtn.clicked.connect(self.onClickCloseBtn)
        self.ui.defaultBtn.clicked.connect(self.onClickDefaultBtn)

    def initLineEditText(self):
        self.ui.energyLineEdit.setText(
            global_control.settings["auto_match_keywords"]["建筑节能计算书"])
        self.ui.thermLineEdit.setText(
            global_control.settings["auto_match_keywords"]["隔热检查计算书"])
        self.ui.exaLineEdit.setText(
            global_control.settings["auto_match_keywords"]["建筑节能设计审查表"])

    def onClickDefaultBtn(self):
        self.ui.energyLineEdit.setText("建筑节能计算书")
        self.ui.thermLineEdit.setText("隔热检查计算书")
        self.ui.exaLineEdit.setText("建筑节能设计审查表")
        MainUI.settingChange = True

    def onClickConfirmBtn(self):
        if self.ui.energyLineEdit.text() != "" and \
                self.ui.thermLineEdit.text() != "" and \
                self.ui.exaLineEdit.text() != "":
            global_control.settings["auto_match_keywords"][
                "建筑节能计算书"] = self.ui.energyLineEdit.text()
            global_control.settings["auto_match_keywords"][
                "隔热检查计算书"] = self.ui.thermLineEdit.text()
            global_control.settings["auto_match_keywords"][
                "建筑节能设计审查表"] = self.ui.exaLineEdit.text()
            MainUI.refreshKeywordUI = True
            self.close()
        # Line edit can not empty
        else:
            myUI.MyMessageBox.about(self, "设置未完善", "输入框内文字不能为空")

    def onClickCloseBtn(self):
        self.close()

    @staticmethod
    def changeSetting():
        MainUI.settingChange = True


class AboutWidget(myUI.MyDialog):

    def __init__(self):
        super(AboutWidget, self).__init__()

        def openReadMe():
            import webbrowser
            readmePath = parentdir + os.path.sep + "README.html"
            webbrowser.open(readmePath)

        def onClickClose():
            self.close()

        def setIcon(widget, iconName, widgetSize=None):
            iconPath = parentdir + os.path.sep + "src" + os.path.sep + \
                       "icons" + os.path.sep + iconName + ".ico"
            icon = QIcon()
            icon.addFile(iconPath, QSize(), QIcon.Normal, QIcon.Off)
            widget.setIcon(icon)
            widget.setIconSize(QSize(20, 20))
            if widgetSize:
                widget.setFixedSize(widgetSize[0], widgetSize[1])
            else:
                pass

        ui = aboutUI.Ui_Form()
        ui.setupUi(self)
        self.setWindowModality(Qt.ApplicationModal)
        self.setFixedSize(421, 141)
        ui.label_2.setOpenExternalLinks(True)
        ui.label_4.setOpenExternalLinks(True)
        ui.label_6.setText(global_control.version)
        ui.pushButton.clicked.connect(onClickClose)
        setIcon(ui.pushButton, "icons8_cancel")
        ui.openReadMe.clicked.connect(openReadMe)
        setIcon(ui.openReadMe, "icons8_globe")


class TestLogWidget(myUI.MyDialog):

    def __init__(self, parentWeiget):
        super().__init__()

        self.GUIHandler = mylog.Pyside2LogHandler(self)
        self.widget = self.GUIHandler.widget
        self._button = QPushButton(parentWeiget)
        self._button.setText('Test Me')

        layout = QVBoxLayout()
        self.setLayout(layout)
        layout.addWidget(self.GUIHandler.widget)
        layout.addWidget(self._button)

        # Connect signal to slot
        self._button.clicked.connect(self.test)

    @staticmethod
    def test():
        logging.debug('damn, a bug')
        logging.info('something to remember')
        logging.warning('that\'s not right')
        logging.error('foobar')


class DepartmentWidget(myUI.MyDialog):

    def __init__(self):
        super(DepartmentWidget, self).__init__()
        self.ui = departmentUI.Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("设置部门名称")
        self.setWindowIcon(MainUI.icon)
        self.setFixedSize(220, 80)
        self.ui.lineEdit.setText(
            global_control.project_settings["department_name"])
        self.ui.lineEdit.setFocus()
        self.ui.confirmBtn.clicked.connect(
            lambda: self.btnDialogWidget(self.ui.confirmBtn))
        self.ui.cancelBtn.clicked.connect(
            lambda: self.btnDialogWidget(self.ui.cancelBtn))
        self.saveChange = False

    def closeEvent(self, event):
        if self.saveChange:
            event.accept()
        else:
            answer = myUI.MyMessageBox.question(
                self, "确认退出", "是否放弃填写部门名称？",
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.No)
            if answer == myUI.MyMessageBox.Yes:
                global_control.project_settings["department_setup_ask"][1] += 1
                write_settings(self, "project_settings")
                event.accept()
            else:
                event.ignore()

    def btnDialogWidget(self, btn):
        if btn == self.ui.confirmBtn:
            if not self.ui.lineEdit.text():
                myUI.MyMessageBox.about(self, "输入错误", "输入框信息不能为空")
                self.ui.lineEdit.setFocus()
            else:
                global_control.project_settings[
                    "department_name"] = self.ui.lineEdit.text()
                global_control.project_settings["department_setup_ask"][
                    0] = True
                write_settings(self, "project_settings")
                self.saveChange = True
                self.close()
        else:
            self.close()
