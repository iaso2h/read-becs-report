﻿/* Compiled under MSVC */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
    // Check line arg validation
    if (argc != 3) {
        printf("Expect 3 arguments, %d argument(s) provided", argc);
        system("pause");
        exit(1);
    }

    FILE *f = NULL;
    FILE *fCpy = NULL;
    char fileName[81] = {0};
    char fileCpyName[85] = {0};
    char mode[21] = {0};
    strncpy_s(fileName, 81, argv[1], 80);
    sprintf_s(fileCpyName, 85, "%s_temp", fileName);
    strncpy_s(mode, 11, argv[2], 10);
    for (int i = 0; (mode[i] = (char)tolower(mode[i])) != '\0'; i += 1) continue;

    // Open target file and create temp file
    errno_t err = fopen_s(&f, fileName, "r");
    if (err) {
        perror("fopen_s()");
        system("pause");
        exit(1);
    }
    errno_t errCpy = fopen_s(&fCpy, fileCpyName, "w");
    if (errCpy) {
        perror("fopen_s()");
        system("pause");
        exit(1);
    }
    // Table header
    if (strcmp(mode, "cn") == 0) fputs("目录\n", fCpy);
    else if (strcmp(mode, "en") == 0) fputs("Table of Contents\n", fCpy);
    else {
        printf(
            "Cannot recognize 3rd argument, enter \"Cn\" or \"En\" to execute the program and print the toc string in that language mode");
        fclose(f);
        fclose(fCpy);
        remove(fileCpyName);
        system("pause");
        exit(1);
    }
    fputs("==\n", fCpy);
    // Table body
    int headingCount[8] = {0}; // h1, h2, h3 ... h8
    int poundCount = 0;
    char ch;
    char heading[81] = {0};
    while ((ch = (char)fgetc(f)) != EOF) {
        if (ch == '#') // Check heading
            poundCount += 1;
        else if (ch == ' ' && poundCount != 0) {
            headingCount[poundCount - 1] += 1;
            if (fgets(heading, 81, f) == NULL) perror("fgets()");
            heading[strlen(heading) - 1] = '\0'; // get rid of '\n'
            if (poundCount != 1) {
                for (int i = 0; i < poundCount - 1; i += 1) {
                    fprintf(fCpy, "  ");
                }
            }
            fprintf(fCpy, "* [%s](#%s)\n", heading, heading);
            poundCount = 0;                         // Reinitial pound count for next heading
        } else if (poundCount != 0) poundCount = 0; // Reinitial pound count when encountering fake heading
    }
    fputc('\n', fCpy);

    // Copy Contents in source file to fCpy file
    fseek(f, 0, SEEK_SET);
    while ((ch = (char)fgetc(f)) != EOF) fputc(ch, fCpy);
    // Wrapping up
    fclose(f);
    fclose(fCpy);
    remove(fileName);
    errno_t err_rename = rename(fileCpyName, fileName);
    if (err_rename) {
        perror("rename()");
        exit(1);
    }
    printf("TOC created successfully");
    printf("\n");
    system("pause");
    return 0;
}
