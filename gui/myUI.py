import global_control

from PySide2.QtWidgets import QMainWindow, QMessageBox, QWidget, QDialog
from PySide2.QtGui import QFont, QPixmap, Qt

parentdir = global_control.wd


class MyMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(10)
        self.setFont(font1)
        self.setWindowIcon(QPixmap(parentdir + '/src/icons/icons8_tea.ico'))

class MyWidget(QWidget):
    def __init__(self):
        super().__init__()
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(10)
        self.setFont(font1)
        self.setWindowIcon(QPixmap(parentdir + '/src/icons/icons8_tea.ico'))


class MyDialog(QDialog):
    def __init__(self):
        super().__init__()
        font1 = QFont()
        font1.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        font1.setPointSize(10)
        self.setWindowModality(Qt.ApplicationModal)
        self.setFont(font1)
        self.setWindowIcon(QPixmap(parentdir + '/src/icons/icons8_tea.ico'))


class MyMessageBox(QMessageBox):
    def __init__(self):
        super().__init__(QWidget())


if __name__ == '__main__':
    from PySide2.QtWidgets import QApplication, QLineEdit
    import sys

    app = QApplication(sys.argv)
    widget1 = MyWidget()
    lineEdit = QLineEdit(widget1)
    lineEdit.setText("Nunquam fallere index.")
    widget1.show()
    sys.exit(app.exec_())
