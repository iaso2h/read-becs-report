import global_control
import logging
import openpyxl as xl
import errors

from becs import becs_energy_process as energy

if global_control.settings["全局设置"]["命令窗口模式"] is False:
    from gui import myUI

    def promptError(input_type):
        myWidget = myUI.MyWidget()
        myUI.MyMessageBox.critical(myWidget, "匹配错误", input_type,
                                   myUI.MyMessageBox.Ok)


logger = logging.getLogger(name=__name__)
parentdir = global_control.wd
logger.info("Searching templates in {}".format(parentdir))
wb = xl.load_workbook(r"{}/src/模板_建筑节能设计专项说明.xlsx".format(parentdir))

# YaHei Style
cell_style1 = xl.styles.NamedStyle(name="YaHei")
cell_style1.font = xl.styles.Font(name='微软雅黑')
cell_style1.alignment = xl.styles.Alignment(horizontal='center',
                                            vertical='center')
cell_style1.number_format = "@"
cell_style1.border = xl.styles.Border(left=xl.styles.Side(style='thin'),
                                      right=xl.styles.Side(style='thin'),
                                      top=xl.styles.Side(style='thin'),
                                      bottom=xl.styles.Side(style='thin'))
climate_area_sheet = {
    "夏热冬暖地区南区": "广西居住建筑（夏热冬暖地区南区）节能设计指标一览表",
    "夏热冬冷地区": "广西居住建筑（夏热冬冷地区）节能设计指标一览表",
    "夏热冬暖地区北区": "广西居住建筑（夏热冬暖地区北区）节能设计指标一览表"
}
warn_cell_address = []
working_sheet = None
# Add Style
wb.add_named_style(cell_style1)


def debug_export_sheet():
    """Temporarily output xlsx for developing use"""
    wb.save(r"{}/test_output.xlsx".format(parentdir))
    print("Output successfully!")


# noinspection PyUnresolvedReferences
def sheet_apply_style(input_range=None, style_name="YaHei"):
    """Apply 微软雅黑 style to sheets, and highlight warn cell"""
    global warn_cell_address
    global working_sheet
    if not input_range:
        for cells in working_sheet[working_sheet.dimensions]:
            for cell in cells:
                if cell.row != 1:
                    cell.style = style_name
                else:
                    pass
    else:
        for cells in working_sheet[input_range]:
            for cell in cells:
                if cell.row != 1:
                    cell.style = style_name
                else:
                    pass
    # NOTE: precision need to setup before applying style
    # noinspection PyUnresolvedReferences
    for addr in warn_cell_address:
        working_sheet.cell(addr[0], addr[1]).style = "Bad"
    warn_cell_address = []


def check_import_exa():
    """Check if 建筑节能设计审查表 found, and return True and False"""
    from file_open import Match_file
    # TODO: support custom keyword
    Match_file.refresh_dir_file()
    exa_file = Match_file((0, "建筑节能设计审查表"))
    # File found
    if Match_file.stop_process is False:
        Match_file.stop_process = False
        return True
        # TODO: support prefix collision check
    # File not found
    else:
        del exa_file
        Match_file.stop_process = False
        return False


def precision_num(input_num):
    """eg: input_num = 0, return "0",
        input_num = 2, return "0.00",
        input_num = 5, return "0.00000",
    """
    if input_num == 0:
        precision = "0"
    else:
        precision = "0." + "0" * input_num
    return precision


def change_column_precision(
        worksheet,
        num_text,
        column_index=1,
        start_row=1,
        end_row_index=50,
):
    """change the number format of specific column letter"""
    for cells in worksheet.iter_rows(min_col=column_index,
                                     min_row=start_row,
                                     max_row=end_row_index - 1):
        for cell in cells:
            cell.number_format = num_text


def try_num(num, mode):
    if mode == "float":
        try:
            result = float(num)
            return result
        except Exception:
            return num
    if mode == "integer":
        try:
            result = int(num)
            return result
        except Exception:
            return num
    else:
        return num


def merge_row_gap(worksheet, column_index, start_row, end_row):
    """merge the gap between tow two per column"""
    gap_list = [
        r_index for r_index, cells_in_tuple in enumerate(
            worksheet.iter_rows(min_row=start_row,
                                max_row=end_row,
                                min_col=column_index,
                                max_col=column_index), start_row)
        if cells_in_tuple[0].value or cells_in_tuple[0].value == 0
    ]
    # hypothesize the row_index = end_row + 1 also contain value
    gap_list.insert(len(gap_list), end_row + 1)

    for list_index, r_index in enumerate(gap_list):
        if list_index != len(gap_list) - 1 and gap_list[list_index] != gap_list[
                list_index + 1]:
            worksheet.merge_cells(range_string=None,
                                  start_row=gap_list[list_index],
                                  start_column=column_index,
                                  end_row=gap_list[list_index + 1] - 1,
                                  end_column=column_index)


# noinspection PyUnresolvedReferences
def warn_message_cell(row, min_column, max_column, messsage):
    global working_sheet
    """Write some text in the specific area and apply a style to it"""
    working_sheet.cell(row, min_column, messsage)
    working_sheet.merge_cells(range_string=None,
                              start_row=row,
                              start_column=min_column,
                              end_row=row,
                              end_column=max_column)
    for c_index in range(min_column, max_column):
        working_sheet.cell(row, c_index).style = "Bad"


def dwg_project_info_sheet():

    def process():
        from becs import becs_exa_report as exa
        import statistics
        global working_sheet
        # 填写工程概况
        wb.active = wb.sheetnames.index('工程概况')
        ws1 = wb.active
        working_sheet = ws1
        ws1.cell(3, 1, f"1. 工程名称：{energy.project['name']}")
        ws1.cell(4, 1, f"2. 建设地点：{energy.project['location']}")
        if energy.building_property == "居住建筑":
            ws1.cell(5, 1, f"3. 所在气候区域：{exa.climate_area}")
        else:
            warn_cell_address.append((5, 1))
            pass  # 公共建筑不填此项
        ws1.cell(6, 1, f"4. 建筑朝向：北向角度：{energy.building['orientation']}°")
        ws1.cell(7, 1, f"5. 使用功能：{energy.building_property}")
        ws1.cell(8, 1, f"6. 结构类型：{energy.building['structure']}")
        ws1.cell(
            9, 1, f"7. 建筑层数：地上{energy.building['storey'][0]}层,"
            f"地下 {energy.building['storey'][1]}层（节能计算层数） 建筑面积："
            f"{energy.building['area']}（节能计算面积）")
        ws1.cell(10, 1, f"8. 体形系数：{round(energy.building['volume_factor'], 2)}")
        ws1.cell(8, 2, f"（1）朝向：北向角度：{energy.building['orientation']}°")

        # 填写“外围护结构主要节能措施”
        def general_energy_process():
            roof_count = energy.component_count["屋顶构造"]
            roof_ratio = [  # noqa
                # TODO: analyse and fill the form
                float(energy.table_k_d_roof.cell(r_index, 2).text)
                for r_index in range(1, roof_count +
                                     1)  # ws1.cell(11, 2, f"（1）屋顶：{}")
                # ws1.cell(12, 2, f"（2）外墙：{}")
                # ws1.cell(13, 2, f"（3）外门窗：{}")
            ]
            warn_cell_address.append((11, 2))
            warn_cell_address.append((12, 2))
            warn_cell_address.append((13, 2))

        # -------------------填写“外遮阳措施”
        # 东或西向[有]遮阳措施的情况
        if any((energy.shading_win_constr["east"],
                energy.shading_win_constr["west"])):
            shading_win_src = []
            for shading_win in (energy.shading_win_constr["east"],
                                energy.shading_win_constr["west"]):
                if shading_win:
                    shading_win_src += shading_win

            shading_list = [single_list[2] for single_list in shading_win_src]
            most_common_shading = statistics.mode(shading_list)
            ws1.cell(2, 3, f"（4）外遮阳措施：{most_common_shading}")
        # 东或西向[没有]遮阳措施的情况
        else:
            warn_cell_address.append((2, 3))
        # ws1.cell(9, 2, f"（2）自然通风：{energy.Project_info.北向角度}")
        # ws1.cell(8, 3, f"1. ：{energy.Project_info.北向角度}")
        # ws1.cell(9, 3, f"2. ：{energy.Project_info.北向角度}")
        # ws1.cell(10, 3, f"3. {energy.Project_info.北向角度}")
        if energy.building_property == "居住建筑":
            ws1.cell(
                3, 3, f"（5）反射隔热饰面应用情况：外墙颜色："
                f"{exa.exa_table._cells[73].text}太阳辐射吸收系数修正前ρ="
                f"{exa.exa_table._cells[74].text}    ，修正后ρ=    "
                f"{exa.exa_table._cells[75].text}")
        else:
            pass  # 公共建筑跳过
        warn_cell_address.append((3, 3))
        warn_cell_address.append((9, 2))
        warn_cell_address.append((9, 3))
        warn_cell_address.append((8, 3))
        warn_cell_address.append((10, 3))
        sheet_apply_style()
        warn_message_cell(20, 1, 5, "请自行确认自行填写红色部分")
        ws1.sheet_properties.tabColor = 'FF00B050'

    # Check whether exa file is in output list or not
    if global_control.settings["output_mode"]["建筑节能设计审查表"] is False:
        while True:
            # TODO: manual select
            logger.warning(f"Cannot find 建筑节能设计审查表 document in {parentdir}")
            reply = myUI.MyMessageBox.question(
                myUI.MyWidget(), "提示",
                "已检测到“设置-->建筑节能设计专项说明-->填写工程概况”为勾选状态，要使用该功能，程序需要读取包含“建筑节能设计审查表”字样的文件\n\n请当在程序目录放置好包含“建筑节能设计审查表”字样的后，\n点击Yes继续，或者点击No跳过使用“填写工程概括”的功能",
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.Yes)
            # Yes
            if reply == myUI.MyMessageBox.Yes:
                check_exa = check_import_exa()
                if check_exa is True:
                    process()
                    # End loop
                    break
                else:
                    # Reset status to default and continue the next iteration
                    continue
            # No
            else:
                wb.remove_sheet(wb.get_sheet_by_name('工程概况'))
                break
    else:
        process()


def dwg_thermal_sheet():
    """填写外围护结构构造及热工性能参数表"""
    global working_sheet
    wb.active = wb.sheetnames.index('外围护结构构造及热工性能参数表')
    ws1 = wb.active
    working_sheet = ws1
    header_names = (("部位", ""), ("构造层", ""), ("密度ρ", "kg/m^U3^U"),
                    ("厚度δ", "mm"), ("导热系数λ", "W/(m·k)"), ("蓄热系数S", "W/(m·k)"),
                    ("热阻R", "m^U2^U·k/W"), ("热惰性指标", "D=R*S"), ("修正系数", "α"),
                    ("燃烧性能", ""), ("热惰性指标D", ""), ("传热系数", "W/(m^U2^U·k)"),
                    ("平均热惰性指标", ""), ("平均传热系数K", "W/(m^U2^U·k)"))
    if energy.building_property == "居住建筑":
        col_max_index = 14
    elif energy.building_property == "公共建筑":
        col_max_index = 12
    else:
        logger.critical(
            "Cannot get corret maximum column index for specific building property"
        )
        col_max_index = 0

    # -----------Headers
    for h_index, header_name in enumerate(header_names[:col_max_index], 1):
        ws1.cell(2, h_index, header_name[0])

        if not header_name[1]:
            continue
        else:
            ws1.cell(3, h_index, header_name[1])

    # -----------Body
    # ----Column 1-12
    # eg:
    # 构造部位components: ['屋顶构造三（坡屋面）', '东西向外墙', '南北向外墙']
    # 材料参数component_parameters: [ [['西式瓦片', '细石混凝土'], [1780.0, 2300.0], [1.25, 1.51]],
    #                                 [['西式瓦片', '细石混凝土'], [1780.0, 2300.0], [1.25, 1.51]]]
    components = list(energy.constr_thermal[0].keys())
    component_parameters = list(energy.constr_thermal[0].values())
    component_gaps = energy.sum_index_list(energy.Parameters.cmp_constr_count)
    for component, component_gap, component_parameters in zip(
            components, component_gaps, component_parameters):
        # Write components at column 1
        row_index = 4 + component_gap
        ws1.cell(row_index, 1, component)
        # Write parameter
        for parameters, col_index in zip(component_parameters,
                                         range(2, col_max_index + 1)):
            # parameters: [['西式瓦片', '细石混凝土'],
            #              [1780.0, 2300.0],
            #              [1.25, 1.51]]
            # col_index [2, 3, ..., 10]
            if col_index < 11:
                for row_para_index, parameter in enumerate(parameters):
                    row_para_index += row_index
                    ws1.cell(row_para_index, col_index, parameter)
            # parameters: [1.234, 3.631]
            # col_index [11, 12]
            else:
                ws1.cell(row_index, col_index, parameters)
    # ----Column 13, 14
    if energy.building_property == "居住建筑":
        constr_index_sum_list = energy.sum_index_list(
            energy.component_count_list)
        for k_d_index, k_d in enumerate(energy.constr_thermal[1]):
            row_index = 4 + component_gaps[constr_index_sum_list[k_d_index]]
            ws1.cell(row_index, 13, k_d[1])
            ws1.cell(row_index, 14, k_d[0])
        # Check special case for 夏热冬冷地区 where no roof overall table k_d found
        if not energy.roof_k_d[0] and not energy.roof_k_d[1]:
            warn_cell_address.append((4, 13))
            warn_cell_address.append((4, 14))

    # 写入数据来源
    row_ori = 4 + component_gaps[len(component_gaps) - 1]
    ws1.cell(row_ori, 1, "数据来源")

    if global_control.settings["全局设置"]["显示完整的软件版本号"] is True:
        thermal_sheet_basis = "、".join(
            energy.basis[:2]
        ) + "及斯维尔" + energy.software["name"] + "." + energy.software["version"]
    else:
        thermal_sheet_basis = "、".join(
            energy.basis[:2]) + "及斯维尔" + energy.software["name"]

    ws1.cell(row_ori, 2, thermal_sheet_basis)
    ws1.merge_cells(range_string=None,
                    start_row=row_ori,
                    start_column=2,
                    end_row=row_ori,
                    end_column=col_max_index)

    # 合并表头
    if energy.building_property == "居住建筑":
        for col_index in range(1, 15):
            merge_row_gap(ws1, col_index, 2, 3)
    elif energy.building_property == "公共建筑":
        for col_index in range(1, 13):
            merge_row_gap(ws1, col_index, 2, 3)
    else:
        pass
    # 合并单元格
    if energy.building_property == "居住建筑":
        for col_index in (1, 11, 12, 13, 14):
            merge_row_gap(ws1, col_index, 4, row_ori - 1)
    elif energy.building_property == "公共建筑":
        for col_index in (1, 11, 12):
            merge_row_gap(ws1, col_index, 4, row_ori - 1)
    else:
        pass

    # 表头名: 列数, precision(小数点后精确位数)
    customize_precision = \
        global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"]["小数点后精确位数"]
    column_precision = {
        "密度": [3, precision_num(1)],
        "厚度": [4, precision_num(0)],
        "导热系数": [5, precision_num(customize_precision)],
        "蓄热系数": [6, precision_num(customize_precision)],
        "热阻": [7, precision_num(customize_precision)],
        "热惰性指标": [8, precision_num(customize_precision)],
        "修正系数": [9, precision_num(2)],
        "热惰性指标D": [11, precision_num(customize_precision)],
        "传热系数": [12, precision_num(customize_precision)],
        "平均热惰性指标": [13, precision_num(customize_precision)],
        "平均传热系数K": [14, precision_num(customize_precision)],
    }
    if energy.building_property == "公共建筑":
        del column_precision["平均热惰性指标"]
        del column_precision["平均传热系数K"]
    else:
        pass

    # apply style
    sheet_apply_style()
    for el in column_precision.values():
        change_column_precision(ws1, el[1], el[0], 4, row_ori)

    # column width
    for cell in list(ws1.rows)[0]:
        if cell.column == 1:
            ws1.column_dimensions[cell.column_letter].width = 15
        elif cell.column == 2:
            ws1.column_dimensions[cell.column_letter].width = 30
        elif cell.column == 14:
            ws1.column_dimensions[cell.column_letter].width = 13
        else:
            ws1.column_dimensions[cell.column_letter].width = 9.25

    # wrap text
    def data_column_wrap(*cols):
        """change the column cell's text to wrap"""
        for col in cols:
            for cells in ws1.iter_rows(min_col=col,
                                       min_row=4,
                                       max_row=row_ori - 1):
                for cell in cells:
                    cell.alignment = xl.styles.Alignment(horizontal='center',
                                                         vertical='center',
                                                         wrap_text=True)

    data_column_wrap(1, 2)

    # 给表头“热惰性指标D”set wrap
    if energy.building_property == "居住建筑":
        ws1.cell(2, 13).alignment = xl.styles.Alignment(horizontal='center',
                                                        vertical='center',
                                                        wrap_text=True)
        ws1.cell(2, 14).alignment = xl.styles.Alignment(horizontal='center',
                                                        vertical='center',
                                                        wrap_text=True)
    else:
        pass

    ws1.cell(2, 11).alignment = xl.styles.Alignment(horizontal='center',
                                                    vertical='center',
                                                    wrap_text=True)

    ws1.sheet_properties.tabColor = 'FF00B050'
    return wb, energy


def dwg_window_sheet():
    """填写外窗热工表"""
    global working_sheet
    import re
    wb.active = wb.sheetnames.index('外窗热工表')
    ws1 = wb.active
    working_sheet = ws1
    if global_control.settings["全局设置"]['开发者模式'] is True:
        for c_index in range(1, 12):
            ws1.cell(1, c_index, c_index)

    def get_win_name(window_name):
        """Return frame name and glass name in standard BECS dwg format.
        When regular expression find None, return None"""
        frame_result = re.search(r'.+[框|窗]', window_name)
        glass_result = re.search(r'(?<=[+（(]).*(?=）)*', window_name)
        if frame_result:
            frame_result = frame_result.group()
        if glass_result:
            glass_result = glass_result.group()
        return frame_result, glass_result

    def outer_win():
        """填写外门窗系数"""

        # 填写窗墙比
        for r_index, value in enumerate(energy.win_wall_ratio, 4):
            ws1.cell(r_index, 5, float(value))

        def write_win_type():
            """填写门窗类型的数据，数据分布在门窗热工表的表列3，4，9，11"""
            # Check if grab the right window and glass names
            check_window_name = True
            check_glass_name = True

            # ------------填写窗名、玻璃名字、窗墙比、气密性、可见光透射比
            # 单一的窗种类
            if energy.check_win_constr_unique is True:
                for r_index in range(4, 8):
                    # 窗墙比不为零的情况
                    if ws1.cell(r_index, 5).value:
                        # Windows name and glass name
                        window_name, glass_name = get_win_name(
                            energy.win_constr[0][0])
                        if window_name:
                            ws1.cell(r_index, 3, window_name)
                        else:
                            warn_cell_address.append((r_index, 3))
                            check_window_name = False
                        if glass_name:
                            ws1.cell(r_index, 4, glass_name)
                        else:
                            warn_cell_address.append((r_index, 4))
                            check_glass_name = False

                        ws1.cell(r_index, 9, float(energy.win_constr[0][1]))
                        # TODO: 遮蔽系数推算
                        # ws1.cell(r_index, 6, float(energy.win_constr[0][2]))
                        ws1.cell(r_index, 11, float(energy.win_constr[0][3]))
                    # 窗墙比为零
                    else:
                        for c_index in [3, 4, 9, 11]:
                            ws1.cell(r_index, c_index, "-")

            # 非单一的窗种类
            else:
                # eg:
                # [False,
                # ('普通铝合金框+（5+9A+5）无色透明中空玻璃', '4.00', '0.75', '0.800'),
                # ('普通铝合金框+（5+9A+6）无色透明中空玻璃', '4.00', '0.75', '0.800'),]

                # 填写已有的门窗类型
                win_count = 0
                for r_index, single_list in zip(range(4, 8), energy.win_constr):
                    # 窗墙比不为零的情况
                    if ws1.cell(r_index, 5).value:
                        window_name, glass_name = get_win_name(
                            energy.win_constr[0][0])
                        if window_name:
                            ws1.cell(r_index, 3, window_name)
                        else:
                            warn_cell_address.append((r_index, 3))
                            check_window_name = False
                        if glass_name:
                            ws1.cell(r_index, 4, glass_name)
                        else:
                            warn_cell_address.append((r_index, 4))
                            check_glass_name = False

                        ws1.cell(r_index, 9, float(single_list[1]))
                        # TODO: 遮蔽系数推算
                        # ws1.cell(r_index, 6, float(single_list[2]))
                        ws1.cell(r_index, 11, float(single_list[3]))
                        win_count += 1
                    else:
                        for c_index in [3, 4, 9, 11]:
                            ws1.cell(r_index, c_index, "-")

                # 实际上是 1 < win_count < 4的情况
                if win_count < 4:
                    # Copying value
                    for r_index in range(0, 4 - win_count):
                        # 窗墙比不为零的情况
                        if ws1.cell((4 + win_count) + r_index, 5).value:
                            ws1.cell((4 + win_count) + r_index, 3,
                                     ws1.cell((4 + win_count) - 1, 3).value)
                            ws1.cell((4 + win_count) + r_index, 4,
                                     ws1.cell((4 + win_count) - 1, 4).value)
                            ws1.cell((4 + win_count) + r_index, 9,
                                     ws1.cell((4 + win_count) - 1, 9).value)
                            # TODO: 遮蔽系数推算
                            # ws1.cell((4 + win_count) + r_index, 6,
                            #          ws1.cell((4 + win_count) - 1, 6).value)
                            ws1.cell((4 + win_count) + r_index, 11,
                                     ws1.cell((4 + win_count) - 1, 11).value)
                        else:
                            for c_index in [3, 4, 9, 11]:
                                ws1.cell(r_index, c_index, "-")

                        # warning
                        warn_cell_address.append(((4 + win_count) + r_index, 3))
                        warn_cell_address.append(((4 + win_count) + r_index, 4))
                        warn_cell_address.append(((4 + win_count) + r_index, 9))
                        warn_cell_address.append(
                            ((4 + win_count) + r_index, 11))
                        warn_message_cell(20, 1, 5, "门窗类型已超1个，请自行填写")

                elif win_count > 4:
                    for r_index in range(4, 8):
                        warn_cell_address.append((r_index, 3))
                        warn_cell_address.append((r_index, 4))
                        warn_cell_address.append((r_index, 9))
                        warn_cell_address.append((r_index, 11))
                        warn_message_cell(20, 1, 5, "门窗类型已超4个，请自行填写")
                else:
                    pass

            #  ------------填写遮蔽系数
            for r_index in range(4, 8):
                if ws1.cell(r_index, 5).value:
                    warn_cell_address.append((r_index, 6))
            warn_message_cell(19, 1, 5, "请自行填写窗玻璃遮蔽系数Se")
            #  ------------检查门窗名字获取成功
            if check_window_name is False or check_glass_name is False:
                logger.warning(
                    "Regex cannot obtain outer window name and glass name")
                errors.warn_message.append("正则表达式不能获取的外窗名称或玻璃品种名称")

        write_win_type()

        # 填写遮阳系数

        def write_shading(worksheet, row, col, input_data, data_index):
            """
                check if the shading exist
                # ws1.cell(4, 7, float(energy.win_shading_factor["east"][0]))
                # ws1.cell(5, 7, float(energy.win_shading_factor["south"][0]))
                # ws1.cell(6, 7, float(energy.win_shading_factor["west[0]))
                # ws1.cell(7, 7, float(energy.win_shading_factor["north"][0]))
                # ws1.cell(4, 8, float(energy.win_shading_factor["east"][1]))
                # ws1.cell(5, 8, float(energy.win_shading_factor["south"][1]))
                # ws1.cell(6, 8, float(energy.win_shading_factor["west[1]))
                # ws1.cell(7, 8, float(energy.win_shading_factor["north"][1]))
                """
            if input_data:
                result = try_num(input_data[data_index], 'float')
                worksheet.cell(row, col, result)
            else:
                worksheet.cell(row, col, "-")

        # UGLY: Comply with the sequence of 东南西北
        write_shading(ws1, 4, 7, energy.win_shading_factor["east"], 0)
        write_shading(ws1, 5, 7, energy.win_shading_factor["south"], 0)
        write_shading(ws1, 6, 7, energy.win_shading_factor["west"], 0)
        write_shading(ws1, 7, 7, energy.win_shading_factor["north"], 0)
        write_shading(ws1, 4, 8, energy.win_shading_factor["east"], 1)
        write_shading(ws1, 5, 8, energy.win_shading_factor["south"], 1)
        write_shading(ws1, 6, 8, energy.win_shading_factor["west"], 1)
        write_shading(ws1, 7, 8, energy.win_shading_factor["north"], 1)

        # 填写窗气密性
        for r_index in range(4, 8):
            if ws1.cell(r_index, 8).value == "-":
                ws1.cell(r_index, 10, "-")
            else:
                ws1.cell(r_index, 10, energy.win_permeate)

    def skylight():
        """填写天窗"""
        # 没有天窗构造
        if not energy.table_skylight_constr:
            for c_index in range(2, 12):
                if c_index != 3:
                    ws1.cell(10, c_index, "－")
        # 多种天窗构造
        elif energy.check_skylight_constr_unique is False:
            warn_message_cell(18, 1, 5, "不支持自动填写超过一个天窗构造的参数，请自行填写")
        # 天窗只有一种的情况
        else:
            check_glass_name = True
            check_window_name = True
            window_name, glass_name = get_win_name(energy.skylight[0])
            if window_name:
                ws1.cell(10, 2, window_name)
            else:
                warn_cell_address.append((10, 3))
                check_window_name = False
            if glass_name:
                ws1.cell(10, 4, glass_name)
            else:
                warn_cell_address.append((10, 4))
                check_glass_name = False

            ws1.cell(10, 9, energy.skylight[1])
            ws1.cell(10, 7, energy.skylight[2])
            ws1.cell(10, 5, energy.skylight[3])

            # warning
            warn_cell_address.append((10, 6))
            warn_cell_address.append((10, 8))
            warn_cell_address.append((10, 10))
            warn_cell_address.append((10, 11))
            warn_message_cell(18, 1, 5, "请自行填写天窗参数")

            if not energy.table_skylight_ratio:  # 权衡的情况
                warn_cell_address.append((10, 5))
            else:
                pass
            #  ------------检查门窗名字获取成功
            if check_window_name is False or check_glass_name is False:
                logger.warning(
                    "Regex cannot obtain skylight name and glass name")
                errors.warn_message.append("正则表达式不能获取的天窗名称或玻璃品种名称")

    outer_win()
    skylight()

    # ----------apply style-----------
    # 列宽度
    ws1.column_dimensions["A"].width = 10
    ws1.column_dimensions["B"].width = 5
    ws1.column_dimensions["C"].width = 15
    ws1.column_dimensions["D"].width = 30
    sheet_apply_style(input_range="A2:K10")

    # highlight warning cells
    warn_message_cell(17, 1, 5, "请自行确认自行填写红色部分")
    # 小数点

    num_text = precision_num(
        global_control.settings["建筑节能设计专项说明"]["外窗热工表"]["小数点后精确位数"])
    for c_index in range(2, 12):
        change_column_precision(ws1, num_text)

    ws1.sheet_properties.tabColor = 'FF00B050'


def dwg_shading_sheet():
    """填写外窗遮阳表"""
    global working_sheet
    wb.active = wb.sheetnames.index('外窗遮阳表')
    ws1 = wb.active
    working_sheet = ws1
    row_gap = [4]

    def write_data(src_list, orientation_keyword):
        """src_list = [energy.Window.east_win_shading,
        energy.Window.west_win_shading]
        orientation_keyword = [“东”, “西”]"""
        # 东或西向窗户型号[不存在]的情况
        if not src_list:
            # 填写朝向
            r_index = row_gap[len(row_gap) - 1]
            ws1.cell(r_index, 2, orientation_keyword)
            # 填写其他
            for c_index in range(3, 12):
                ws1.cell(r_index, c_index, "-")
            # 东向窗户不存在
            if orientation_keyword == "东":
                row_gap.append(r_index)
            # 西向窗户不存在
            else:
                row_gap.append(r_index)
        # 东或西向窗户型号[存在]的情况
        else:
            for r_index, data_row in enumerate(src_list):
                # eg: one element inside src_list: ('C1524-1', '1500X2400',
                # '织物卷帘遮阳', '0.67')
                # Plus r_index
                r_index_plus = r_index + row_gap[len(row_gap) - 1]
                # 遮阳类型[存在]
                # ----------The second column_index is based on Excel
                # template
                # 朝向
                ws1.cell(r_index_plus, 2, orientation_keyword)
                # Fake data
                ws1.cell(r_index_plus, 10, "-")
                ws1.cell(r_index_plus, 11, "-")
                # 写入窗型号
                ws1.cell(r_index_plus, 3, data_row[0])
                ws1.cell(r_index_plus, 4, data_row[1])
                ws1.cell(r_index_plus, 5, data_row[2])
                ws1.cell(r_index_plus, 12, try_num(data_row[3], "float"))
                # ----------写入窗遮阳
                if data_row[2]:
                    # Using shading type as the value of key to get the dict
                    # value
                    # value = dict[key] eg: value: ['2.550', '1.450',
                    # '0.200', '0.000']
                    shading_data = energy.shading_constr[data_row[2]]
                    for c_index, shading_datum in enumerate(shading_data, 6):
                        shading_datum = try_num(shading_datum, "float")
                        # Floats
                        if type(shading_datum) is float:
                            shading_datum *= 1000
                            shading_datum = int(shading_datum)
                        # String
                        else:
                            pass
                        ws1.cell(r_index_plus, c_index, shading_datum)
                    # --------- Last data_row
                    if r_index == len(src_list) - 1:
                        row_gap.append(r_index_plus + 1)
                # 遮阳类型[不存在]
                else:
                    warn_cell_address.append((r_index_plus, 5))
                    warn_cell_address.append((r_index_plus, 6))
                    warn_cell_address.append((r_index_plus, 7))
                    warn_cell_address.append((r_index_plus, 8))
                    warn_cell_address.append((r_index_plus, 9))
                    errors.warn_message.append("空白的遮阳名称")

    # Writing data
    write_data(energy.shading_win_constr["east"], "东")
    write_data(energy.shading_win_constr["west"], "西")

    # Merge cells of column 1
    ws1.merge_cells(range_string=None,
                    start_row=2,
                    start_column=1,
                    end_row=row_gap[2],
                    end_column=1)
    # Merge cells of column 2
    merge_row_gap(ws1, 2, 4, row_gap[2] - 1)
    # Merge cells of info row
    ws1.cell(row_gap[2], 2, "注明：A——遮阳外挑长度，B——遮阳板根部到窗对边距离")
    ws1.merge_cells(range_string=None,
                    start_row=row_gap[2],
                    start_column=2,
                    end_row=row_gap[2],
                    end_column=12)

    # change column width
    ws1.column_dimensions["A"].width = 3.5
    ws1.column_dimensions["C"].width = 20
    ws1.column_dimensions["D"].width = 15
    ws1.column_dimensions["E"].width = 15
    change_column_precision(ws1, "0.00", 12, 4, row_gap[2])
    ws1.cell(2, 1).alignment = xl.styles.Alignment(horizontal='center',
                                                   vertical='center',
                                                   wrap_text=True)
    sheet_apply_style()
    ws1.sheet_properties.tabColor = 'FF00B050'


def dwg_exa_sheet():
    """填写居住建筑节能设计指标一览表"""
    # Check whether exa file is in output list or not
    if global_control.settings["output_mode"][
            "建筑节能设计审查表"] is True and energy.building_property == "居住建筑":
        check_exa = True
    else:
        check_exa = False

    if check_exa is True:
        from becs import becs_exa_report as exa
    else:
        while True:
            # TODO: manual select
            logger.warning(f"Cannot find 建筑节能设计审查表 document in {parentdir}")
            reply = myUI.MyMessageBox.question(
                myUI.MyWidget(), "提示",
                "已检测到“设置-->建筑节能设计专项说明-->填写建筑节能设计指标一览表”为勾选状态，要使用该功能，程序需要读取包含“建筑节能设计审查表”字样的文件\n\n请当在程序目录放置好包含“建筑节能设计审查表”字样的后，\n点击Yes继续，或者点击No跳过使用“填写工程概括”的功能",
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.Yes)
            # Yes
            if reply == myUI.MyMessageBox.Yes:
                check_exa = check_import_exa()
                if check_exa is True:
                    # End loop
                    from becs import becs_exa_report as exa
                    break
                else:
                    # Reset status to default and continue the next iteration
                    continue
            # No
            else:
                check_exa = False
                for value in climate_area_sheet.values():
                    wb.remove_sheet(wb.get_sheet_by_name(value))
                break

    if check_exa is False:  # do not process any further
        pass
    else:

        def residence():
            global working_sheet
            import re
            keys = list(climate_area_sheet.keys())
            try:
                keys.remove(exa.climate_area)
            except Exception:
                promptError("读取审查表的气候地区错误")
            # Delete other sheets
            for key in keys:
                wb.remove_sheet(wb.get_sheet_by_name(climate_area_sheet[key]))
            wb.active = wb.sheetnames.index(
                climate_area_sheet[exa.climate_area])
            ws1 = wb.active
            working_sheet = ws1
            sheet_addresses = {2: []}
            # eg: sheet_addresses = {2: ["h1", "s1", "a1"], 3: ["b1", "c2"]}
            # reading data from 建筑节能设计审查表
            exa_table = exa.exa_table

            def copy_value(xlsx_cell_address, docx_index, decimal_places=2):
                """Copy value from xlsx to docx, and store the decimalplaces
                value"""
                value = exa_table._cells[docx_index].text.strip()
                ws1[xlsx_cell_address] = try_num(value, "float")
                # Whether to apply precision control or not for cells
                if decimal_places != 0:
                    if decimal_places in sheet_addresses:
                        pass
                    else:
                        sheet_addresses[decimal_places] = []
                    sheet_addresses[decimal_places].append(xlsx_cell_address)

            # Write data into sheet
            if exa.climate_area == "夏热冬暖地区南区":
                # test under becs 2018
                copy_value("H3", 21)
                copy_value("H4", 38)
                copy_value("H5", 89)
                copy_value("H6", 106)
                ws1["H7"] = energy.walls_k_d["east"][0]
                ws1["J7"] = energy.walls_k_d["east"][1]
                ws1["H8"] = energy.walls_k_d["west"][0]
                ws1["J8"] = energy.walls_k_d["west"][1]
                copy_value("H10", 174)
                copy_value("I10", 175)
                copy_value("J10", 178)
                copy_value("K10", 181)
                ws1["H11"] = energy.win_shading_factor["south"][0]
                ws1["I11"] = energy.win_shading_factor["north"][0]
                ws1["J11"] = energy.win_shading_factor["east"][0]
                ws1["K11"] = energy.win_shading_factor["west"][0]
                copy_value("H12", 208)
                copy_value("H13", 225)
                copy_value("H14", 242)
                copy_value("H15", 259)
                copy_value("H18", 293, 0)
                copy_value("H19", 310, 0)
                ws1["I20"] = re.search(r".+(?=、)",
                                       exa_table._cells[327].text).group()
                ws1["K20"] = re.search(r"(?<=、).+",
                                       exa_table._cells[327].text).group()
                copy_value("H21", 344)
                copy_value("H22", 361)
                copy_value("H23", 378)
                copy_value("H25", 412)
                copy_value("J25", 417)
                copy_value("H26", 429)
                copy_value("H28", 446)
                copy_value("H30", 463)
                warn_cell_address.append((17, 8))
            elif exa.climate_area == "夏热冬暖地区北区":
                # test under becs 2018
                copy_value("H3", 22)
                copy_value("H4", 40)
                copy_value("H5", 94)
                copy_value("H6", 112)
                ws1["H7"] = energy.walls_k_d["east"][0]
                ws1["J7"] = energy.walls_k_d["east"][1]
                ws1["H8"] = energy.walls_k_d["west"][0]
                ws1["J8"] = energy.walls_k_d["west"][1]
                copy_value("H10", 184)
                copy_value("I10", 186)
                copy_value("J10", 188)
                copy_value("K10", 190)
                ws1["H11"] = energy.win_shading_factor["south"][0]
                ws1["I11"] = energy.win_shading_factor["north"][0]
                ws1["J11"] = energy.win_shading_factor["east"][0]
                ws1["K11"] = energy.win_shading_factor["west"][0]
                copy_value("H12", 220)
                copy_value("H13", 238)
                copy_value("H14", 256)
                copy_value("H15", 274)
                copy_value("H16", 292)
                copy_value("H19", 328, 0)
                copy_value("H20", 346, 0)
                ws1["I21"] = re.search(r".+(?=、)",
                                       exa_table._cells[364].text).group()
                ws1["K21"] = re.search(r"(?<=、).+",
                                       exa_table._cells[364].text).group()
                copy_value("H22", 382)
                copy_value("H23", 400)
                copy_value("H24", 418)
                copy_value("H26", 454)
                copy_value("J26", 458)
                copy_value("H27", 472)
                copy_value("H29", 490)
                copy_value("H31", 508)
                warn_cell_address.append((18, 8))
            elif exa.climate_area == "夏热冬冷地区":
                # test under becs 2018
                copy_value("H3", 44)
                copy_value("H4", 63)
                copy_value("H5", 82)
                copy_value("H6", 101)
                copy_value("H7", 120)
                copy_value("H10", 158)
                copy_value("I10", 159)
                copy_value("J10", 160)
                copy_value("K10", 163)
                ws1["H11"] = energy.win_shading_factor["south"][0]
                ws1["I11"] = energy.win_shading_factor["north"][0]
                ws1["J11"] = energy.win_shading_factor["east"][0]
                ws1["K11"] = energy.win_shading_factor["west"][0]
                copy_value("H12", 196)
                copy_value("H13", 215)
                copy_value("H14", 234)
                copy_value("H15", 253)
                copy_value("H16", 272)
                copy_value("H19", 310, 0)
                copy_value("H20", 329, 0)
                ws1["I21"] = re.search(r".+(?=、)",
                                       exa_table._cells[348].text).group()
                ws1["K21"] = re.search(r"(?<=、).+",
                                       exa_table._cells[348].text).group()
                copy_value("H22", 367)
                copy_value("H23", 386)
                copy_value("H24", 405)
                copy_value("H26", 443)
                copy_value("J26", 444)
                copy_value("H27", 462)
                copy_value("H29", 481)
                copy_value("H31", 500)
                warn_cell_address.append((18, 8))
            else:
                pass
            ws1.sheet_properties.tabColor = 'FF00B050'
            # Applying format
            sheet_addresses[2].append("H7")
            sheet_addresses[2].append("H8")
            sheet_addresses[2].append("J7")
            sheet_addresses[2].append("J8")
            sheet_apply_style()
            for decimal in sheet_addresses.keys():
                num_format = precision_num(decimal)
                for cell_addr in sheet_addresses[decimal]:
                    ws1[cell_addr].number_format = num_format
            warn_message_cell(40, 1, 5, "请自行确认自行填写红色部分")

        residence()


# 居住建筑
if energy.building_property == "居住建筑":
    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "工程概况"] is True:
        logger.info("Writing 工程概况 into Excel")
        dwg_project_info_sheet()
    else:
        wb.remove_sheet(wb.get_sheet_by_name('工程概况'))

    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "外围护结构构造及热工性能参数表"] is True:
        logger.info("Writing 外围护结构构造及热工性能参数表 into Excel")
        dwg_thermal_sheet()
    else:
        wb.remove_sheet(wb.get_sheet_by_name('外围护结构构造及热工性能参数表'))

    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "外窗热工表"] is True:
        logger.info("Writing 外窗热工表 into Excel")
        dwg_window_sheet()
    else:
        wb.remove_sheet(wb.get_sheet_by_name('外窗热工表'))

    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "外窗遮阳表"] is True:
        logger.info("Writing 外窗遮阳表 into Excel")
        dwg_shading_sheet()
    else:
        wb.remove_sheet(wb.get_sheet_by_name('外窗遮阳表'))

    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "节能设计指标一览表"] is True:
        logger.info("Writing 节能设计指标一览表 into Excel")
        dwg_exa_sheet()
    else:
        for value in climate_area_sheet.values():
            wb.remove_sheet(wb.get_sheet_by_name(value))
# 公共建筑
else:
    if global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
            "外围护结构构造及热工性能参数表"] is True:
        logger.info("Writing 外围护结构构造及热工性能参数表 into Excel")
        dwg_thermal_sheet()
    else:
        wb.remove_sheet(wb.get_sheet_by_name('外围护结构构造及热工性能参数表'))
    logger.info("Skipped proccessing 工程概况、外窗热工表、外窗遮阳表、节能设计指标一览表")
    wb.remove_sheet(wb.get_sheet_by_name('工程概况'))
    wb.remove_sheet(wb.get_sheet_by_name('外窗热工表'))
    wb.remove_sheet(wb.get_sheet_by_name('外窗遮阳表'))
    for value in climate_area_sheet.values():
        wb.remove_sheet(wb.get_sheet_by_name(value))
