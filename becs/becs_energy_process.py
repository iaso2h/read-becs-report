import logging
import re

from docx import Document

import erase_cover
import errors
import global_control
from gui import myUI

logger = logging.getLogger(name=__name__)

# Python-docx object
docx = None
sections = None
paras = None
tables = None

table_constr_overall = None
table_constr_list = []

table_wall_win_ratio = None
table_k_d_roof = None
table_k_d_wall = dict(south=None,
                      north=None,
                      east=None,
                      west=None,
                      overall=None)
table_skylight_ratio = None
table_skylight_constr = None
table_win_constr = None
table_win_air_permeate = None
table_win = dict(south=None, north=None, east=None, west=None)

table_shading_outer = None
table_shading = dict(custom=None, blinds=None, plate=None)
table_shading_overall = None
# Check
check_deformation = False
check_stipulation = False
check_win = dict(skylight=False, south=True, north=True, east=True, west=True)
check_shading = dict(plate=False, blinds=False, custom=False)
check_wall = dict(south=True, north=True, east=True, west=True, overall=True)
check_skylight_constr_unique = False
check_win_constr_unique = False
# Document data
building_property = None
basis = []
software = {}
project = {}
building = {}
components = []
component_count = dict(屋顶构造=0, 外墙相关构造=0, 挑空楼板构造=0, 架空或外挑楼板=0, 凸窗顶板=0, 凸窗侧板=0)
component_count_list = None
constr_project_count = None
component_parse_slice_dict = dict(屋顶构造=[],
                                  外墙相关构造=[],
                                  挑空楼板构造=[],
                                  架空或外挑楼板=[],
                                  凸窗顶板=[],
                                  凸窗侧板=[])
constr_thermal = None

roof_k_d = None
walls_k_d = dict(south=None, north=None, east=None, west=None, overall=None)
win_wall_ratio = []
win_constr = []
win_permeate = None
win_shading_factor = dict(south=None,
                          north=None,
                          east=None,
                          west=None,
                          overall=None)
skylight = []
shading_constr = {}
shading_win_constr = dict(south=None, north=None, east=None, west=None)

# -------------------------Utility funcs Begin-------------------------


def promptError(input_type):
    myWidget = myUI.MyWidget()
    myUI.MyMessageBox.critical(myWidget, "匹配错误", input_type,
                               myUI.MyMessageBox.Ok)


def sum_index_list(input_list: list):
    """loop through the input list and a list of sum of every index being stacked up
    eg: when your input list is:[10, 10, 10, 10]
            it generator result:[0, 10, 20, 30, 40]
    """
    temp_list = input_list.copy()
    temp_list.insert(0, 0)
    sum_stage = 0
    result_list = []
    for i in temp_list:
        sum_stage += i
        result_list.append(sum_stage)
    return result_list


def list_sum(input_list: list, num=None):
    """return the sum of all the element inside given list, the optional
    num indicate the offset of the list and the func will return the sum of
    all list's certain element specified by that offset
    """
    init_num = 0
    if not num:
        for i in input_list:
            if type(i) != str:
                init_num += i
        return init_num
    else:
        pass


def convert_float(input_str):
    """Convert text into float, return src str when conversion fail"""
    try:
        return float(input_str)
    except Exception:
        return input_str


def convert_float_re(input_str):
    """Find number in str and return float"""
    try:
        return float(input_str)
    except ValueError:
        result = re.search(r'\d\.\d+', input_str)
        if result:
            return float(result.group())
        else:
            return result


def unique_nested_list(input_list):
    """
    eg:
    when input_list = [[1,1],[2,2],[1,1]]
    this func return  [[1,1],[2,2]]
    """
    if len(input_list) == 1:
        return input_list
    else:
        return list(set(map(lambda i: tuple(i), input_list)))


# -------------------------Utility funcs End---------------------------


# noinspection PyUnresolvedReferences,PyProtectedMember
def parse_software_project_building():
    """Get info from the first three tables from BECS report"""
    table_project = tables[0]
    table_software = tables[1]
    table_building = tables[2]
    # Software
    logger.info("Gathering software information")
    software["name"] = table_software._cells[1].text
    software["version"] = table_software._cells[3].text
    # Project
    logger.info("Gathering project information")
    project["name"] = table_project._cells[1].text
    project["location"] = table_project._cells[3].text
    project["id"] = table_project._cells[5].text
    project["client"] = table_project._cells[7].text
    project["company"] = "华蓝设计（集团）有限公司"
    # Building
    logger.info("Gathering building information")
    building["geographic_location"] = f"{table_building._cells[7].text}，" \
                                      f"{table_building._cells[8].text}"
    re_result = re.findall(r'\d+', table_building._cells[10].text)
    building["area"] = f"地上{re_result[0]}m^U2^U， 地下{re_result[1]}m^U2^U"
    building["storey"] = re.findall(r'\d+', table_building._cells[13].text)
    building["height"] = table_building._cells[16].text
    building["orientation"] = table_building._cells[25].text
    building["structure"] = table_building._cells[28].text
    building["volume_factor"] = float(table_building.cell(7, 1).text) / float(
        table_building.cell(6, 1).text)


# noinspection PyUnresolvedReferences
def parse_paragraphs():
    """Loop through all paragraphs inside Word document and get info
    containing keywords """
    global basis
    basis_slice = []
    check_start_component = False
    check_stop_component = False
    for para_index, p in enumerate(paras):
        # Parsing 依据
        if "设计依据" in p.text:
            basis_slice.append(para_index)
        elif "建筑大样" in p.text:
            basis_slice.append(para_index)
        elif "模型观察" in p.text and len(basis_slice) != 2:
            basis_slice.append(para_index)
        elif "工程材料" in p.text:
            if len(basis_slice) != 2:
                basis_slice.append(para_index)
            # -------Parsing project basis
            paragraphs = paras[basis_slice[0] + 1:basis_slice[1]]
            for basic_p in paragraphs:
                basis += re.findall(r'《.*?[)）]', basic_p.text)
        # Check if start to parse component keywords
        elif "外窗表" in p.text or "可见光透射比" in p.text:
            check_start_component = True
        # Parsing 天窗
        elif "天窗屋顶比" in p.text:
            if "本工程无此项内容" not in paras[para_index + 1].text:
                check_win["skylight"] = True
        elif "天窗类型" in p.text:
            if "本工程无此项内容" not in paras[para_index + 1].text:
                check_win["skylight"] = True
            else:
                check_win["skylight"] = False
        # ------ Parsing index of keywords related to component to get component
        elif check_start_component is True and check_stop_component is False:
            if "屋顶构造" in p.text and "无此项内容" not in paras[
                    para_index + 1].text and len(
                        component_parse_slice_dict["屋顶构造"]) == 0:
                component_parse_slice_dict["屋顶构造"].append(para_index)
            elif "外墙构造" in p.text and len(
                    component_parse_slice_dict["屋顶构造"]) == 1:
                component_parse_slice_dict["屋顶构造"].append(para_index)

            elif "外墙相关构造" in p.text and "无此项内容" not in paras[
                    para_index + 1].text and len(
                        component_parse_slice_dict["外墙相关构造"]) == 0:
                component_parse_slice_dict["外墙相关构造"].append(para_index)
            elif "外墙平均热工特性" in p.text and len(
                    component_parse_slice_dict["外墙相关构造"]) == 1:
                component_parse_slice_dict["外墙相关构造"].append(para_index)

            elif "挑空楼板构造" in p.text and "无此项内容" not in paras[
                    para_index + 1].text and len(
                        component_parse_slice_dict["挑空楼板构造"]) == 0:
                component_parse_slice_dict["挑空楼板构造"].append(para_index)
            elif "外窗热工" in p.text:
                if len(component_parse_slice_dict["挑空楼板构造"]) == 1:
                    component_parse_slice_dict["挑空楼板构造"].append(para_index)
                elif len(component_parse_slice_dict["凸窗侧板"]) == 1:
                    component_parse_slice_dict["凸窗侧板"].append(para_index)
                check_stop_component = True

            elif "架空或外挑楼板" in p.text and "无此项内容" not in paras[
                    para_index + 1].text and len(
                        component_parse_slice_dict["架空或外挑楼板"]) == 0:
                component_parse_slice_dict["架空或外挑楼板"].append(para_index)
            elif "分户墙" in p.text and len(
                    component_parse_slice_dict["架空或外挑楼板"]) == 1:
                component_parse_slice_dict["架空或外挑楼板"].append(para_index)
                check_stop_component = True

            elif "凸窗顶板" in p.text and "无此项内容" not in paras[
                    para_index + 1].text and len(
                        component_parse_slice_dict["凸窗顶板"]) == 0:
                component_parse_slice_dict["凸窗顶板"].append(para_index)
            elif "凸窗侧板" in p.text:
                if len(component_parse_slice_dict["凸窗顶板"]) == 1:
                    component_parse_slice_dict["凸窗顶板"].append(para_index)
                elif "无此项内容" not in paras[para_index + 1].text and len(
                        component_parse_slice_dict["凸窗侧板"]) == 1:
                    component_parse_slice_dict["凸窗侧板"].append(para_index)
            else:
                pass
        # ------ Parsing index of component keywords
        elif "平板遮阳" in p.text:
            # Checking existence of shadings
            check_shading["plate"] = True
        elif "百叶遮阳" in p.text:
            check_shading["blinds"] = True
        elif "自定义遮阳" in p.text:
            check_shading["custom"] = True
        else:
            pass


# noinspection PyUnresolvedReferences
def parse_tables():
    """Loop through all tables inside Word document and get info
    containing keywords """
    global building_property
    global table_constr_overall, table_constr_list, table_wall_win_ratio, \
        table_k_d_roof, table_k_d_wall, table_win_constr, \
        table_skylight_constr, table_skylight_ratio, table_win, \
        table_shading_outer, table_shading, table_shading_overall, \
        table_win_air_permeate
    global check_stipulation, check_deformation
    for t_index, table in enumerate(tables):
        t_column_count = len(table.columns)
        t_row_count = len(table.rows)
        if "材料名称" in table.cell(0, 0).text:
            if not table_constr_overall:
                table_constr_overall = table
            else:
                # Discard excceding constr tables 夏热冬冷地区
                # eg:
                # 控温与非控温楼板构造一
                # 控温房间楼板构造一
                # 通往封闭空间的户门
                # 通往非封闭空间或户外的户门
                if len(table_constr_list) != constr_project_count:
                    table_constr_list.append(table)
        elif t_row_count > 1 and table.cell(
                1, 0).text == "南向" and t_column_count > 1 and table.cell(
                    0, 1).text == "窗面积(㎡)":
            table_wall_win_ratio = table
            building_property = "居住建筑"
            parse_wall_win_ratio(table)
        elif t_row_count > 1 and table.cell(
                1, 0).text == "南向" and t_column_count > 2 and table.cell(
                    0, 2).text == "窗面积(㎡)":
            table_wall_win_ratio = table
            building_property = "公共建筑"
            parse_wall_win_ratio(table)
        elif table.cell(0, 0).text == "构造名称":
            if t_column_count == 7 or t_column_count == 6:
                if "面积" in table.cell(0, 1).text:
                    table_k_d_roof = table
                else:
                    for item in check_wall.items():
                        if item[1] is True and table_k_d_wall[item[0]] is None:
                            table_k_d_wall[item[0]] = table
                            break
            else:
                pass
        elif table.cell(0, 0).text == "朝向" and table.cell(
                0, 1).text == "面积" and table.cell(0, 2).text == "面积所占比例":
            # special case when deformation joint is used in the project,
            table_k_d_wall["overall"] = table
            check_deformation = True
        elif check_win["skylight"] is True and t_column_count > 4 and table.cell(
                0, 4).text == "自遮阳系数" and table.cell(0, 5).text == "备注":
            table_skylight_constr = tables[t_index]
            if check_stipulation is True:
                table_skylight_ratio = tables[t_index - 1]
        elif t_column_count > 5 and table.cell(0, 5).text == "可见光透射比":
            table_win_constr = tables[t_index]
            # -------遮阳类型
            shading_items = list(check_shading.items())
            i_index = 1
            for item in shading_items:
                if item[1] is True:
                    table = tables[t_index + i_index]
                    table_shading[item[0]] = table
                    i_index += 1
                else:
                    pass
            # -------外遮阳
            if tables[t_index + i_index].cell(0, 0).text == "朝向":
                table_shading_outer = tables[t_index + i_index]
                i_index += 1
            # -------各朝向窗户综合遮阳系数Sw
            # 夏热冬冷地区北区跳过窗户平均传热系数表
            # Check if 平均传热系数表 is ahead of 平均遮阳系数表
            if len(tables[t_index + i_index].row_cells(0)) == 8:
                skip_k_index = 4
            else:
                skip_k_index = 0
            window_items = list(check_win.items())[1:]  # Omit skylight
            for item in window_items:
                if item[1] is True:
                    table = tables[t_index + skip_k_index + i_index]
                    table_win[item[0]] = table
                    i_index += 1
                else:
                    pass
        elif table.cell(0, 0).text == "朝向" and "面积（" in table.cell(0, 1).text:
            # 建筑综合遮阳系数表
            table_shading_overall = table
        elif t_column_count > 2 and table.cell(1, 0).text == "最不利气密性等级":
            table_win_air_permeate = tables[t_index]
            break
        else:
            pass


def parse_wall_win_ratio(table):
    """Check whether four orientation of wall's and windows's area is 0 or
    not"""
    # Wall
    for r_index, key in zip(range(1, 5), list(check_wall.keys())):
        if table.cell(r_index, 2).text == "0.00":
            check_wall[key] = False
    # Window
    for r_index, key in zip(range(1, 5), list(check_win.keys())[1:]):
        if table.cell(r_index, 1).text == "0.00":
            check_win[key] = False


# noinspection PyUnresolvedReferences
def get_component():
    """Get component text based on variable component_parse_slice_dict"""

    def component_text_pure(src_str):
        """Check if the text contain words featured in BECS 2016 like:
        First has 0, Second has 1:  '11.1 平屋面\t8'
        First has 0, Second has 1:  '12.1.1 外墙\t9'
        First has 0, Second has 1:  '12.1.2 热桥柱\t9'
        First has 0, Second has 1:  '12.1.3 热桥梁\t10'
        First has 0, Second has 1:  '12.1.4 热桥板\t10'
        First has 0, Second has 1:  '13 挑空楼板构造\t11'
        Return a clean object
        """
        if "\t" in src_str:
            result = re.search(r'(?<=\s)\b.+?(?=\t)', src_str)
            if result:
                return result.group()
            else:
                return src_str
        else:
            return src_str

    global components
    component = []

    for item in list(component_parse_slice_dict.items()):
        # eg: item --> ["屋顶构造", [123, 321]]
        if len(item[1]) != 2:
            continue
        # Check if two indexes are found
        component_keyword = item[0]
        index1 = item[1][0]
        index2 = item[1][1]
        # Get component text based on paragraph index
        if component_keyword == "屋顶构造":
            # Single roof
            if index2 - index1 <= 3:
                component = [paras[index1 + 1].text]
            # Multiple roofs
            else:
                # Get rid of 外墙主断面传热系数的修正系数
                for p_index, p in enumerate(paras[index1:index2], index1):
                    if "屋顶相关构造" in p.text and p.text:
                        index1 = p_index
                    elif "屋顶平均热工特性" in p.text and p.text:
                        index2 = p_index + 1

                paras_component = paras[(index1 + 1):(index2 - 1)]
                component = [p.text for p in paras_component]
        elif component_keyword == "外墙相关构造":
            # Get rid of 外墙主断面传热系数的修正系数, instance in 公共建筑
            for p_index, p in enumerate(paras[index1:index2], index1):
                if "外墙主断面传热系数" in p.text:
                    index2 = p_index

            paras_component = paras[(index1 + 1):index2]
            component = [p.text for p in paras_component]
        elif component_keyword == "挑空楼板构造" or component_keyword == "架空或外挑楼板":
            if index2 - index1 > 2:
                paras_component = paras[index1 + 1:(index1 + 2)]
                component = [p.text for p in paras_component]
            else:
                pass
        elif component_keyword == "凸窗顶板":
            if index2 - index1 == 3:
                component = [paras[index1 + 1].text]
            else:
                paras_component = paras[(index1 + 1):(index2 - 1)]
                component = [p.text for p in paras_component]
        elif component_keyword == "凸窗侧板":
            if index2 - index1 == 2:
                component = [paras[index1 + 1].text]
            else:
                paras_component = paras[(index1 + 1):(index2 - 1)]
                component = [p.text for p in paras_component]
        else:
            pass
        # Concanate the component to components list
        if component:
            component = list(map(component_text_pure, component))
            components += component
            # Write count
            component_count[component_keyword] = len(component)
            # Reinitialization
            component = []
        # No component
        else:
            # No write access to component_count_dict
            pass


class Parameters:
    """ https://baike.baidu.com/item/%E4%BC%A0%E7%83%AD%E7%B3%BB%E6%95%B0
    /10804658?fr=aladdin
    x_factor, namely 围护结构两表面热交换系数"""
    cmp_x_factor = []
    cmp_constr_count = []
    cmp_row_index_sum = []

    def __init__(self, column_index: int):
        self.data = []
        self.x_factor = None
        self.row_index_sum = None
        self.count = None
        self.get_data(column_index)

    def get_data(self, column_index):
        """Get row index of 各层之和 , xfactor, constructions count of each
        construction table, parameter data"""
        for table in table_constr_list:
            # get the right area to slice
            first_column_cells = table.column_cells(0)
            for cell in first_column_cells:
                if "各层之和" in cell.text:
                    self.row_index_sum = first_column_cells.index(cell)
                    if len(Parameters.cmp_row_index_sum) != len(
                            table_constr_list):
                        self.count = self.row_index_sum - 2
                        Parameters.cmp_constr_count.append(self.count)
                        Parameters.cmp_row_index_sum.append(self.row_index_sum)
                elif "传热系数" in cell.text:
                    self.x_factor = float(
                        re.search(r'(?<=\().*(?=\+)', cell.text).group())
                    if len(Parameters.cmp_x_factor) != len(table_constr_list):
                        Parameters.cmp_x_factor.append(self.x_factor)
                elif "修正后" in cell.text:
                    break

            # Slice the specific column cells
            cells = table.column_cells(column_index)[2:self.row_index_sum]
            # Data format conversion for different column index
            cell_text = [cell.text for cell in cells]
            if column_index != 0:
                cell_text = list(map(convert_float, cell_text))
            self.data.append(cell_text)


# ------------------外围护结构热工表 Begin ----------------------
# noinspection PyUnresolvedReferences,PyUnboundLocalVariable
def constr_thermal_sheet():
    """Return a dict contains all data of 外围护结构热工表"""

    # noinspection PyTypeChecker,PyUnresolvedReferences
    def get_overall_k_d():
        """Parse roof_k_d walls_k_d"""

        def find_component_process_count(input_table):
            """find the count of different segments of wall"""
            for c_index, cell in enumerate(input_table.column_cells(0)):
                # Stop at 合计
                if cell.text == "合计":
                    return c_index - 1

        def average_component_k_d(input_table, ratio_column_index=3):
            """return the K and D from different orientations of wall"""
            if input_table:
                k_column_index = ratio_column_index + 1
                d_column_index = ratio_column_index + 2

                count = find_component_process_count(input_table)
                if not count:
                    logger.critical("Wrong table")
                    raise TypeError
                k = [
                    float(input_table.cell(r_index, ratio_column_index).text) *
                    float(input_table.cell(r_index, k_column_index).text)
                    for r_index in range(1, count + 1)
                ]
                d = [
                    float(input_table.cell(r_index, ratio_column_index).text) *
                    float(input_table.cell(r_index, d_column_index).text)
                    for r_index in range(1, count + 1)
                ]
                k_d = [list_sum(k), list_sum(d)]
                return k_d
            else:
                return "－", "－"

        logger.info("Calculating overall 热惰性指标, 传热系数")
        global table_k_d_roof
        global table_k_d_wall
        global roof_k_d
        global walls_k_d
        # ----------Roof--------
        # 当屋顶只有一种，不需要计算平均k_d值的情况
        if component_count["屋顶构造"] == 1:
            roof_k_d = C_传热系数[0], C_各层热惰性之和[0]
        else:
            # Check if roof overall table k_d found
            # Special case for 夏热冬冷地区 where no roof overall table k_d found
            if table_k_d_roof is None:
                roof_k_d = (0, 0)
                errors.warn_message.append("空白的屋顶平均传热系数与热惰性指标")
            else:
                roof_k_d = average_component_k_d(table_k_d_roof, 2)

        # ----------Walls--------
        # ----4 orientation wall k_ds
        for table in table_k_d_wall.items():
            if table[1] and table[0] != "overall":
                walls_k_d[table[0]] = average_component_k_d(table[1])
            else:
                pass
        # ----overall wall k_ds
        if check_deformation is False:
            walls_k_d["overall"] = average_component_k_d(
                table_k_d_wall["overall"])
        # When deformation is found in project, calculate the over wall_k_ds
        # Test on BECS 2018
        else:
            c_wall_k_ds = list(walls_k_d.values())
            # Delete "overall" table, and we got 4 k_ds tuple from 4
            # orientations
            del c_wall_k_ds[4]
            k = list_sum([
                float(table_k_d_wall["overall"].cell(r_index, 2).text) * k_d[0]
                for r_index, k_d in zip(range(1, 5), c_wall_k_ds)
                if k_d
            ])
            d = list_sum([
                float(table_k_d_wall["overall"].cell(r_index, 2).text) * k_d[1]
                for r_index, k_d in zip(range(1, 5), c_wall_k_ds)
                if k_d
            ])
            walls_k_d["overall"] = [k, d]

    global table_constr_overall
    global components

    logger.info("Gathering construction table information")
    if table_constr_overall:
        密度表 = {
            单组构造.text: 单组密度.text for 单组构造, 单组密度 in zip(
                table_constr_overall.column_cells(0)[2:],
                table_constr_overall.column_cells(3)[2:])
        }
    else:
        logger.error("Cannot find table_constr_overall")
        promptError("无法找到工程材料")

    # -----Data from Word document
    logger.info("Gathering 构造层 from each construction table")
    构造层 = Parameters(0)
    logger.info("Gathering 密度 from each construction table")
    厚度 = Parameters(1)
    logger.info("Gathering 导热系数 from each construction table")
    导热系数 = Parameters(2)
    logger.info("Gathering 蓄热系数 from each construction table")
    蓄热系数 = Parameters(3)
    logger.info("Gathering 修正系数 from each construction table")
    修正系数 = Parameters(4)
    logger.info("Gathering 热阻 from each construction table")
    热阻 = Parameters(5)  # 不使用从报告读取的数值
    logger.info("Gathering 热惰性指标 from each construction table")
    热惰性指标 = Parameters(6)
    logger.info("Gathering 各层热惰性之和 from overall construction table")

    密度 = []
    for processes in 构造层.data:
        densities = [float(密度表[process]) for process in processes]
        密度.append(densities)
    logger.info("Gathering 各层热惰性之和 from each construction table")

    # 各层热惰性之和 = [
    #     convert_float_re(table.cell(row_index, 1).text) for table, row_index in
    #     zip(table_constr_list, Parameters.cmp_row_index_sum)
    # ]

    # -----Data from calculation
    logger.info("Matching 燃烧性能")
    b1_burning_mat = global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"][
        "B1材料关键字"]
    logger.info(f"Words contain {b1_burning_mat} will be treated as B1级")
    燃烧性能 = []
    for processes in 构造层.data:
        # Default A
        单组燃烧性能 = ["A" for i in range(0, len(processes))]
        # Checking for B1
        for keyword in b1_burning_mat:
            for p_index, process in enumerate(processes):
                if keyword in process:
                    单组燃烧性能[p_index] = "B1"
                else:
                    pass
        燃烧性能.append(单组燃烧性能)

    # 以下带有"C_"前缀的变量意思是通过计算得到的版本，与从节能报告读取的版本相区分
    # 不带"C_"前缀的变量都从节能报告读取的版本可从上一区域代码获取
    logger.info("Calculating 热阻")
    C_热阻 = []  # 运算因子：厚度，导热系数，修正系数
    for 单组厚度, 单组导热系数, 单组修正系数 in zip(厚度.data, 导热系数.data, 修正系数.data):
        单组热阻 = []
        for 单厚度, 单导热系数, 单修正系数 in zip(单组厚度, 单组导热系数, 单组修正系数):
            if type(单厚度) == str or type(单导热系数) == str or type(单厚度) == str:
                单组热阻.append(0)
            else:
                单组热阻.append(单厚度 / 单导热系数 / 1000 / 单修正系数)
        C_热阻.append(单组热阻)

    logger.info("Calculating 热惰性指标")
    C_热惰性指标 = []  # 运算因子：蓄热系数，修正系数，单组热阻
    for 单组蓄热系数, 单组修正系数, C_单组热阻 in zip(蓄热系数.data, 修正系数.data, C_热阻):
        C_单组热惰性指标 = []
        for 单蓄热系数, 单修正系数, C_单热阻 in zip(单组蓄热系数, 单组修正系数, C_单组热阻):
            if type(单蓄热系数) == str or type(单修正系数) == str or type(C_单热阻) == str:
                C_单组热惰性指标.append('-')
            else:
                C_单组热惰性指标.append(单蓄热系数 * 单修正系数 * C_单热阻)
        C_热惰性指标.append(C_单组热惰性指标)

    logger.info("Calculating 各层热惰性之和")
    C_各层热惰性之和 = [list_sum(C_单组热惰性指标) for C_单组热惰性指标 in C_热惰性指标]

    logger.info("Calculating 传热系数")
    C_传热系数 = [
        1 / (x_factor + list_sum(resistances))
        for x_factor, resistances in zip(Parameters.cmp_x_factor, C_热阻)
    ]

    if building_property == "居住建筑":
        get_overall_k_d()
    else:
        logger.info("公共建筑 detected, skip calculating overall 热惰性指标, 传热系数")

    # Column 1-12
    logger.info("Merging 构造做法表")
    column1_12 = {}
    for 单组部位, 单组构造, 单组密度, 单组厚度, 单组导热系数, 单组蓄热系数, C_单组热阻, 单组热惰性指标, 单组修正系数, \
        单组燃烧性能, C_单组各层热惰性之和, C_单组传热系数 in zip(
        components, 构造层.data, 密度, 厚度.data, 导热系数.data, 蓄热系数.data, C_热阻,
        热惰性指标.data, 修正系数.data, 燃烧性能, C_各层热惰性之和, C_传热系数):
        column1_12[单组部位] = [
            单组构造, 单组密度, 单组厚度, 单组导热系数, 单组蓄热系数, C_单组热阻, 单组热惰性指标, 单组修正系数, 单组燃烧性能,
            C_单组各层热惰性之和, C_单组传热系数
        ]
    # Column 13-14
    column13_14 = [roof_k_d, walls_k_d["overall"]]
    # When founded component number > 2
    cmp_valid = list(component_count.keys())
    # OPTIMIZE
    if len(cmp_valid) > 2:
        constr_index_sum_list = sum_index_list(component_count_list)
        if "挑空楼板构造" in component_count or "架空或外挑楼板" in component_count:
            if "挑空楼板构造" in cmp_valid:
                overhang_cmp_index = cmp_valid.index("挑空楼板构造")
            else:
                overhang_cmp_index = cmp_valid.index("架空或外挑楼板")
            overhang_k_d = [
                C_传热系数[constr_index_sum_list[overhang_cmp_index]],
                C_各层热惰性之和[overhang_cmp_index]
            ]
            column13_14.append(overhang_k_d)
        if "凸窗顶板" in component_count:
            baywindow_cmp_index = cmp_valid.index("凸窗顶板")
            baywindow_k_d = [
                C_传热系数[constr_index_sum_list[baywindow_cmp_index]],
                C_各层热惰性之和[baywindow_cmp_index]
            ]
            column13_14.append(baywindow_k_d)
    return column1_12, column13_14


# ---------------------Window--------------------------------


# noinspection PyUnresolvedReferences
def window():
    global building_property
    global table_skylight_constr, table_skylight_ratio, table_wall_win_ratio,\
        table_win, table_win_constr, table_win_air_permeate
    global check_shading, check_skylight_constr_unique, check_win_constr_unique
    global win_wall_ratio, win_constr, win_permeate, win_shading_factor
    global shading_constr, shading_win_constr
    global skylight
    # -----窗墙比-----
    if building_property == "居住建筑":
        win_wall_ratio = [
            table_wall_win_ratio.cell(3, 3).text,
            table_wall_win_ratio.cell(1, 3).text,
            table_wall_win_ratio.cell(4, 3).text,
            table_wall_win_ratio.cell(2, 3).text,
        ]
    else:
        pass  # 公共建筑不适配
    # -----窗类型-----
    win_constr_duplicate = []
    for r_index in range(1, len(table_win_constr.column_cells(0))):
        row_list = [
            table_win_constr.cell(r_index, c_index).text
            for c_index in range(6)
            if c_index != 0 and c_index != 2
        ]
        win_constr_duplicate.append(row_list)

    # check if all windows type are of the same type
    if global_control.settings["建筑节能设计专项说明"]["外窗遮阳表"]["去除重复门窗的表格行"] is True:
        win_constr = unique_nested_list(win_constr_duplicate)
    else:
        win_constr = win_constr_duplicate
    if len(win_constr) == 1 and len(win_constr_duplicate[0]) == len(
            win_constr[0]):
        check_win_constr_unique = True  # [0构造名称 1传热系数 2自遮阳系数 3可见光透射比]
    else:
        check_win_constr_unique = False

    # -----窗遮阳系数 * 4 -----
    def get_win_shading(input_table):
        # 该方向的窗户不存在的情况下
        if not input_table:
            return "－", "－"
        else:
            row_len = len(input_table.column_cells(0))
            win_shading = float(input_table.cell(row_len - 1, 9).text)
            overall_shading = float(input_table.cell(row_len - 1, 10).text)
            return win_shading, overall_shading

    if building_property == "居住建筑":
        win_shading_factor["south"] = get_win_shading(table_win["south"])
        win_shading_factor["north"] = get_win_shading(table_win["north"])
        win_shading_factor["east"] = get_win_shading(table_win["east"])
        win_shading_factor["west"] = get_win_shading(table_win["west"])
    else:
        pass  # 公共建筑不适配

    # -----窗气密性-----
    if "-" not in table_win_air_permeate.cell(1, 2).text:
        win_permeate = table_win_air_permeate.cell(1, 1).text[:3].strip()
    else:
        win_permeate = table_win_air_permeate.cell(1, 2).text[:3].strip()

    # -----遮阳形式-----
    # Check valid shading
    if building_property == "居住建筑" and list(
            check_shading.values()).count(False) == 3:
        promptError("本工程没有设置遮阳措施，居住建筑东、西向外窗必须采取建筑外遮阳措施筑")
    # 平板遮阳
    if check_shading["plate"] is True:
        table = table_shading["plate"]

        for r_index, cell in enumerate(table.column_cells(1)):
            if r_index != 0 and cell.text:
                one_plate_type = [
                    table.cell(r_index, c_index).text
                    for c_index in range(2, 6)
                ]
                shading_constr[table.cell(r_index, 1).text] = one_plate_type
    else:
        pass

    # 百叶遮阳
    if check_shading["blinds"] is True:
        for r_index, cell in enumerate(table_shading['blinds'].column_cells(1)):
            if r_index != 0 and cell.text:
                shading_constr[cell.text] = ["-", "-", "-", "-"]
            else:
                pass
    else:
        pass

    # 自定义遮阳
    if check_shading["custom"] is True:
        for r_index, cell in enumerate(table_shading["custom"].column_cells(1)):
            if r_index != 0 and cell.text:
                shading_constr[cell.text] = ["-", "-", "-", "-"]
            else:
                pass
    else:
        pass

    # -----窗户型号-----
    def get_win_shanding(input_table):
        if not input_table:  # 该方向没有窗户的时候
            return None
        else:

            def get_window_size(window_text):
                """eg: Return 2400X1500 based on BYC2415"""
                # Blank window label
                if not window_text:
                    logger.warning("Blank window label")
                    errors.warn_message.append("空白的门窗标号")
                    return ""
                # Valid window label
                else:
                    result = re.search(r'\d{4,}', window_text)
                    # Regex failed
                    if not result:
                        logger.warning("Cannot get window size")
                        errors.warn_message.append("无效的门窗标号")
                        return ""
                    # Return correct window size
                    else:
                        result = result.group()
                        width = int(result[:len(result) - 2])
                        height = int(result[len(result) - 2:])
                        return str(width * 100) + "X" + str(height * 100)

            shading_win_constr_list = []
            for r_index in range(1, len(input_table.column_cells(0)) - 1):
                win_shading_list = [
                    input_table.cell(r_index, c_index).text
                    for c_index in [1, 8, 9]
                ]
                # 插入推算出来的门窗尺寸
                win_shading_list.insert(1, get_window_size(win_shading_list[0]))
                shading_win_constr_list.append(win_shading_list)

            if global_control.settings["建筑节能设计专项说明"]["外窗遮阳表"][
                    "去除重复门窗的表格行"] is True:
                shading_win_constr_list = unique_nested_list(shading_win_constr_list)
            else:
                pass

        # 0门窗编号  1门窗尺寸  2外遮阳编号  3综合遮阳系数
        return shading_win_constr_list

    if building_property == "居住建筑":
        shading_win_constr["east"] = get_win_shanding(table_win["east"])
        shading_win_constr["west"] = get_win_shanding(table_win["west"])
    else:
        pass

    # -----天窗型号-----
    # 0 唯一构造 1构造名称	2传热系数  3自遮阳系数   4窗屋比
    # 检查是否只有一种天窗构造
    if check_win["skylight"] is True and building_property == "居住建筑":
        # Multiple skylight
        if table_skylight_constr.cell(2, 0).text != "平均":
            pass
        # Unique Skylight
        else:
            check_skylight_constr_unique = True
            # 规定指标
            if check_stipulation is True:
                skylight = [
                    table_skylight_constr.cell(1, 1).text,
                    float(table_skylight_constr.cell(1, 3).text),
                    float(table_skylight_constr.cell(1, 4).text),
                    float(table_skylight_ratio.cell(5, 2).text) /
                    float(table_skylight_ratio.cell(5, 3).text)
                ]
            # 性能指标
            else:
                skylight = [
                    table_skylight_constr.cell(1, 1).text,
                    float(table_skylight_constr.cell(1, 3).text),
                    float(table_skylight_constr.cell(1, 4).text), ""
                ]
    else:
        pass


# ---------------------Window Jargon----------------------------
def unify_win_shading_paras_jargon():
    global paras
    """统一围护结构作法简要说明处的遮阳系数为窗本身遮阳系数SC"""

    for p_index, p in enumerate(paras):
        if "自身遮阳系数" in p.text:
            p.text = p.text.replace("自身遮阳系数", "窗本身遮阳系数SC")
            p.text = p.text.replace("m^2.K", "m²·K")
        # 统一“平均遮阳系数”为“平均综合遮阳系数”
        elif "平均遮阳系数" in p.text and "6" not in p.text:
            p.text = p.text.replace("平均遮阳系数", "平均综合遮阳系数")
        elif "平均遮阳系数" in p.text and "6" in p.text:
            p.text = p.text.replace("平均遮阳系数", "外窗平均综合遮阳系数")
        else:
            pass


# noinspection PyUnresolvedReferences
def unify_win_shading_table_jargon():
    """test under becs2018 居建"""

    def unify_win_shading_header(input_table):
        """统一窗遮阳系数表的表头”"""
        if not input_table:
            pass
        else:
            input_table.cell(0, 7).text = "窗本身遮阳系数SC"
            input_table.cell(0, 9).text = "建筑外遮阳系数SD"
            input_table.cell(0, 10).text = "外窗综合遮阳系数Sw"
            input_table.cell(len(input_table.column_cells(0)) - 1,
                             6).text = "朝向平均综合遮阳系数Sw"

    unify_win_shading_header(table_win["south"])
    unify_win_shading_header(table_win["north"])
    unify_win_shading_header(table_win["east"])
    unify_win_shading_header(table_win["west"])

    # 更改外窗构造表头
    table_win_constr.cell(0, 4).text = "窗本身遮阳系数SC"
    # 更改建筑综合平均遮阳系数计算表
    if check_stipulation is True:
        table_shading_overall.cell(0, 3).text = "朝向平均综合遮阳系数Sw"
        table_shading_overall.cell(5, 0).text = "朝向平均综合遮阳系数Sw"
    else:
        pass
    # 更改自定义外遮阳表头
    if table_shading["custom"]:
        table_shading["custom"].cell(0, 4).text = "平均遮阳系数SD"
    # 更改外遮阳表头
    if table_shading_outer:
        table_shading_outer.cell(0, 2).text = "建筑外遮阳系数SD"
    # 更改天窗表头
    if table_skylight_constr:
        table_skylight_constr.cell(0, 4).text = "窗本身遮阳系数SC"


def run(file_name, check_valid_document=True):
    global docx, sections, paras, tables
    global check_stipulation
    global constr_thermal, component_count_list, constr_project_count
    docx = Document(file_name)
    sections = docx.sections
    paras = docx.paragraphs
    tables = docx.tables

    # 检查规定性指标
    if check_valid_document is True and tables[0]._cells[0].text != "工程名称":
        promptError("不支持的文档，请确保指定文件是从BECS中导出的建筑节能计算书")
    if "规定性指标" in paras[1].text:
        check_stipulation = True
    else:
        pass

    # Parsing software, project, building info
    if check_valid_document is True:
        parse_software_project_building()
        # Erase cover and header of Word document
        erase_cover.erase("建筑节能计算书", docx, 6)

    # Parsing paragraphs
    parse_paragraphs()
    # Parse components
    # Parse get components
    get_component()
    # Purge items that contain 0 constr in global var component_count
    for item in tuple(component_count.items()):
        if not item[1]:
            del component_count[item[0]]
    # Get construction count
    count_list = list(component_count.values())
    component_count_list = list(component_count.values())
    constr_project_count = list_sum(count_list)
    # Parsing tables
    parse_tables()
    # Get construction thermal data
    constr_thermal = constr_thermal_sheet()

    if building_property == "居住建筑":
        logger.info("Gathering window, shading info")
        window()

    if global_control.settings["建筑节能计算书"][
            "统一所有门窗遮阳系数术语"] is True and building_property == "居住建筑":
        logger.info("Unifying 门窗遮阳系数 jargons")
        unify_win_shading_paras_jargon()
        unify_win_shading_table_jargon()

    # Gathering project info for statistics
    if check_valid_document is True:
        global_control.project_settings_int(building_property, software,
                                            project, building)
