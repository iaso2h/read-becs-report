import os
import logging
import logging.handlers
import sys
from PySide2.QtWidgets import QPlainTextEdit

home = os.path.expanduser('~')
settings_dir = os.path.join(home, "BECS tool")
log_file_path = os.path.join(settings_dir, "debug.log")


class Pyside2LogHandler(logging.Handler):
    def __init__(self, parentWidget):
        super().__init__()
        self.widget = QPlainTextEdit(parentWidget)
        self.widget.setReadOnly(True)
        self.setUplog()

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)

    def setUplog(self):
        widgetFormatter = logging.Formatter(
            fmt="%(asctime)s - %(name)s - [%(levelname)s]\n%("
                "message)s")
        self.setFormatter(widgetFormatter)

        streamFormatter = logging.Formatter(
            fmt="%(asctime)s - %(name)s - [%(levelname)s] - %(""message)s")
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(streamFormatter)

        logFileHandler = logging.handlers.RotatingFileHandler(
            filename=log_file_path,
            maxBytes=5 * 1024 * 1024)
        logFileHandler.setFormatter(streamFormatter)

        logging.basicConfig(level=logging.INFO,
                            handlers=[self, streamHandler, logFileHandler])
