import sys
warn_message = []


def raise_error(error_message, input_str=None):
    if error_message == 'bad_worksheet_name' and input_str is None:
        sys.exit("You're not ready yet, pls enter the right name")
    else:
        print('\n\n')
        sys.exit(error_message)


class File_not_found(Exception):
    pass


class file_name_error(Exception):
    pass


class Becs_nonestandard(Exception):
    pass


class Becs_simpleproject(Exception):
    pass


class file_name_inconsistency(Exception):
    pass


class Debug_stop(Exception):
    pass


exception_Text = r"""

░░▄▀░░░░░░░░░░░░░░░▀▀▄▄░░░░░ 
░░▄▀░░░░░░░░░░░░░░░░░░░░▀▄░░░ 
░▄▀░░░░░░░░░░░░░░░░░░░░░░░█░░ 
░█░░░░░░░░░░░░░░░░░░░░░░░░░█░ 
▐░░░░░░░░░░░░░░░░░░░░░░░░░░░█ 
█░░░░▀▀▄▄▄▄░░░▄▌░░░░░░░░░░░░▐ 
▌░░░░░▌░░▀▀█▀▀░░░▄▄░░░░░░░▌░▐ 
▌░░░░░░▀▀▀▀░░░░░░▌░▀██▄▄▄▀░░▐ 
▌░░░░░░░░░░░░░░░░░▀▄▄▄▄▀░░░▄▌ 
▐░░░░▐░░░░░░░░░░░░░░░░░░░░▄▀░ 
░█░░░▌░░▌▀▀▀▄▄▄▄░░░░░░░░░▄▀░░ 
░░█░░▀░░░░░░░░░░▀▌░░▌░░░█░░░░ 
░░░▀▄░░░░░░░░░░░░░▄▀░░▄▀░░░░░ 
░░░░░▀▄▄▄░░░░░░░░░▄▄▀▀░░░░░░░ 
░░░░░░░░▐▌▀▀▀▀▀▀▀▀░░░░░░░░░░░ 
░░░░░░░░█░░░░░░░░░░░░░░░░░░░░ 
░░╔═╗╔═╗╔═╗░░░░░║░║╔═╗║░║░░░░ 
░░╠═╣╠╦╝╠╣░░░░░░╚╦╝║░║║░║░░░░ 
░░║░║║╚═╚═╝░░░░░░║░╚═╝╚═╝░░░░ 
║╔═░╦░╦═╗╦═╗╦╔╗║╔═╗░░╔╦╗╔═╗╔╗ 
╠╩╗░║░║░║║░║║║║║║═╗░░║║║╠╣░╔╝ 
║░╚░╩░╩═╝╩═╝╩║╚╝╚═╝░░║║║╚═╝▄░ 

"""
