import PyInstaller.__main__
import shutil
import os
import global_control

if __name__ == "__main__":
    # remove tree
    try:
        shutil.rmtree('dist')
    except FileNotFoundError:
        pass
    except OSError:
        pass

    # change markdown file in place
    with open("./README.md", encoding="utf-8", mode="r+") as md_file:
        line_index = 0
        new_content = ''
        while True:
            line = md_file.readline()
            if line_index == 0:
                line = '去你大爷的斯维尔节能(BECS tool)v{}\n'.format(
                    global_control.version)
            else:
                pass
            if not line:
                break
            else:
                new_content += line
                line_index += 1
        md_file.seek(0)
        md_file.write(new_content)
    # Create TOC
    # os.system("create_md_toc.exe README.md cn")

    # Build executable file in .py program
    PyInstaller.__main__.run([
        '--name=BECS_tool',
        '--onefile',
        '--noconfirm',
        # '--add-data=./src/模板_设计报告书封面.docx;./src',
        # '--add-data=./src/模板_建筑节能设计专项说明.xlsx;./src',
        # '--add-data=./src/icons/*.ico;./src/icons',
        '--add-data=./.venv/Lib/site-packages/docxcompose/templates/*;./docxcompose/templates/',
        '--noconsole',
        '--icon={}'.format("./src/icons/icons8_tea.ico"),
        './main.py'
    ])
    # Command line
    # pyinstaller --noconfirm --onefile --name="BECS_tool" --add-data='./.venv/Lib/site-packages/docxcompose/templates/*;./docxcompose/templates/' --console --icon="./src/icons/icons8_tea.ico" main.py

    # copy file to /dist/src
    home = os.getcwd()
    src_dir = home + os.path.sep + "src"
    for file_name in os.listdir(home):
        if "README" in file_name:
            shutil.copy('./' + file_name, './dist/' + file_name)
        else:
            pass
    shutil.copytree("./src", "./dist/src")
    shutil.copytree("./md", "./dist/md")
    # Copy folder to desktop
    shutil.copytree(
        "./dist",
        f"{global_control.home}/Desktop/BECS toolv{global_control.version}")
