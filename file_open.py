import datetime
import logging
import os
import re
import sys

from PySide2.QtWidgets import QFileDialog

import global_control
from gui import multipleFileUI, myUI, browseSourceFileUI

logger = logging.getLogger(name=__name__)
wd = global_control.wd
output_file_dict = {}
timestr = datetime.datetime.now().strftime("%Y-%m-%d %H%M%S")


def gui_get_file_name_path(file_keyword):
    """Popups a getfile dialog and return file_name and file_path
    """
    myWidget = myUI.MyWidget()
    file_path = \
        QFileDialog.getOpenFileName(myWidget, "请选择文件", wd,
                                    "{}Word文档(*.docx)".format(
                                        file_keyword))[0]

    file_name = re.search(r"([^/]+)\.docx$", file_path).group()
    return file_name, file_path


def analyse_file():
    global wd
    global output_file_dict
    logger.info("Python " + sys.version)
    logger.info("BECS tool " + global_control.default_settings["version"])
    logger.info("Checking output type")
    # TODO: checking output necessity
    # Gathering output_type and corresponding keywords
    output_file_keyword_dict = global_control.settings["auto_match_keywords"].copy()
    for item in global_control.settings["output_mode"].items():
        if item[0] != "建筑节能设计专说明" and item[1] is False:
            del output_file_keyword_dict[item[0]]
    # Initial Match_file object and start auto matching files
    output_file_dict = {
        key_item[0]: Match_file(key_item)
        for key_item in output_file_keyword_dict.items()
        if "建筑节能设计专说明" != key_item[0]
    }
    if Match_file.stop_process is True:
        logger.info("No match files found, program abort")
        return

    # Manual match mode
    if global_control.settings["auto_match"] is False:
        widget = BrowseSourceFileWidget()
        widget.update_match_file_object(output_file_dict)
        widget.exec_()
    # Auto match mode
    else:
        match_object_list = list(output_file_dict.values())
        # Perform prefix collision check
        if Match_file.stop_process is False:
            match_object_list[0].compare_prefix(match_object_list)
        else:
            pass

    # Outputting files
    if Match_file.stop_process is False:
        if "建筑节能设计专项说明" in global_control.settings["output_mode"]:
            output_file_dict["建筑节能设计专项说明"] = "建筑节能设计专项说明"
        logger.info("Start processing Document files")
        import output
        output.output()
    else:
        logger.info("No match files found, program abort")


class BrowseSourceFileWidget(myUI.MyDialog):
    """Open up the browse source file dialog box"""

    def __init__(self):
        super(BrowseSourceFileWidget, self).__init__()
        self.setFixedSize(460, 150)
        self.ui = browseSourceFileUI.Ui_Form()
        self.ui.setupUi(self)

        # Checking vars
        self.find_all_files = False
        # Match object
        self.match_file_energy = None
        self.match_file_thermal = None
        self.match_file_exa = None

        # linking confirm & cancel btn
        self.ui.cancelBtn.clicked.connect(
            lambda: self.on_click_confirm_cancel_btn(self.ui.cancelBtn))
        self.ui.confirmBtn.clicked.connect(
            lambda: self.on_click_confirm_cancel_btn(self.ui.confirmBtn))

    def update_match_file_object(self, match_file_dict):
        """Setting up lineEdit widgets availabilities and Linking them with
        Match_file objects"""
        # ----Setting lineEdit widget availabilities
        for file in match_file_dict.keys():
            if "建筑节能计算书" in file:
                self.ui.energyBtn.setEnabled(True)
                self.ui.energyLineEdit.setEnabled(True)
                self.match_file_energy = match_file_dict[file]
            elif "隔热检查计算书" in file:
                self.ui.thermalBtn.setEnabled(True)
                self.ui.thermalLineEdit.setEnabled(True)
                self.match_file_thermal = match_file_dict[file]
            elif "建筑节能设计审查表" in file:
                self.ui.exaBtn.setEnabled(True)
                self.ui.exaLineEdit.setEnabled(True)
                self.match_file_exa = match_file_dict[file]
        # ----Connecting signal with slot funcs
        self.ui.energyBtn.clicked.connect(lambda: self.on_click_tool_btn(
            self.match_file_energy, self.ui.energyLineEdit))
        self.ui.thermalBtn.clicked.connect(lambda: self.on_click_tool_btn(
            self.match_file_thermal, self.ui.thermalLineEdit))
        self.ui.exaBtn.clicked.connect(lambda: self.on_click_tool_btn(
            self.match_file_exa, self.ui.exaLineEdit))

    # Override method
    def closeEvent(self, event):
        if self.find_all_files:
            event.accept()
        else:
            Match_file.stop_process = True
            event.accept()

    @staticmethod
    def on_click_tool_btn(match_object, lineEditWidget):
        """Store file info from file selected from GUI"""
        try:
            file_path = gui_get_file_name_path(
                match_object.file_keyword)[1]
            lineEditWidget.setText(file_path)
            match_object.file_name = file_path
            match_object.file_ext = ".docx"
            match_object.file_base_name = file_path[file_path.rindex("/") + 1:len(file_path) - 5]
            # get prefix
            # match_object.get_prefix(match_object.file_type_name)
        # When user cancel the dialog box
        except Exception:
            Match_file.stop_process = True

    def on_click_confirm_cancel_btn(self, btn_name):
        # checkBtn
        if btn_name.objectName() == "confirmBtn":
            enabled_line_edit_widgets = []
            if self.ui.energyLineEdit.isEnabled():
                enabled_line_edit_widgets.append(self.ui.energyLineEdit)
            if self.ui.thermalLineEdit.isEnabled():
                enabled_line_edit_widgets.append(self.ui.thermalLineEdit)
            if self.ui.exaLineEdit.isEnabled():
                enabled_line_edit_widgets.append(self.ui.exaLineEdit)
            # check all files are ready
            line_edit_truthy = tuple(
                map(lambda line_edit: not (line_edit.text() == ""),
                    enabled_line_edit_widgets))
            if all(line_edit_truthy):
                self.find_all_files = True
                self.close()
            else:
                myUI.MyMessageBox.about(myUI.MyWidget(), "文件不全", "请选择完所有文件的路径")
        # cancelBtn
        else:
            self.close()


class Match_file(object):
    list_dir = os.listdir(wd)
    # new class vars
    stop_process = False
    output_file_count = 0
    output_file_list = []

    def __init__(self, file_keyword_item: tuple):
        self.file_keyword = file_keyword_item[1]
        Match_file.output_file_count += 1
        Match_file.output_file_list.append(file_keyword_item[0])

        self.file_name = None
        self.file_ext = None
        self.file_base_name = None
        self.file_prefix = None
        self.line_edit_widget = None
        # Gathering file info
        if Match_file.stop_process is False:
            # Auto match
            if global_control.settings["auto_match"] is True:
                logger.info("Searching source files at {}".format(wd))
                self.auto_search_file(file_keyword_item)
            # NOTE: Manual match does not process in class initialization

    def auto_search_file(self, file_keyword_item):
        """Automatically finding Word document files contains certain
        patterns"""
        # get self.file_name, self.file_ext
        self.get_file_name_ext()
        # get file_base_name
        if self.file_name:
            self.file_base_name = self.file_name[:len(self.file_name) - 5]
        # Get prefix
        if self.file_name:
            self.get_prefix(file_keyword_item[1])

    @staticmethod
    def manual_select_file():
        """Selecting files via GUI"""
        pass

    def get_file_name_ext(self):
        """
        Check if the at one file per report file are found and tell the user
        which type is satisfied
        """

        def get_file_name(file_ext):
            """return match file and file extension"""
            match_files = [
                single_file for single_file in Match_file.list_dir
                if self.file_keyword +
                file_ext in single_file and "~$" not in single_file
            ]

            # Multiple match files
            if len(match_files) > 1:
                logger.warning("Multiple files containing keywords found")
                myDialog = myUI.MyDialog()
                myDialog.setFixedSize(440, 100)
                ui = multipleFileUI.Ui_Form()
                ui.setupUi(myDialog)
                ui.label_2.setText(
                    f"当前目录（{wd}）存在{len(match_files)}个文件包含“"
                    f"{self.file_keyword}{file_ext}”字样的文件\n\n请选择文件继续操作")
                ui.comboBox.addItems(match_files)

                def onClickConfirm():
                    current_text = ui.comboBox.currentText()
                    if current_text in match_files:
                        self.file_name = ui.comboBox.currentText()
                    # get file str from path
                    else:
                        self.file_name = re.search(r"([^/]+)\.docx$",
                                                   current_text).group()
                    myDialog.close()

                def onClickBrowse():
                    try:
                        file_path = gui_get_file_name_path(
                            self.file_keyword)[1]  # discard the first item
                        ui.comboBox.addItem(file_path)
                        ui.comboBox.setCurrentText(file_path)
                    # when use hit the cancel btn in the browse file
                    # dialog box
                    except Exception:
                        pass

                # link signal
                ui.pushButton.clicked.connect(onClickConfirm)
                ui.toolButton.clicked.connect(onClickBrowse)
                myDialog.exec_()
                # check self.file_name when dialog box is closed
                if not self.file_name:
                    logger.warning(
                        "No match file selected from multiple files which "
                        "contain keywords")
                    Match_file.stop_process = True
                else:
                    pass
            elif len(match_files) == 1:
                self.file_name = match_files[0]
            # no file found
            else:
                myUI.MyMessageBox.about(
                    myUI.MyWidget(), "提示",
                    "程序目录下（{}）\n没有找到包含“{}”字样的文件".format(wd, self.file_keyword))
                Match_file.stop_process = True

        self.file_ext = ".docx"
        get_file_name(self.file_ext)

    @classmethod
    def refresh_dir_file(cls):
        cls.list_dir = os.listdir(wd)

    @staticmethod
    def clear_state():
        Match_file.match_files = {}
        Match_file.browse_file_widget = []

    def get_prefix(self, file_type_name):
        """get file prefix based on given file type name"""
        # 广西居住建筑节能设计审查表
        if "广西居住建筑节能设计审查表" in self.file_base_name:
            file_type_name = "广西居住建筑节能设计审查表"
            result = re.search(r'.*(?={})'.format(file_type_name),
                               self.file_base_name).group()
        elif "广西公共建筑节能设计审查表" in self.file_base_name:
            file_type_name = "广西公共建筑节能设计审查表"
            result = re.search(r'.*(?={})'.format(file_type_name),
                               self.file_base_name).group()
        else:
            result = re.search(r'.*(?={})'.format(file_type_name),
                               self.file_base_name).group()

        self.file_prefix = result

    @staticmethod
    def compare_prefix(input_list):
        """Compare with all self.prefix match file object in the given list"""
        logger.info("Checking file prefix consistency")
        force_break = False
        for first_index, file1 in enumerate(input_list):
            # check when to break the for in loop
            if first_index == len(input_list) - 1:
                break
            # basic start of second_index
            second_index = first_index + 1

            # UGLY: break outside
            if force_break is False:
                while second_index < len(input_list):
                    file2 = input_list[second_index]
                    prefix1 = file1.file_prefix
                    prefix2 = file2.file_prefix

                    if prefix1 != prefix2:
                        text = "“{}”与“{}”的项目名称前缀不一致，是否继续当前操作继续吗？\n\n“{}” " \
                               "≠ “{}”".format(
                            file1.file_name, file2.file_name,
                            file1.file_prefix,
                            file2.file_prefix)
                        reply = myUI.MyMessageBox.question(
                            myUI.MyWidget(), "项目名称不一致", text,
                            myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                            myUI.MyMessageBox.No)
                        if reply == myUI.MyMessageBox.Yes:
                            # Continue the comparison
                            second_index += 1
                        else:
                            Match_file.stop_process = True
                            # UGLY
                            # break inside
                            force_break = True
                            second_index += 1
                            # break outside
                            break
                    else:
                        second_index += 1
