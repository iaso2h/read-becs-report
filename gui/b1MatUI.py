# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'b1mat.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient,
                           QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):

    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(327, 186)
        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 10, 311, 31))
        font = QFont()
        font.setPointSize(9)
        self.label_2.setFont(font)
        self.label_2.setWordWrap(True)
        self.groupBox1 = QGroupBox(Form)
        self.groupBox1.setObjectName(u"groupBox1")
        self.groupBox1.setGeometry(QRect(10, 50, 311, 61))
        self.groupBox1.setAlignment(Qt.AlignCenter)
        self.widget = QWidget(self.groupBox1)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(10, 20, 291, 31))
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.lineEdit = QLineEdit(self.widget)
        self.lineEdit.setObjectName(u"lineEdit")

        self.horizontalLayout_2.addWidget(self.lineEdit)

        self.addBtn = QPushButton(self.widget)
        self.addBtn.setObjectName(u"addBtn")

        self.horizontalLayout_2.addWidget(self.addBtn)

        self.groupBox1_2 = QGroupBox(Form)
        self.groupBox1_2.setObjectName(u"groupBox1_2")
        self.groupBox1_2.setGeometry(QRect(10, 120, 311, 61))
        self.groupBox1_2.setAlignment(Qt.AlignCenter)
        self.widget1 = QWidget(self.groupBox1_2)
        self.widget1.setObjectName(u"widget1")
        self.widget1.setGeometry(QRect(10, 20, 291, 28))
        self.horizontalLayout = QHBoxLayout(self.widget1)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.widget1)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.b1MatKeywordComboBox = QComboBox(self.widget1)
        self.b1MatKeywordComboBox.setObjectName(u"b1MatKeywordComboBox")
        self.b1MatKeywordComboBox.setMinimumSize(QSize(70, 0))

        self.horizontalLayout.addWidget(self.b1MatKeywordComboBox)

        self.deleteBtn = QPushButton(self.widget1)
        self.deleteBtn.setObjectName(u"deleteBtn")

        self.horizontalLayout.addWidget(self.deleteBtn)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.label_2.setText(
            QCoreApplication.translate(
                "Form",
                u"\u6240\u6709\u5305\u542b\u8fd9\u4e9b\u5173\u952e\u5b57\u7684\u6750\u6599\u4f1a\u9ed8\u8ba4\u586b\u5199\u4e3aB1\u7ea7\u71c3\u70e7\u6027\u80fd\uff0c\u5176\u4ed6\u6750\u6599\u586b\u5199\u4e3aA\u7ea7\u71c3\u70e7\u6027\u80fd",
                None))
        self.groupBox1.setTitle(
            QCoreApplication.translate("Form",
                                       u"\u6dfb\u52a0\u5173\u952e\u5b57", None))
        self.lineEdit.setPlaceholderText(
            QCoreApplication.translate(
                "Form",
                u"\u591a\u4e2a\u5173\u952e\u5b57\u7528\u7a7a\u683c\u5206\u5f00",
                None))
        self.addBtn.setText(
            QCoreApplication.translate("Form",
                                       u"\u6dfb\u52a0\u5173\u952e\u5b57", None))
        self.groupBox1_2.setTitle(
            QCoreApplication.translate("Form",
                                       u"\u5220\u9664\u5173\u952e\u5b57", None))
        self.label.setText(
            QCoreApplication.translate(
                "Form", u"B1\u7ea7\u6750\u6599\u5173\u952e\u5b57", None))
        self.deleteBtn.setText(
            QCoreApplication.translate("Form",
                                       u"\u5220\u9664\u5f53\u524d\u9009\u62e9",
                                       None))

    # retranslateUi
