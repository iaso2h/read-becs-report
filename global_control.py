import getpass
import logging
import os
import json

logger = logging.getLogger(name=__name__)
home = os.path.expanduser('~')
wd = os.getcwd()
logger.info("Current working directory {}".format(wd))

settings_dir = os.path.join(home, "BECS tool")
version = "0.0.13c"
if not os.path.exists(settings_dir):
    os.mkdir(settings_dir)

default_settings = {
    "settings_name": "settings",
    "version": version,
    "output_dir": f"{wd}\\Output",
    "output_mode": {
        "建筑节能计算书": True,
        "建筑节能设计审查表": True,
        "建筑节能设计专项说明": True,
        "隔热检查计算书": True
    },
    "auto_match": True,
    "auto_match_keywords": {
        "建筑节能计算书": "建筑节能计算书",
        "建筑节能设计审查表": "建筑节能设计审查表",
        "隔热检查计算书": "隔热检查计算书"
    },
    "全局设置": {
        "输出文件添加时间后缀": True,
        "显示完整的软件版本号": False,
        "开发者模式": False,
        "命令窗口模式": False,
    },
    "建筑节能计算书": {
        "去除斯维尔原页眉": True,
        "去除斯维尔原目录": False,
        "去除斯维尔原封面": True,
        "插入华蓝设计封面": True,
        "统一所有门窗遮阳系数术语": True,
    },
    "隔热检查计算书": {
        "去除斯维尔原页眉": True,
        "去除斯维尔原目录": False,
        "去除斯维尔原封面": True,
        "插入华蓝设计封面": True,
    },
    "建筑节能设计专项说明": {
        "sheet_output_mode": {
            "工程概况": True,
            "外围护结构构造及热工性能参数表": True,
            "外窗热工表": True,
            "外窗遮阳表": True,
            "节能设计指标一览表": True,
        },
        "工程概况": {
            "填写外围护结构主要节能措施": True,
            "填写最频繁使用的遮阳类型": True
        },
        "外围护结构构造及热工性能参数表": {
            "小数点后精确位数": 3,
            "B1材料关键字": ["聚苯"]
        },
        "外窗热工表": {
            "小数点后精确位数": 3
        },
        "外窗遮阳表": {
            "去除重复门窗的表格行": True
        }
    },
    "建筑节能设计审查表": {
        "检查外窗遮阳系数统一": True,
    }
}
default_project_settings = dict(settings_name="project settings",
                                version=version,
                                total_run_time=0,
                                user_name=getpass.getuser(),
                                current_running_project=None,
                                department_name="建筑工程七所",
                                department_setup_ask=[False, 0],
                                projects={})

settings = None
settings_json_path = os.path.join(settings_dir, "settings.json")

project_settings = None
project_settings_json_path = os.path.join(settings_dir, "project_settings.json")


def write_json(json_path, setting_var):
    """write .json file into user os"""
    with open(json_path, 'w', encoding='utf-8') as outfile:
        logger.info(f'Using {setting_var["settings_name"]} '
                    f'{setting_var["version"]}')
        json.dump(setting_var, outfile, ensure_ascii=False)
        return setting_var


def read_json(json_path):
    """read .json file from user os"""
    try:
        with open(json_path, 'r', encoding='utf-8') as infile:
            settings_var = json.load(infile)
            logger.info(f'Reading {settings_var["settings_name"]}'
                        f'{settings_var["version"]}')
        return settings_var
    except json.decoder.JSONDecodeError:
        import codecs
        settings_var = json.load(codecs.open(json_path, 'r', 'utf-8-sig'))
        logger.info(f'Reading {settings_var["settings_name"]}'
                    f'{settings_var["version"]}, decoding with "utf-8-sig"')
        return settings_var


def settings_init():
    global settings
    global settings_json_path
    global project_settings
    global project_settings_json_path

    def json_setup(json_path, default_settings_var):
        # First time launch program
        if not os.path.isfile(json_path):
            return write_json(json_path, default_settings_var)
        # Program launched before
        else:
            try:
                read_jason_setting = read_json(json_path)
                if read_jason_setting["version"] != default_settings_var[
                        "version"]:
                    # when critical change is made, overwrite the old version?
                    logger.info("Deprecated {} version {}".format(
                        read_jason_setting["settings_name"],
                        read_jason_setting["version"]))
                    # settings part
                    updated_settings = default_settings_var
                    # Update project settings
                    if read_jason_setting[
                            "settings_name"] == "project settings":
                        updated_settings["user_name"] = read_jason_setting[
                            "user_name"]
                        updated_settings["projects"] = read_jason_setting[
                            "projects"]
                        updated_settings["total_run_time"] = read_jason_setting[
                            "total_run_time"]
                        updated_settings[
                            "department_name"] = read_jason_setting[
                                "department_name"]
                        updated_settings[
                            "department_setup_ask"] = read_jason_setting[
                                "department_setup_ask"]
                        updated_settings[
                            "current_running_project"] = read_jason_setting[
                                "current_running_project"]
                    else:
                        pass

                    return write_json(json_path, updated_settings)
                else:
                    return read_jason_setting
            except Exception:
                logger.critical("Error occurs when reading json file settings")
                return write_json(json_path, default_settings_var)

    settings = json_setup(settings_json_path, default_settings)
    project_settings = json_setup(project_settings_json_path,
                                  default_project_settings)


# noinspection PyUnboundLocalVariable
def write_settings(input_settings):
    if input_settings == "settings":
        json_path = settings_json_path
        settings_var = settings
    elif input_settings == "project_settings":
        json_path = project_settings_json_path
        settings_var = project_settings
    else:
        exit(1)

    with open(json_path, 'w', encoding='utf-8') as outfile:
        json.dump(settings_var, outfile, ensure_ascii=False)


# noinspection PyUnresolvedReferences
def project_settings_int(building_property, software, project, building):
    try:
        if project_settings["projects"][project["name"]]:
            project_settings["current_running_project"] = project["name"]
    except KeyError:
        logger.info("Setting up new project settings")
        project_settings["current_running_project"] = project["name"]
        project_settings["projects"][project["name"]] = {
            "审核人": "输入姓名",
            "校核人": "输入姓名",
            "runtime": 0,
        }
    finally:
        project_settings["projects"][
            project["name"]][building_property] = {}
        project_settings["projects"][
            project["name"]][building_property]["software"] = software
        project_settings["projects"][
            project["name"]][building_property]["project"] = project
        project_settings["projects"][
            project["name"]][building_property]["building"] = building
