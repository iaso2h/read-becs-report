import os
import sys

from PySide2.QtGui import QFont

import global_control

from PySide2.QtWidgets import QStatusBar
from PySide2.QtCore import Qt

from gui import b1MatUI
from gui import UIUtil
from gui import myUI
from gui import settingsUI

parentdir = os.getcwd()
sys.path.append(parentdir)
MainUI = None
checkBoxCount = []


class SettingWidget(myUI.MyMainWindow):

    def __init__(self):
        super(SettingWidget, self).__init__()

        def onClickInfo():
            myUI.MyMessageBox.about(
                self, "提示", """ 公共建筑仅支持以下功能：
  
[1] 删除原斯维尔页眉
[2] 删除原斯维尔目录
[3] 插入华蓝设计封面
[4] 输出外围护结构构造及热工性能参数表""")

        def onClickDefault():
            # self.ui.energyGroup.setChecked(True)
            self.ui.thermalGroup.setChecked(True)
            self.ui.exaGroup.setChecked(True)
            self.ui.dwgGroup.setChecked(True)
            self.initGeneralSettings(global_control.default_settings)
            self.initEnergySettings(global_control.default_settings)
            self.initThermalSettings(global_control.default_settings)
            self.initDwgSheetSettings(global_control.default_settings)
            self.initDwgSheetData(global_control.default_settings)
            self.initExaSettings(global_control.default_settings)

        def onClickConfirm():
            global_control.settings["全局设置"][
                "显示完整的软件版本号"] = self.ui.completeVersionNameChkBtn.isChecked()
            global_control.settings["全局设置"][
                "输出文件添加时间后缀"] = self.ui.timeStampChkBtn.isChecked()
            global_control.settings["全局设置"][
                "命令窗口模式"] = self.ui.consoleModeChkBtn.isChecked()
            # -----------------------------------------------------------------
            # global_control.settings["output_mode"][
            #     "建筑节能计算书"] = self.ui.energyGroup.isChecked()
            global_control.settings["建筑节能计算书"][
                "去除斯维尔原页眉"] = self.ui.eraseHeaderChkBtn.isChecked()
            global_control.settings["建筑节能计算书"][
                "去除斯维尔原目录"] = self.ui.eraseTOCChkBtn.isChecked()
            global_control.settings["建筑节能计算书"][
                "去除斯维尔原封面"] = self.ui.eraseCoverChkBtn.isChecked()
            global_control.settings["建筑节能计算书"][
                "插入华蓝设计封面"] = self.ui.insertCoverChkBtn.isChecked()
            global_control.settings["建筑节能计算书"][
                "统一所有门窗遮阳系数术语"] = self.ui.unifyWinJargonChkBtn.isChecked()
            # -----------------------------------------------------------------
            # global_control.settings["output_mode"][
            #     "隔热检查计算书"] = self.ui.thermalGroup.isChecked()
            global_control.settings["隔热检查计算书"][
                "去除斯维尔原页眉"] = self.ui.eraseHeaderChkBtn_2.isChecked()
            global_control.settings["隔热检查计算书"][
                "去除斯维尔原目录"] = self.ui.eraseTOCChkBtn_2.isChecked()
            global_control.settings["隔热检查计算书"][
                "去除斯维尔原封面"] = self.ui.eraseCoverChkBtn_2.isChecked()
            global_control.settings["隔热检查计算书"][
                "插入华蓝设计封面"] = self.ui.insertCoverChkBtn_2.isChecked()
            # -----------------------------------------------------------------
            # global_control.settings["output_mode"][
            #     "建筑节能设计审查表"] = self.ui.exaGroup.isChecked()
            global_control.settings["建筑节能设计审查表"][
                "检查外窗遮阳系数统一"] = self.ui.checkWinShadingConsChkBtn.isChecked()
            # -----------------------------------------------------------------
            # global_control.settings["output_mode"][
            #     "建筑节能设计专项说明"] = self.ui.dwgGroup.isChecked()
            global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
                "工程概况"] = self.ui.projectGeneralGroup.isChecked()
            global_control.settings["建筑节能设计专项说明"]["工程概况"][
                "填写最频繁使用的遮阳类型"] = self.ui.mostUsedWinShadingChkBtn.isChecked()
            # -----------------------------------------------------------------
            global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
                "外围护结构构造及热工性能参数表"] = self.ui.outterComponentGroup.isChecked()
            global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"][
                "小数点后精确位数"] = self.ui.decimalPrecisionSpinBox.value()

            b1MaterKeywordItems = UIUtil.getComboBoxItems(
                self.ui.b1MatKeywordComboBox)
            global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"][
                "B1材料关键字"] = b1MaterKeywordItems
            # -----------------------------------------------------------------
            global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
                "外窗热工表"] = self.ui.winSheetGroup.isChecked()
            global_control.settings["建筑节能设计专项说明"]["外窗热工表"][
                "小数点后精确位数"] = self.ui.decimalPrecisionSpinBox_2.value()
            # -----------------------------------------------------------------
            global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
                "外窗遮阳表"] = self.ui.winShadingGroup.isChecked()
            global_control.settings["建筑节能设计专项说明"]["外窗遮阳表"][
                "去除重复门窗的表格行"] = self.ui.duplicateMergeChkBtn.isChecked()
            # -----------------------------------------------------------------
            global_control.settings["建筑节能设计专项说明"]["sheet_output_mode"][
                "节能设计指标一览表"] = self.ui.exaSheetChkBtn.isChecked()

            UIUtil.write_settings(self, "settings")
            self.settingChange = False
            self.close()

        def onClickClose():
            self.close()

        self.ui = settingsUI.Ui_Form()
        self.ui.setupUi(self)
        # Basic Btn signals
        self.ui.defaultBtn.clicked.connect(onClickDefault)
        self.ui.confirmBtn.clicked.connect(onClickConfirm)
        self.ui.closeBtn.clicked.connect(onClickClose)
        self.ui.infoBtn.clicked.connect(onClickInfo)
        # self.ui.closeBtn.set

        self.setWindowModality(Qt.ApplicationModal)
        self.setFixedSize(550, 500)
        # Initialize settings
        self.initOutputMode()
        self.initGeneralSettings(global_control.settings)
        self.initEnergySettings(global_control.settings)
        self.initThermalSettings(global_control.settings)
        self.initDwgSheetSettings(global_control.settings)
        self.initDwgSheetData(global_control.settings)
        self.initExaSettings(global_control.settings)
        self.setIcon()
        self.toolTip()
        self.statusBar()
        self.statusBarTip()
        self.btnDialogBox()

        self.ui.closeBtn.setFocus()
        self.settingChange = False

    def closeEvent(self, event):
        if self.settingChange:
            answer = myUI.MyMessageBox.question(
                self, "设置未保存", "是否放弃所作改动？",
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.No)
            if answer == myUI.MyMessageBox.Yes:
                event.accept()
            else:
                event.ignore()
        else:
            event.accept()

    def initOutputMode(self):

        def initBtnCheckState():
            # self.ui.energyGroup.toggled.connect(self.settingChangeCheck)
            self.ui.energyGroup.setTitle(
                global_control.settings["auto_match_keywords"]["建筑节能计算书"])

            if global_control.settings["output_mode"]["建筑节能计算书"] is True:
                self.ui.energyGroup.setChecked(Qt.CheckState.Checked)
            else:
                self.ui.energyGroup.setChecked(Qt.CheckState.Unchecked)

            self.ui.thermalGroup.toggled.connect(self.settingChangeCheck)
            self.ui.thermalGroup.setTitle(
                global_control.settings["auto_match_keywords"]["隔热检查计算书"])
            if global_control.settings["output_mode"]["隔热检查计算书"] is True:
                self.ui.thermalGroup.setChecked(Qt.CheckState.Checked)
            else:
                self.ui.thermalGroup.setChecked(Qt.CheckState.Unchecked)

            self.ui.exaGroup.toggled.connect(self.settingChangeCheck)
            self.ui.exaGroup.setTitle(
                global_control.settings["auto_match_keywords"]["建筑节能设计审查表"])
            if global_control.settings["output_mode"]["建筑节能设计审查表"] is True:
                self.ui.exaGroup.setChecked(Qt.CheckState.Checked)
            else:
                self.ui.exaGroup.setChecked(Qt.CheckState.Unchecked)

            self.ui.dwgGroup.toggled.connect(self.settingChangeCheck)
            if global_control.settings["output_mode"]["建筑节能设计专项说明"] is True:
                self.ui.dwgGroup.setChecked(Qt.CheckState.Checked)
            else:
                self.ui.dwgGroup.setChecked(Qt.CheckState.Unchecked)

        def energyOnClick():
            self.ui.energyGroup.setChecked(True)
            myUI.MyMessageBox.about(self, "说明",
                                    "当前的软件版本必须检查与输出建筑节能计算书\n\n该选项为必选选项，无法改变")

        initBtnCheckState()
        self.ui.energyGroup.clicked.connect(energyOnClick)

    def initGeneralSettings(self, settings_type):
        """input settings from global_control.settings or
        global_control.default_settings"""
        self.ui.completeVersionNameChkBtn.clicked.connect(
            self.settingChangeCheck)
        if settings_type["全局设置"]["显示完整的软件版本号"] is True:
            self.ui.completeVersionNameChkBtn.setCheckState(
                Qt.CheckState.Checked)
        else:
            self.ui.completeVersionNameChkBtn.setCheckState(
                Qt.CheckState.Unchecked)

        self.ui.timeStampChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["全局设置"]["输出文件添加时间后缀"] is True:
            self.ui.timeStampChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.timeStampChkBtn.setCheckState(Qt.CheckState.Unchecked)

        self.ui.consoleModeChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["全局设置"]["命令窗口模式"] is True:
            self.ui.consoleModeChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.consoleModeChkBtn.setCheckState(Qt.CheckState.Unchecked)

    def initEnergySettings(self, settings_type):

        self.ui.eraseCoverChkBtn.clicked.connect(self.settingChangeCheck)
        self.ui.eraseCoverChkBtn.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.eraseCoverChkBtn))
        if settings_type["建筑节能计算书"]["去除斯维尔原封面"] is True:
            self.ui.eraseCoverChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseCoverChkBtn.setCheckState(Qt.CheckState.Unchecked)

        self.ui.eraseHeaderChkBtn.clicked.connect(self.settingChangeCheck)
        self.ui.eraseHeaderChkBtn.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.eraseHeaderChkBtn))
        if settings_type["建筑节能计算书"]["去除斯维尔原页眉"] is True:
            self.ui.eraseHeaderChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseHeaderChkBtn.setCheckState(Qt.CheckState.Unchecked)

        self.ui.eraseTOCChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["建筑节能计算书"]["去除斯维尔原目录"] is True:
            self.ui.eraseTOCChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseTOCChkBtn.setCheckState(Qt.CheckState.Unchecked)

        self.ui.insertCoverChkBtn.clicked.connect(self.settingChangeCheck)
        self.ui.insertCoverChkBtn.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.insertCoverChkBtn))
        if settings_type["建筑节能计算书"]["插入华蓝设计封面"] is True:
            self.ui.insertCoverChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.insertCoverChkBtn.setCheckState(Qt.CheckState.Unchecked)

        self.ui.unifyWinJargonChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["建筑节能计算书"]["统一所有门窗遮阳系数术语"] is True:
            self.ui.unifyWinJargonChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.unifyWinJargonChkBtn.setCheckState(Qt.CheckState.Unchecked)

    def initThermalSettings(self, settings_type):
        self.ui.eraseCoverChkBtn_2.clicked.connect(self.settingChangeCheck)
        self.ui.eraseCoverChkBtn_2.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.eraseCoverChkBtn_2))
        if settings_type["隔热检查计算书"]["去除斯维尔原封面"] is True:
            self.ui.eraseCoverChkBtn_2.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseCoverChkBtn_2.setCheckState(Qt.CheckState.Unchecked)

        self.ui.eraseHeaderChkBtn_2.clicked.connect(self.settingChangeCheck)
        self.ui.eraseHeaderChkBtn_2.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.eraseHeaderChkBtn_2))
        if settings_type["隔热检查计算书"]["去除斯维尔原页眉"] is True:
            self.ui.eraseHeaderChkBtn_2.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseHeaderChkBtn_2.setCheckState(Qt.CheckState.Unchecked)

        self.ui.eraseTOCChkBtn_2.clicked.connect(self.settingChangeCheck)
        if settings_type["隔热检查计算书"]["去除斯维尔原目录"] is True:
            self.ui.eraseTOCChkBtn_2.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.eraseTOCChkBtn_2.setCheckState(Qt.CheckState.Unchecked)

        self.ui.insertCoverChkBtn_2.clicked.connect(self.settingChangeCheck)
        self.ui.insertCoverChkBtn_2.clicked.connect(
            lambda: self.onClickInsertCoverChkBtn(self.ui.insertCoverChkBtn_2))
        if settings_type["隔热检查计算书"]["插入华蓝设计封面"] is True:
            self.ui.insertCoverChkBtn_2.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.insertCoverChkBtn_2.setCheckState(Qt.CheckState.Unchecked)

    def initExaSettings(self, settings_type):
        self.ui.checkWinShadingConsChkBtn.clicked.connect(
            self.settingChangeCheck)
        if settings_type["建筑节能设计审查表"]["检查外窗遮阳系数统一"] is True:
            self.ui.checkWinShadingConsChkBtn.setCheckState(
                Qt.CheckState.Checked)
        else:
            self.ui.checkWinShadingConsChkBtn.setCheckState(
                Qt.CheckState.Unchecked)

    def initDwgSheetSettings(self, settings_type):
        self.ui.projectGeneralGroup.toggled.connect(self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["sheet_output_mode"]["工程概况"] is True:
            self.ui.projectGeneralGroup.setChecked(Qt.CheckState.Checked)
        else:
            self.ui.projectGeneralGroup.setChecked(Qt.CheckState.Unchecked)

        self.ui.mostUsedWinShadingChkBtn.clicked.connect(
            self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["工程概况"]["填写最频繁使用的遮阳类型"] is True:
            self.ui.mostUsedWinShadingChkBtn.setCheckState(
                Qt.CheckState.Checked)
        else:
            self.ui.mostUsedWinShadingChkBtn.setCheckState(
                Qt.CheckState.Unchecked)
        # -----------------------------------------------------------------
        self.ui.outterComponentGroup.toggled.connect(self.settingChangeCheck)
        self.ui.decimalPrecisionSpinBox.textChanged.connect(
            self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["sheet_output_mode"][
                "外围护结构构造及热工性能参数表"] is True:
            self.ui.outterComponentGroup.setChecked(Qt.CheckState.Checked)
        else:
            self.ui.outterComponentGroup.setChecked(Qt.CheckState.Unchecked)
        # -----------------------------------------------------------------
        self.ui.winSheetGroup.toggled.connect(self.settingChangeCheck)
        self.ui.decimalPrecisionSpinBox_2.textChanged.connect(
            self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["sheet_output_mode"]["外窗热工表"] is True:
            self.ui.winSheetGroup.setChecked(Qt.CheckState.Checked)
        else:
            self.ui.winSheetGroup.setChecked(Qt.CheckState.Unchecked)
        # -----------------------------------------------------------------
        self.ui.winShadingGroup.toggled.connect(self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["sheet_output_mode"]["外窗遮阳表"] is True:
            self.ui.winShadingGroup.setChecked(Qt.CheckState.Checked)
        else:
            self.ui.winShadingGroup.setChecked(Qt.CheckState.Unchecked)

        self.ui.duplicateMergeChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["外窗遮阳表"]["去除重复门窗的表格行"] is True:
            self.ui.duplicateMergeChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.duplicateMergeChkBtn.setCheckState(Qt.CheckState.Unchecked)
        # -----------------------------------------------------------------
        self.ui.exaSheetChkBtn.clicked.connect(self.settingChangeCheck)
        if settings_type["建筑节能设计专项说明"]["sheet_output_mode"]["节能设计指标一览表"] is True:
            self.ui.exaSheetChkBtn.setCheckState(Qt.CheckState.Checked)
        else:
            self.ui.exaSheetChkBtn.setCheckState(Qt.CheckState.Unchecked)

    def initDwgSheetData(self, settings_type):
        self.ui.b1MatKeywordComboBox.addItems(
            settings_type["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"]["B1材料关键字"])
        self.ui.decimalPrecisionSpinBox.setValue(
            settings_type["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"]["小数点后精确位数"])
        self.ui.decimalPrecisionSpinBox_2.setValue(
            settings_type["建筑节能设计专项说明"]["外窗热工表"]["小数点后精确位数"])

    def setIcon(self):
        UIUtil.setIcon(self.ui.closeBtn, "icons8_cancel")
        UIUtil.setIcon(self.ui.confirmBtn, "icons8_approval")
        UIUtil.setIcon(self.ui.defaultBtn, "icons8_default")

    def toolTip(self):
        self.ui.eraseTOCToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseTOCToolTip.png"))
        self.ui.eraseTOCToolBtn.setToolTipDuration(15000)

        self.ui.eraseTOCToolBtn_2.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseTOCToolTip.png"))
        self.ui.eraseTOCToolBtn_2.setToolTipDuration(15000)

        self.ui.eraseCoverToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseCoverToolTip.png"))
        self.ui.eraseCoverToolBtn.setToolTipDuration(15000)
        self.ui.eraseCoverToolBtn_2.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseCoverToolTip.png"))
        self.ui.eraseCoverToolBtn_2.setToolTipDuration(15000)

        self.ui.eraseHeaderToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseHeaderToolTip.png"))
        self.ui.eraseHeaderToolBtn.setToolTipDuration(15000)
        self.ui.eraseHeaderToolBtn_2.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "eraseHeaderToolTip.png"))
        self.ui.eraseHeaderToolBtn_2.setToolTipDuration(15000)

        self.ui.insertCoverToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "insertCoverToolTip.png"))
        self.ui.insertCoverToolBtn.setToolTipDuration(15000)
        self.ui.insertCoverToolBtn_2.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "insertCoverToolTip.png"))
        self.ui.insertCoverToolBtn_2.setToolTipDuration(15000)

        self.ui.unifyWinJargonToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "unifyWinJargonToolTip.png"))
        self.ui.unifyWinJargonToolBtn.setToolTipDuration(15000)
        self.ui.checkWinShadingConsToolBtn.setToolTip(
            '<img src="{}/md/{}">'.format(parentdir,
                                          "checkWinShadingConsToolTip.png"))
        self.ui.checkWinShadingConsToolBtn.setToolTipDuration(15000)

        self.ui.mostUsedWinShadingToolBtn.setToolTip(
            '<img src="{}/md/{}">'.format(parentdir,
                                          "mostUsedWinShadingToolTip.png"))
        self.ui.mostUsedWinShadingToolBtn.setToolTipDuration(15000)

        self.ui.b1MatKeywordToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "b1MatKeywordToolTip.png"))
        self.ui.b1MatKeywordToolBtn.setToolTipDuration(15000)

        self.ui.decimalPrecisionToolBtn.setToolTip(
            '<img src="{}/md/{}">'.format(parentdir,
                                          "decimalPrecisionToolTip.png"))
        self.ui.decimalPrecisionToolBtn.setToolTipDuration(15000)

        self.ui.decimalPrecisionToolBtn_2.setToolTip(
            '<img src="{}/md/{}">'.format(parentdir,
                                          "decimalPrecisionToolTip_2.png"))
        self.ui.decimalPrecisionToolBtn_2.setToolTipDuration(15000)

        self.ui.duplicateMergeToolBtn.setToolTip('<img src="{}/md/{}">'.format(
            parentdir, "duplicateMergeToolTip.png"))
        self.ui.duplicateMergeToolBtn.setToolTipDuration(15000)

    def statusBar(self):
        statusBar = QStatusBar(self)
        # self.statusbar.setPalette(palette)
        font = QFont()
        font.setFamily(u"\u5fae\u8f6f\u96c5\u9ed1")
        statusBar.setFont(font)
        statusBar.setStyleSheet(u"background-color: rgb(150, 165, 210);\n"
                                "color: rgb(255, 255, 255);")
        self.setStatusBar(statusBar)

    def statusBarTip(self):
        self.ui.insertCoverToolBtn.setStatusTip("点击设置封面项目信息")
        self.ui.insertCoverToolBtn_2.setStatusTip("点击设置封面项目信息")

        self.ui.b1MatKeywordToolBtn.setStatusTip("点击设置B1级燃烧性能材料关键字")

    def btnDialogBox(self):

        def onClickInsertCoverToolBtn():
            """Open up the cover info setting dialog box"""
            myUI.MyMessageBox.about(
                self, "提示", "计算书封面的项目信息的改动会自动反映到下一次运行输出的的计算书文件上\n\n"
                "每次输出完成后，请手动对计算书的封面的“页数”和“日期”的Word域进行“更新域”的操作")
            widget = UIUtil.ProjectCoverWidget()
            widget.exec_()

        def onClickB1Mat():
            widget = B1MatWidget()
            widget.exec_()
            self.ui.b1MatKeywordComboBox.clear()
            self.ui.b1MatKeywordComboBox.addItems(
                global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"]
                ["B1材料关键字"])

        def onClickConsole():
            if self.ui.consoleModeChkBtn.isChecked() is True:
                reply = myUI.MyMessageBox.warning(
                    self, "警告", "此功能为开发者使用的模式，确定要使用吗？",
                    myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                    myUI.MyMessageBox.No)
                if reply == myUI.MyMessageBox.Yes:
                    pass
                else:
                    self.ui.consoleModeChkBtn.setChecked(False)
            else:
                pass

        self.ui.consoleModeChkBtn.clicked.connect(onClickConsole)
        self.ui.insertCoverToolBtn.clicked.connect(onClickInsertCoverToolBtn)
        self.ui.insertCoverToolBtn_2.clicked.connect(onClickInsertCoverToolBtn)
        self.ui.b1MatKeywordToolBtn.clicked.connect(onClickB1Mat)

    def onClickInsertCoverChkBtn(self, Btn):
        """When insert cover enable you have to also enable erase cover
        and erase header at the same time"""
        # prompt the user infomation
        # OPTIMIZE: append all widgets to a list and check bind all widgets
        #  within the list with coresponding slots
        global checkBoxCount
        if len(checkBoxCount) == 3:
            checkBoxCount = []
        else:
            pass

        self.ui.checkWinShadingConsChkBtn.objectName()
        if Btn == self.ui.insertCoverChkBtn:
            if Btn.isChecked():
                self.ui.eraseHeaderChkBtn.setChecked(True)
                self.ui.eraseCoverChkBtn.setChecked(True)
            else:
                pass
        elif Btn == self.ui.insertCoverChkBtn_2:
            if Btn.isChecked():
                self.ui.eraseHeaderChkBtn_2.setChecked(True)
                self.ui.eraseCoverChkBtn_2.setChecked(True)
            else:
                pass
        elif (Btn == self.ui.eraseCoverChkBtn) or (
                Btn == self.ui.eraseHeaderChkBtn):
            if self.ui.insertCoverChkBtn.isChecked():
                Btn.setChecked(True)
                checkBoxCount.append(Btn.objectName())
                if checkBoxCount.count(Btn.objectName()) == 3:
                    myUI.MyMessageBox.about(self, "提示", "当开启插入华蓝封面时无法取消该选项")
            else:
                pass
        elif (Btn == self.ui.eraseCoverChkBtn_2) or (
                Btn == self.ui.eraseHeaderChkBtn_2):
            if self.ui.insertCoverChkBtn_2.isChecked():
                Btn.setChecked(True)
                checkBoxCount.append(Btn.objectName())
                if checkBoxCount.count(Btn.objectName()) == 3:
                    myUI.MyMessageBox.about(self, "提示", "当开启插入华蓝封面时无法取消该选项")
            else:
                pass
        else:
            pass

    def settingChangeCheck(self):
        self.settingChange = True


class B1MatWidget(myUI.MyDialog):

    def __init__(self):
        super(B1MatWidget, self).__init__()

        self.ui = b1MatUI.Ui_Form()
        self.ui.setupUi(self)
        self.setFixedSize(330, 190)
        self.setWindowTitle("自定义B1材料关键字")
        self.originalMatList = []
        self.loadPreset(global_control.settings)
        self.ui.addBtn.clicked.connect(lambda: self.btnSignal(self.ui.addBtn))
        self.ui.deleteBtn.clicked.connect(
            lambda: self.btnSignal(self.ui.deleteBtn))

    def btnSignal(self, btn):

        def writeRefresh():
            global_control.settings["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"][
                "B1材料关键字"] = self.originalMatList
            UIUtil.write_settings(self, "settings")
            self.loadPreset(global_control.settings)

        if btn.objectName() == "addBtn":
            lineEditText = self.ui.lineEdit.text()
            if not lineEditText:
                myUI.MyMessageBox.warning(self, "输入错误", "材料关键字不能为空",
                                          myUI.MyMessageBox.Ok,
                                          myUI.MyMessageBox.Ok)
                self.ui.lineEdit.setFocus()
            else:
                self.ui.lineEdit.clear()
                newList = lineEditText.split(" ")
                newList = list(
                    filter(lambda newStr: ((not newStr) == False), newList))
                self.originalMatList += newList
                writeRefresh()
        else:
            currentText = self.ui.b1MatKeywordComboBox.currentText()
            reply = myUI.MyMessageBox.question(
                self, "是否删除",
                "即将要删除当前选择的关键字“{}”, 是否确定当前操作？".format(currentText),
                myUI.MyMessageBox.Yes | myUI.MyMessageBox.No,
                myUI.MyMessageBox.No)
            if reply == myUI.MyMessageBox.Yes:
                currentTextIndex = self.originalMatList.index(currentText)
                del self.originalMatList[currentTextIndex]
                writeRefresh()
            else:
                pass

    def loadPreset(self, settings_type):
        self.originalMatList = settings_type["建筑节能设计专项说明"]["外围护结构构造及热工性能参数表"][
            "B1材料关键字"]
        self.ui.b1MatKeywordComboBox.clear()
        self.ui.b1MatKeywordComboBox.addItems(self.originalMatList)
