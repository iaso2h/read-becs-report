import os
import unittest
import logging
import global_control
from becs import becs_energy_process as energy

file_dir = os.getcwd() + os.sep + "energy"
files = os.listdir(file_dir)
file_list = [
    file_dir + os.sep + file
    for file in files
    if "~$" not in file and ".py" not in file and ".docx" in file
]
logger = logging.getLogger(name=__name__)
logging.basicConfig(level=logging.CRITICAL,
                    format="%(levelname)s %(threadName)s %(name)s %(message)s")
global_control.settings = global_control.default_settings
global_control.project_settings = global_control.default_project_settings


def reinitialize_var():
    energy.Parameters.cmp_x_factor = []
    energy.Parameters.cmp_constr_count = []
    energy.Parameters.cmp_row_index_sum = []
    energy.table_constr_overall = None
    energy.table_constr_list = []
    energy.table_wall_win_ratio = None
    energy.table_k_d_roof = None
    energy.table_k_d_wall = dict(south=None,
                                 north=None,
                                 east=None,
                                 west=None,
                                 overall=None)
    energy.table_skylight_ratio = None
    energy.table_skylight_constr = None
    energy.table_win_constr = None
    energy.table_win_air_permeate = None
    energy.table_win = dict(south=None, north=None, east=None, west=None)
    energy.table_shading_outer = None
    energy.table_shading = dict(custom=None, blinds=None, plate=None)
    energy.table_shading_overall = None
    # Check
    energy.check_deformation = False
    energy.check_stipulation = False
    energy.check_win = dict(skylight=False,
                            south=True,
                            north=True,
                            east=True,
                            west=True)
    energy.check_shading = dict(plate=False, blinds=False, custom=False)
    energy.check_wall = dict(south=True,
                             north=True,
                             east=True,
                             west=True,
                             overall=True)
    energy.check_skylight_constr_unique = False
    energy.check_win_constr_unique = False
    # Document data
    energy.building_property = None
    energy.basis = []
    energy.software = {}
    energy.project = {}
    energy.building = {}
    energy.components = []
    energy.component_count = dict(屋顶构造=0,
                                  外墙相关构造=0,
                                  挑空楼板构造=0,
                                  架空或外挑楼板=0,
                                  凸窗顶板=0,
                                  凸窗侧板=0)
    energy.component_count_list = None
    energy.constr_project_count = None
    energy.component_parse_slice_dict = dict(屋顶构造=[],
                                             外墙相关构造=[],
                                             挑空楼板构造=[],
                                             架空或外挑楼板=[],
                                             凸窗顶板=[],
                                             凸窗侧板=[])
    energy.constr_thermal = None
    energy.roof_k_d = None
    energy.walls_k_d = dict(south=None,
                            north=None,
                            east=None,
                            west=None,
                            overall=None)
    energy.win_wall_ratio = []
    energy.win_constr = []
    energy.win_permeate = None
    energy.win_shading_factor = dict(south=None,
                                     north=None,
                                     east=None,
                                     west=None,
                                     overall=None)
    energy.skylight = []
    energy.shading_constr = {}
    energy.shading_win_constr = dict(south=None,
                                     north=None,
                                     east=None,
                                     west=None)


projects_component_dict = {}
projects_construction_count_dict = {}
projects_construction_table_count_dict = {}
projects_roof_k_d_dict = {}
projects_wall_k_d_dict = {}
projects_win_shading_factor = {}
for i_index, file in enumerate(file_list):
    if i_index >= 0:
        energy.run(file, False)
        logger.critical(f"Current file: {file}  Index: [{i_index}]")
        # Component & Construction
        projects_component_dict[file] = energy.components
        projects_construction_count_dict[file] = energy.constr_project_count
        projects_construction_table_count_dict[file] = len(
            energy.table_constr_list)
        # K & D
        projects_roof_k_d_dict[file] = energy.roof_k_d
        projects_wall_k_d_dict[file] = energy.walls_k_d["overall"]
        # Win shading factor
        del energy.win_shading_factor["overall"]
        projects_win_shading_factor[file] = energy.win_shading_factor
        # Reinitialization
        reinitialize_var()

print("\n\n\n\n")


class Test_Component_Constr(unittest.TestCase):
    maxDiff = None

    # 1#建筑节能计算书.docx
    def test_project01(self):
        key = '1#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '上人保温隔热平屋面(有保温层,I级防水)', '剪力墙构造一（东西）', '外墙构造二（南北）', '剪力墙构造二（南北）',
            '热桥梁构造二（南北）', '外墙构造一(东西)', '热桥梁构造一（东西）', '热桥板', '凸窗顶板'
        ]

        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 123132#建筑节能计算书.docx
    def test_project02(self):
        key = '123132#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造一', '户内客厅、餐厅、卧室、走道、玄关、阳台外墙构造一', '户内客厅、餐厅、卧室、走道、玄关、阳台热桥柱构造一',
            '户内客厅、餐厅、卧室、走道、玄关、阳台热桥梁构造一', '户内客厅、餐厅、卧室、走道、玄关、阳台热桥板构造一'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 2#建筑节能计算书.docx
    def test_project03(self):
        key = '2#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = ['屋顶', '外墙', '热桥梁', '热桥柱', '热桥板', '凸窗顶板构造一']
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 28#建筑节能计算书.docx
    def test_project04(self):
        key = '28#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '南北向外墙', '东西向外墙',
            '南北向热桥梁', '热桥柱构造一', '东西向热桥梁', '热桥板构造一'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 29#建筑节能计算书.docx
    def test_project05(self):
        key = '29#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '南北向外墙', '东西向外墙',
            '南北向热桥梁', '热桥柱构造一', '东西向热桥梁', '热桥板构造一', '阳台隔墙'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 粮油建筑节能计算书.docx
    def test_project06(self):
        key = '粮油建筑节能计算书.docx'
        component_list = [
            '外墙构造一', '热桥梁构造一', '热桥柱构造一', '热桥板构造一', '屋顶构造一（不上人屋面）', '屋顶构造三（坡屋面）'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 7#商业.docx
    def test_project07(self):
        key = '7#商业.docx'
        key = file_dir + os.sep + key
        component_list = ['平屋面', '外墙', '热桥柱', '热桥梁', '热桥板']
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx
    def test_project08(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '东西向外墙', '南北向外墙',
            '热桥柱构造一', '南北向热桥梁', '东西向热桥梁', '热桥板构造一', "阳台隔墙", '挑空楼板'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 29#桂林夏热冬冷地区建筑节能设计报告书.docx
    def test_project09(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '东西向外墙', '南北向外墙',
            '热桥柱构造一', '南北向热桥梁', '东西向热桥梁', '热桥板构造一', "阳台隔墙", '挑空楼板'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_table_count_dict[key])

    # 29#河池夏热冬冷地区北区建筑节能设计报告书.docx
    def test_project10(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）',
            '屋顶构造一（上人屋面一）',
            '屋顶构造二（上人屋面二）',
            '东西向外墙',
            '南北向外墙',
            '热桥柱构造一',
            '南北向热桥梁',
            '东西向热桥梁',
            '热桥板构造一',
            '阳台隔墙',
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx
    def test_project11(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）',
            '屋顶构造一（上人屋面一）',
            '屋顶构造二（上人屋面二）',
            '东西向外墙',
            '南北向外墙',
            '热桥柱构造一',
            '南北向热桥梁',
            '东西向热桥梁',
            '热桥板构造一',
            '阳台隔墙',
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 29#建筑节能设计报告书.docx
    def test_project12(self):
        key = '29#建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '南北向外墙', '东西向外墙',
            '南北向热桥梁', '热桥柱构造一', '东西向热桥梁', '热桥板构造一', '阳台隔墙', "凸窗顶板"
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 29#建筑节能设计报告书_性能指标.docx
    def test_project13(self):
        key = '29#建筑节能设计报告书_性能指标.docx'
        key = file_dir + os.sep + key
        component_list = [
            '屋顶构造三（坡屋面）', '屋顶构造一（上人屋面一）', '屋顶构造二（上人屋面二）', '南北向外墙', '东西向外墙',
            '南北向热桥梁', '热桥柱构造一', '东西向热桥梁', '热桥板构造一', '阳台隔墙', "凸窗顶板"
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 粮油北区挑空楼板顶板底板建筑节能设计报告1.docx
    def test_project14(self):
        key = '粮油北区挑空楼板顶板底板建筑节能设计报告1.docx'
        key = file_dir + os.sep + key
        component_list = [
            '外墙构造一（面砖外墙）', '热桥梁构造一（面砖梁）', '热桥柱构造一（面砖柱）', '热桥板构造一',
            '屋顶构造二（不上人屋面）', '屋顶构造一（上人屋面）', '挑空楼板'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 粮油建筑节能计算书.docx
    def test_project15(self):
        key = '粮油建筑节能计算书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '外墙构造一', '热桥梁构造一', '热桥柱构造一', '热桥板构造一', '屋顶构造一（不上人屋面）', '屋顶构造三（坡屋面）'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])

    # 粮油建筑节能设计报告书.docx
    def test_project16(self):
        key = '粮油建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        component_list = [
            '外墙构造一（面砖外墙）', '热桥梁构造一（面砖梁）', '热桥柱构造一（面砖柱）', '热桥板构造一',
            '屋顶构造二（不上人屋面）', '屋顶构造一（上人屋面）', '挑空楼板'
        ]
        if key in file_list:
            self.assertCountEqual(component_list, projects_component_dict[key])
            self.assertEqual(len(component_list),
                             projects_construction_count_dict[key])


class Test_K_D(unittest.TestCase):
    maxDiff = None

    # 1#建筑节能计算书.docx
    def test_project01(self):
        key = '1#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.83, 3.226]
        wall_k_d = [0.91, 3.99]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 123132#建筑节能计算书.docx
    def test_project02(self):
        key = '123132#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.61, 3.527]
        wall_k_d = [0.71, 3.71]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 2#建筑节能计算书.docx
    def test_project03(self):
        key = '2#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.69, 2.785]
        wall_k_d = [1.48, 3.35]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 28#建筑节能计算书.docx
    def test_project04(self):
        key = '28#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.66]
        wall_k_d = [1.91, 0]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#建筑节能计算书.docx
    def test_project05(self):
        key = '29#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.64]
        wall_k_d = [1.61, 3.02]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx
    def test_project08(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0, 0]
        wall_k_d = [1.84, 3.03]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#桂林夏热冬冷地区建筑节能设计报告书.docx
    def test_project09(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0, 0]
        wall_k_d = [1.84, 3.03]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#河池夏热冬冷地区北区建筑节能设计报告书.docx
    def test_project10(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.64]
        wall_k_d = [1.68, 3.03]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx
    def test_project11(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.64]
        wall_k_d = [1.68, 3.03]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#建筑节能设计报告书.docx
    def test_project12(self):
        key = '29#建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.64]
        wall_k_d = [1.61, 3.02]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)

    # 29#建筑节能设计报告书_性能指标.docx
    def test_project13(self):
        key = "29#建筑节能设计报告书_性能指标.docx"
        key = file_dir + os.sep + key
        roof_k_d = [0.86, 2.64]
        wall_k_d = [1.61, 3.02]

        if key in file_list:
            if projects_roof_k_d_dict[key]:
                self.assertAlmostEqual(roof_k_d[0],
                                       projects_roof_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(roof_k_d[1],
                                       projects_roof_k_d_dict[key][1],
                                       delta=0.02)
            if projects_wall_k_d_dict[key]:
                self.assertAlmostEqual(wall_k_d[0],
                                       projects_wall_k_d_dict[key][0],
                                       delta=0.02)
                self.assertAlmostEqual(wall_k_d[1],
                                       projects_wall_k_d_dict[key][1],
                                       delta=0.02)


class Test_Win_Shading_Factor_Constr(unittest.TestCase):
    maxDiff = None

    # 1#建筑节能计算书.docx
    def test_project01(self):
        key = '1#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.82, 0.61),
                                  north=(0.91, 0.68),
                                  east=(0.66, 0.50),
                                  west=(0.68, 0.51))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 123132#建筑节能计算书.docx
    def test_project02(self):
        key = '123132#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(east=(0.66, 0.3),
                                  north=('－', '－'),
                                  south=('－', '－'),
                                  west=('－', '－'))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 2#建筑节能计算书.docx
    def test_project03(self):
        key = '2#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(east=(0.614, 0.522),
                                  north=('－', '－'),
                                  south=('－', '－'),
                                  west=(0.632, 0.537))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 28#建筑节能计算书.docx
    def test_project04(self):
        key = '28#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.83, 0.63),
                                  north=(0.86, 0.65),
                                  east=(0.67, 0.50),
                                  west=(0.67, 0.50))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#建筑节能计算书.docx
    def test_project05(self):
        key = '29#建筑节能计算书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.84, 0.60),
                                  north=(0.84, 0.60),
                                  east=(0.67, 0.49),
                                  west=(0.67, 0.47))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx
    def test_project08(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告_性能docx.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.870, 0.619),
                                  north=(0.919, 0.655),
                                  east=(0.639, 0.469),
                                  west=(0.665, 0.473))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#桂林夏热冬冷地区建筑节能设计报告书.docx
    def test_project09(self):
        key = '29#桂林夏热冬冷地区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.870, 0.619),
                                  north=(0.919, 0.655),
                                  east=(0.639, 0.469),
                                  west=(0.665, 0.473))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#河池夏热冬冷地区北区建筑节能设计报告书.docx
    def test_project10(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.86, 0.61),
                                  north=(0.89, 0.63),
                                  east=(0.64, 0.46),
                                  west=(0.67, 0.47))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx
    def test_project11(self):
        key = '29#河池夏热冬冷地区北区建筑节能设计报告书_性能指标.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.859, 0.613),
                                  north=(0.890, 0.634),
                                  east=(0.636, 0.461),
                                  west=(0.665, 0.473))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#建筑节能设计报告书.docx
    def test_project12(self):
        key = '29#建筑节能设计报告书.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.85, 0.61),
                                  north=(0.86, 0.61),
                                  east=(0.68, 0.50),
                                  west=(0.70, 0.50))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])

    # 29#建筑节能设计报告书_性能指标.docx
    def test_project13(self):
        key = '29#建筑节能设计报告书_性能指标.docx'
        key = file_dir + os.sep + key
        shading_win_constr = dict(south=(0.853, 0.607),
                                  north=(0.859, 0.612),
                                  east=(0.680, 0.497),
                                  west=(0.704, 0.500))
        if key in file_list:
            self.assertEqual(shading_win_constr,
                             projects_win_shading_factor[key])


if __name__ == '__main__':
    unittest.main(verbosity=2)
