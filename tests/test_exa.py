import os
import unittest
import logging
from becs import becs_exa_report as exa

file_dir = os.getcwd() + os.sep + "exa"
files = os.listdir(file_dir)
file_list = [
    file_dir + os.sep + file
    for file in files
    if "~$" not in file and ".py" not in file and ".docx" in file
]
logger = logging.getLogger(name=__name__)
logging.basicConfig(level=logging.CRITICAL,
                    format="%(levelname)s %(threadName)s %(name)s %(message)s")

print(file_list)


def reinitialize_var():
    exa.docx = None
    exa.exa_table = None
    exa.paras = None
    exa.climate_area = None
    exa.win_shading_south = None
    exa.win_shading_north = None
    exa.win_shading_east = None
    exa.win_shading_west = None


projects_win_shading_factor = {}
projects_win_climate_area = {}

for i_index, file in enumerate(file_list):
    if i_index >= 0:
        exa.docx = exa.Document(file)
        exa.paras = exa.docx.paragraphs
        exa.exa_table = exa.docx.tables[0]
        exa.parse_climate_area(exa.paras)
        exa.parse_win_shading(exa.exa_table)
        logger.critical(f"Current file: {file}  Index: [{i_index}]")
        # Climate
        projects_win_climate_area[file] = exa.climate_area
        # Win shading factor
        projects_win_shading_factor[file] = [
            exa.win_shading_south.text, exa.win_shading_north.text, exa.win_shading_east.text,
            exa.win_shading_west.text
        ]
        # Reinitialization
        reinitialize_var()

print("\n\n\n\n")


class Test_Case1(unittest.TestCase):
    maxDiff = None

    def test_project01(self):
        key = '(2020.4.10)广西居住建筑节能设计审查表（夏热冬暖地区南区）.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ["－", "－", '0.70', '0.80']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project02(self):
        key = '1#广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ["－", "－", '0.76', '0.80']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project03(self):
        key = '123132#广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ["asd", "assd", '0.30', "dd"]
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project04(self):
        key = '28#广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ['0.63', '0.64', '0.50', '0.50']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project05(self):
        key = '29#广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ['0.61', '0.61', '0.50', '0.50']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project06(self):
        key = '29#河池夏热冬冷地区北区广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区北区"
        shading_factor = ['0.61', '0.63', '0.46', '0.47']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project07(self):
        key = '(2020.4.10)广西居住建筑节能设计审查表（夏热冬暖地区南区）.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ["－", "－", '0.70', '0.80']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project08(self):
        key = '贵源1#广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬暖地区南区"
        shading_factor = ['0.93', '1.00', '0.90', '0.80']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])

    def test_project09(self):
        key = '29#桂林夏热冬冷地区广西居住建筑节能设计审查表.docx'
        key = file_dir + os.sep + key
        climate_area = "夏热冬冷地区"
        shading_factor = ['0.61', '0.63', '0.47', '0.47']
        if key in file_list:
            self.assertEqual(climate_area, projects_win_climate_area[key])
            self.assertEqual(shading_factor, projects_win_shading_factor[key])


if __name__ == '__main__':
    unittest.main(verbosity=2)
