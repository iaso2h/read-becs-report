import errors
import re
import logging
import global_control
from gui import myUI
from docx import Document

logger = logging.getLogger(name=__name__)
file_name = None
file_base_name = None
file_ext = None
docx = None
paras = None
exa_table = None
climate_area = None
win_shading_south = None
win_shading_north = None
win_shading_east = None
win_shading_west = None


def debug_paras(input_paras):
    for p_index, p in enumerate(input_paras):
        print(f"[{p_index} offset content is: {p.text}]")
    pass


# noinspection PyUnresolvedReferences
def debug_table_offset_value():
    if exa_table:
        print("_----------------------------------------")
        for c_index, cell in enumerate(exa_table._cells):
            if cell.text and exa_table._cells[c_index].text != exa_table._cells[
                    c_index - 1].text:
                print(f"The first [{cell.text}] offset is: [{c_index}]")
        print("-----------------------------------------")


def promptError(input_type):
    myWidget = myUI.MyWidget()
    myUI.MyMessageBox.critical(myWidget, "匹配错误", input_type,
                               myUI.MyMessageBox.Ok)


def check_exa_docx(loaded_document):
    """检查是否是标准的建筑节能设计审查表"""
    if "建筑节能设计" in loaded_document.paragraphs[0].text:
        return True
    # elif "广西公共建筑围护结构节能设计" in loaded_document.paragraphs[0].text:
    #     return True
    else:
        promptError("不支持的建筑节能设计审查表类型")


# noinspection PyProtectedMember
def parse_win_shading(table):
    """Parse win shading in the report"""
    global win_shading_south, win_shading_north, win_shading_east, \
        win_shading_west

    climate_area_list = ("夏热冬暖地区南区", "夏热冬冷地区", "夏热冬暖地区北区")
    climate_index = climate_area_list.index(climate_area)
    if climate_index == 0:
        # 夏热冬暖地区
        win_shading_south = table._cells[191]
        win_shading_north = table._cells[192]
        win_shading_east = table._cells[196]
        win_shading_west = table._cells[198]
    elif climate_index == 1:
        # 夏热冬冷地区
        win_shading_south = table._cells[177]
        win_shading_north = table._cells[178]
        win_shading_east = table._cells[179]
        win_shading_west = table._cells[182]
    elif climate_index == 2:
        # 夏热冬暖地区北区
        win_shading_south = table._cells[202]
        win_shading_north = table._cells[204]
        win_shading_east = table._cells[206]
        win_shading_west = table._cells[208]
    else:
        promptError("读取审查表的气候地区错误")


def parse_climate_area(input_paras):
    global climate_area
    result = re.search(r'(?<=（).+(?=）)', input_paras[0].text)
    if result:
        climate_area = result.group()
    else:
        logger.warning("Regex cannot obtain climate area")
        errors.warn_message.append("正则表达式不能获取的节能气候地区")
        climate_area = ""


# noinspection PyUnresolvedReferences
def check_shading_consistency():
    from becs import becs_energy_process as energy
    global win_shading_south
    global win_shading_north
    global win_shading_east
    global win_shading_west
    # first el comes from 审查表, 2nd from 节能计算书, orientation mark, index for
    # future use
    compare_pairs = (
        (win_shading_south, energy.win_shading_factor["south"][0], "南", 1),
        (win_shading_north, energy.win_shading_factor["north"][0], "北", 2,),
        (win_shading_east, energy.win_shading_factor["east"][0], "东", 3),
        (win_shading_west, energy.win_shading_factor["west"][0], "西", 4))
    failed_pairs = list(
        filter(lambda compare_pair: compare_pair[0].text != compare_pair[1],
               compare_pairs))
    failed_pairs_list = []
    if failed_pairs and global_control.default_settings["全局设置"][
            "开发者模式"] is False:
        logger.warning("Window shading parameters inconsistency found")
        output_chk = False
        for pair in failed_pairs:
            text1 = f"    节能审查表{pair[2]}侧的外窗外遮阳系数SD为:{pair[0].text}\n"
            text2 = f"建筑节能计算书{pair[2]}侧的外窗外遮阳系数SD为:{pair[1]}\n"
            text1 += text2
            failed_pairs_list.append(text1)
        failed_pairs_list.append("\n是否将所有的审查表外遮阳系数SD改成计算书的数值？\n")

        reply = myUI.MyMessageBox.question(
            myUI.MyWidget(), "参数不一致", "\n".join(failed_pairs_list),
            myUI.MyMessageBox.Yes | myUI.MyMessageBox.No, myUI.MyMessageBox.Yes)
        if reply == myUI.MyMessageBox.Yes:
            output_chk = True
    else:
        output_chk = True

    if output_chk is True:
        change_text_list = []
        for pair in failed_pairs:
            orientation_index = pair[3] - 1
            changed_pair = compare_pairs[orientation_index]
            changed_pair[0].text = str(changed_pair[1])
            change_text = f"节能审查表{changed_pair[2]}侧的外窗外遮阳系数SD的值已改为:" \
                          f"{changed_pair[0].text}\n"
            change_text_list.append(change_text)

        failed_pairs_list.append(
            "<------------------------------------------------------->\n")
        failed_pairs_list += change_text_list
        myUI.MyMessageBox.about(myUI.MyWidget(), "参数修改完成",
                                "\n".join(failed_pairs_list))

        logger.info("Window shading parameters have been revised")
    else:
        logger.info("Skipped revising window shading parameters")


def process(match_object):
    import os
    import global_control
    import file_open
    global file_name
    global file_base_name
    global file_ext
    global paras
    global docx
    global exa_table
    file_name = match_object.file_name
    file_base_name = match_object.file_base_name
    file_ext = match_object.file_ext
    docx = Document(os.path.join(file_open.wd, file_name))

    # check valid "广西居住建筑节能设计审查表"
    logger.info("Checking standard exa docx")
    check_exa_docx(docx)
    paras = docx.paragraphs
    exa_table = docx.tables[0]

    # debug_paras()
    # debug_table_offset_value()
    parse_climate_area(paras)
    parse_win_shading(exa_table)
    if global_control.settings["建筑节能设计审查表"]["检查外窗遮阳系数统一"] is True:
        logger.info("Checking shading consistency")
        check_shading_consistency()
