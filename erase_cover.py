import global_control
import logging

logger = logging.getLogger(name=__name__)


def erase(file_type, docx, end_p_index):
    paras = docx.paragraphs
    tables = docx.tables
    sections = docx.sections
    # 去除页眉
    if global_control.settings[file_type]["去除斯维尔原页眉"] is True:
        logger.info("Erasing header")
        for sec in sections:
            sec.header.is_linked_to_previous = True
    else:
        pass

    def delete_element(el):
        el._element.getparent().remove(el._element)

    # 去除封面和目录
    if global_control.settings[file_type]["去除斯维尔原封面"] is True:
        logger.info(f"Erasing cover in {file_type}")
        for table in tables[:2]:
            delete_element(table)
        if global_control.settings[file_type]["去除斯维尔原目录"] is True:
            logger.info("Erasing table of content")
            p_key = 0
            for p_index, p in enumerate(paras):
                if p.text == "建筑概况":
                    p_key = p_index
                    break
            for el in paras[:p_key]:
                delete_element(el)
        for el in paras[:end_p_index]:
            delete_element(el)
    else:
        pass
